/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
        $(".select2_demo_1").select2();
//esta funcion es el ajax que unicamente manda valores obtenidos de esta manera "parametros=('indice' : valor)"
function multiple_ajax(urls,parametros,div){
        $.ajax({
                data:  parametros,
                url:   site+urls,
                type:  'post',
                beforeSend: function () {
                        $(div).html("<img src='"+base+"plantilla/img/default.gif'>");
                },
                success:  function (response) {
                        $(div).html(response);
                },
                error: function(status){
                    alert("el status es; "+status);
                }
        });
}
//y esta otra funcion solo los que son obtenidos directamente del formulario "serealize","new=formdata.document..."
function multiple_ajax2(urls,parametros,div){
        $.ajax({
                url: site+urls,
                type: "post",
                dataType: "html",
                data: parametros,
                cache: false,
                contentType: false,
	     processData: false
            })
                .done(function(res){
                    $(div).html(res);
                });
}
    $('#id_departamento').change(function() {
         var formData ={
                 "id_departamento": $(this).val()
             };
        multiple_ajax("/material/datosdepa",formData,"#info");
    });
    $(function(){
        var MaxInputs       = 10; //Número Maximo de Campos
        var AddButton       = $("#agregarCampo"); //ID del Botón Agregar
        //var x = número de campos existentes en el contenedor
        var x = $("#contenedor tr").length + 1;
        var field=x;
        $(AddButton).click(function (e) {
             var response="";
            $.ajax({
               // data: {"item" : field},
                type: "POST",   
                url: site+"/material/new_cell",   
                async: false,
                success : function(text)
                {
                    response= text;
                }
            });
            if(x <= MaxInputs) //max input box allowed
            {
                //agregar campo
                $("#mytable tr:last").after(response);
                x++; //text box increment
                field++; //text box increment
            }
            return false;
        });
        $("body").on("click","#eliminar", function(e){ //click en eliminar campo
            if( x > 1 ) {
                $(this).parent('tr').remove(); //eliminar el campo
                x--;
            }
            return false;
        });
    });
    $('#frmdomaterial').on("submit",function(e) {
        e.preventDefault();
        var formData = new FormData(document.getElementById("frmdomaterial"));
        multiple_ajax2("/material/save_solicitud",formData,"#save_material");
		//alert("Solicitud De Material Exitosa");
    });
});