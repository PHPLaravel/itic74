$(document).ready(function() {

var navegador = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
if(!navegador) $("#not-chrome").show(); 


/*************************************************************************************/

$("#pass_user").on('keyup', function() {
	var pass = $(this).val(),
		pass2 = $("#pass2_user").val();

	if(pass != pass2){
		$("#error_pass").css('display', 'inline');
		$("button[type=submit]").attr('disabled', 'disabled');
	}
	else{
	    $("#error_pass").css('display', 'none');
		$("button[type=submit]").removeAttr('disabled');
	}
});

$("#pass2_user").on('keyup', function() {
	var pass2 = $(this).val(),
		pass = $("#pass_user").val();

	if(pass != pass2){
		$("#error_pass").css('display', 'inline');
		$("button[type=submit]").attr('disabled', 'disabled');
	}
	else{
	    $("#error_pass").css('display', 'none');
		$("button[type=submit]").removeAttr('disabled');
	}
});





$('#pass2').blur(function(){
		$("#msmpas").hide();
		var pass2 = $(this).val().trim();
		var pass1 = $('#pass1').val().trim();
		if(pass2=="" || pass1==""){
			$("#pass1").val('');
			$("#pass2").val('');
		}
		else{
			$.ajax({
				type: "POST",
				url: site+"/procesar/verificarPass",
				data: {ps2:pass2,ps1:pass1},
				success: function(resp) {

					if(resp){
						$("#pass2").val('');
						$("#msmpas").fadeIn('fast').html(resp);
					}
				}
			});
		}		
	});

});






/**
 * [soloLetras función para evitar que se introduscan caracteres no deseados]
 * @param  {[evento]} e [description]
 * @return {[bool]}   [description]
 */


