@extends('layout.layout')
@section('centro')
	<div class="col-md-12">
            @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-success">
        	<div class="card-header">
        		<h3><p>
        			Propaganda &nbsp;
        			<a href="{{ route('documentos.create') }}">
                <button class="btn btn-success">Add</button>   
              </a>
        		</p></h3>
        	</div>
            <div class="card-body">
                 <table class="table table-hover">
                   <thead>
                     <tr style="background-color: #FF5733;">
                      <th>Nombre del Documento</th>
                      <th>Formato</th>
                      <th colspan="2">
                        <Center>OPT</Center>
                      </th>
                     </tr>
                   </thead>
                   <tbody>
                    @foreach ($documentos as $file)
                    <tr>

                      <td>
                        {{ $file->name }}
                      </td>

                      <td>
                        {{ $file->extencion }}
                      </td>

                      <td>

                        <a href="{{ route('documentos.edit',$file->id) }}">
                          <button class="btn btn-info">
                            <i class="fa fa-archive"></i>
                          </button>
                        </a>
                        <hr>
                        <form action="">
                          <button class="btn btn-danger">
                            <i class="fa fa-times-rectangle"></i>
                          </button>
                        </form>
                      </td>

                    </tr>
                    @endforeach
                   </tbody>
                 </table> 
            </div>
        </div>
    </div>
@endsection 
@section('scrip')

@stop



