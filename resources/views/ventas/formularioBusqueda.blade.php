@extends('layout.layout')
@section('centro')
<div class="col-md-12">
        <div class="card card-success">
          <div class="card-header">
            <h3><p>
              Hola  listo para consultar tus tramites..? &nbsp;
            </p></h3>
          </div>
            <div class="card-body">
                 <form action="" method="POST">
                    @csrf
                    <div class="form-group">
                      <label>Nombre:</label>
                      <input type="text" name="nombre" class="form-control" id="nombre">
                      <span style="color: red;">{!! $errors->first('nombre') !!}</span>
                    </div>
                    <button type="submit" class="btn btn-success">Buscar...!!!</button>
                 </form>
                 <hr>
                 <div id="mensaje"></div>
            </div>
        </div>
    </div>

@stop
@section('scrip')
$(document).ready(function(){
    let variable = ""

    $("form").on('submit', function(e){

      e.preventDefault();

      

      var nombre = $('#nombre').val()

      //console.log(nombre)
      
      $.ajax({

        url:'{{ route('busqueda') }}',

        type:'POST',

        data: {

            "_token": "{{ csrf_token() }}",
            "nombre": nombre
        },
        success:function(response){
          //console.log(response)

          $.each(response, function(i,val){

            variable += `    <table class="table table-hover">
                              <thead>
                                <tr>
                                  <th>claveTransaccion</th>
                                  <th>Nombre Alumno</th>
                                  <th>Correo</th>
                                  <th>Total</th>
                                  <th>Moneda</th>
                                  <th>Estado</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>${val.claveTransaccion}</td>
                                  <td>${val.nombreAlumno}</td>
                                  <td>${val.correo}</td>
                                  <td>${val.total}</td>
                                  <td>${val.currency}</td>
                                  <td>${val.status}</td>
                                </tr>
                              </tbody>
                            </table>`

          })

          $('#mensaje').html(variable)

        },
        error:function(error){
          console.error(error)
        }

      })
      

    });

})

@stop
