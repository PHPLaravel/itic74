@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Transacciones &nbsp;
        			
              &nbsp;

        		</p></h3>
        	</div>
            <div class="card-body">
                  <table class="table table-hover">
                  	<thead>
                  		<tr>
                  			<th>Num Trnsaccion</th>
                  			<th>Nombre Alumno</th>
                  			<th>Correo</th>
                  			<th>Total</th>
                  			<th>Pesos</th>
                        <th>Estado</th>
                  		</tr>
                  	</thead>
                  	<tbody>
                  		@forelse ($ventas as $v)
                  			<tr>
                  				<td>
                  					{!! $v->claveTransaccion !!}
                  				</td>
                  				<td>
                  					{!! $v->nombreAlumno !!}
                  				</td>
                  				<td>
                  					{!! $v->correo !!}
                  				</td>
                  				<td>
                  					{!! $v->total !!}
                  				</td>
                          <td>
                            {!! $v->currency !!}
                          </td>
                          <td>
                            {!! $v->status !!}
                          </td>
                  				<td>
                  					<a href="">
                  						<button class="btn btn-info">Edit</button>
                  					</a>
                  					&nbsp;
                  					<form action="" method="POST">
                  						@csrf

										          {{ method_field('DELETE') }}
                  						<button class="btn btn-danger">Delete</button>
                  					</form>
                  				</td>
                  			</tr>
                  		@empty
                  			<p>Sin Registros...</p>	
                  		@endforelse
                  	</tbody>
                  </table>
            </div>
        </div>
    </div>
@stop

@section('script')

@stop

