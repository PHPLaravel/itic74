@extends('layout.layout')
@section('centro')
	<div class="col-md-12">
        <div class="card card-success">
        	<div class="card-header">
        		<h3><p>
        			Docentes &nbsp;
        		</p></h3>
        	</div>
            <div class="card-body">
                 <form action="{{ route('updateEventos',$eventos[0]->id) }}" method="POST" enctype="multipart/form-data">

                 	@csrf
                    @method('PUT')

                 	<div class="form-group">
                 		<label>Evento Escolar:</label>
                 		<input name="eventoEscolar" id="eventoEscolar" value="{{ $eventos[0]->eventoEscolar }}" class="form-control">
                        
                 		<span style="color: red;">{!! $errors->first('eventoEscolar') !!}</span>
                 	</div>

                    <div class="form-group">
                        <label>Docente:</label>
                        <select name="idD" id="idD" class="form-control">
                            <option value="{{ $eventos[0]->idD }}">{{ $eventos[0]->nombre }}</option>
                            @foreach ($docente as $doc)
                            <option value="{{ $doc->idD }}">
                                {{ $doc->nombre }}
                            </option>
                            @endforeach

                        </select>
                        <span style="color: red;">{!! $errors->first('eventoEscolar') !!}</span>
                    </div>

                    <div class="form-group">
                        <label>Grupo:</label>
                        <select name="idGrupo" id="idGrupo" class="form-control">

                            <option value="{{ $eventos[0]->idGrupo }}">{{ $eventos[0]->nombreG }}</option>

                            @foreach ($grupo as $grupos)
                            <option value="{{ $grupos->idGrupo }}">
                                {{ $grupos->nombreG }}
                            </option>
                            @endforeach

                        </select>
                        <span style="color: red;">{!! $errors->first('eventoEscolar') !!}</span>
                    </div>

                    <div class="form-group">
                        <label>Documento:</label>
                            <br>
                            <a href="{{ route('download',$eventos[0]->id) }}">
                                <img src="{{ $eventos[0]->ruta }}/{{ $eventos[0]->Foto }}" alt="" width="100px" height="100px;" class="img-rounded">
                            </a>
                        <br>
                        <br>
                        <input type="file" name="Foto" class="form-control">
                        <span style="color: red;">{!! $errors->first('eventoEscolar') !!}</span>
                    </div>

                 	<button type="submit" class="btn btn-success">Save...!!!</button>
                 </form>
            </div>
        </div>
    </div>
@stop