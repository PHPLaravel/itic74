@extends('layout.layout')
@section('centro')
	<div class="col-md-12">
            @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-success">
        	<div class="card-header">
        		<h3><p>
        			Eventos &nbsp;
        			<a href="{{ route('eventos.create') }}">
                <button class="btn btn-success">Add</button>   
              </a>
        		</p></h3>
        	</div>
            <div class="card-body">
                 <table class="table table-hover">
                   <thead>
                     <tr style="background-color: #FF5733;">
                      <th>Evento Academico</th>
                      <th>Formato</th>
                      <th>OPT</th>
                     </tr>
                   </thead>
                   <tbody>
                    @foreach ($evento as $i)
                      <tr>
                        
                        <td>
                          {{ $i->eventoEscolar }}
                        </td>
                        <td>
                          <a href="{{ route('download',$i->id) }}">
                            <img src="{{ $i->ruta }}/{{ $i->Foto }}" alt="" width="100px" height="100px;" class="img-rounded">
                          </a>
                        </td>
                        <td>
                          <a href="{{ route('editEventos',$i->id) }}">
                            <button class="btn btn-info edit">
                              Edit
                            </button>
                          </a>
                          &nbsp;
                          <form action="{{ route('delete',$i->id) }}" method="POST">
                            @csrf
                            {{ method_field('DELETE') }}
                            <button class="btn btn-danger">
                              Delete
                            </button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                   </tbody>
                 </table> 
            </div>
        </div>
    </div>
@endsection 
@section('scrip')

@stop



