@extends('layout.layout')
@section('centro')
	<div class="col-md-12">
        <div class="card card-success">
        	<div class="card-header">
        		<h3><p>
        			Docentes &nbsp;
        		</p></h3>
        	</div>
            <div class="card-body">
                 <form action="{{ route('eventos.store') }}" method="POST" enctype="multipart/form-data">

                 	@csrf
                    

                 	<div class="form-group">
                 		<label>Evento Escolar:</label>
                 		<textarea name="eventoEscolar" id="eventoEscolar" cols="30" rows="10"  class="form-control">
                            
                        </textarea>
                 		<span style="color: red;">{!! $errors->first('eventoEscolar') !!}</span>
                 	</div>

                    <div class="form-group">
                        <label>Docente:</label>
                        <select name="idD" id="idD" class="form-control">
                            @foreach ($docente as $docente)
                            <option value="{{ $docente->idD }}">
                                {{ $docente->nombre }}
                            </option>
                            @endforeach
                        </select>
                        <span style="color: red;">{!! $errors->first('idD') !!}</span>
                    </div>

                    <div class="form-group">
                        <label>Docente:</label>
                        <select name="idGrupo" id="idGrupo" class="form-control">
                            @foreach ($grupo as $g)
                            <option value="{{ $g->idGrupo }}">
                                {{ $g->nombreG }}
                            </option>
                            @endforeach
                        </select>
                        <span style="color: red;">{!! $errors->first('idGrupo') !!}</span>
                    </div>

                    <div class="form-group">
                        <label>Documento u Evento:</label>
                        <input type="file" name="Foto" class="form-control">
                        <span style="color: red;">{!! $errors->first('Foto') !!}</span>
                    </div>

                 	<button type="submit" class="btn btn-success">Save...!!!</button>
                 </form>
            </div>
        </div>
    </div>
@stop