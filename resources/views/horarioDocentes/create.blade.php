@extends('layout.layout')
@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Horari de Docentes &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>'horarioDocentes.store', 'method' => 'POST']) !!}

						<div class="form-group">
							{!! Form::label('dia', 'Dia') !!}

							{!! Form::text('dia',null,['class'=>'form-control','placeholder'=>'Dia']) !!}
							<span style="color: red;">{!! $errors->first('dia') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('docente', 'Docente') !!}
							<select class="form-control" name="idD">
								@foreach($docente as $x)
									<option value="{{ $x->idD }}">
										{{ $x->nombre }}
									</option>
								@endforeach
								<span style="color: red;">{!! $errors->first('idD') !!}</span>
							</select>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Materia') !!}
							
							<select class="form-control" name="idMateria">
								@foreach($materia as $x)
									<option value="{{ $x->idMateria }}">
										{{ $x->nombreDelamateria }}
									</option>
								@endforeach
								<span style="color: red;">{!! $errors->first('idMateria') !!}</span>
							</select>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Grupo') !!}
							
							<select class="form-control" name="idGrupo">
								@foreach($grupo as $y)
									<option value="{{ $y->idGrupo }}">
										{{ $y->nombreG }}
									</option>
								@endforeach
								<span style="color: red;">{!! $errors->first('idGrupo') !!}</span>
							</select>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Hora de Inicio') !!}

							
							{{ Form::time('horaInicio',null, ['class'=>'form-control','placeholder'=>'hora'] ) }}
							<span style="color: red;">{!! $errors->first('horaInicio') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Hora de Termino') !!}

							{{ Form::time('horaFin',null, ['class'=>'form-control','placeholder'=>'hora'] ) }}
							<span style="color: red;">{!! $errors->first('horaFin') !!}</span>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop