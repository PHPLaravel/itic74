@extends('layout.layout')
@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Horari de Docentes &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>array('horarioDocentes.update',$edit->idHorario), 'method' => 'POST']) !!}
                  	@method('PUT')
						<div class="form-group">
							{!! Form::label('dia', 'Dia') !!}

							{!! Form::text('dia',$edit->dia,['class'=>'form-control','placeholder'=>'Dia']) !!}
							<span style="color: red;">{!! $errors->first('dia') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('docente', 'Docente') !!}
							<select class="form-control" name="idD">
								<option value="{{ $edit->docentes->idD }}">
									{{ $edit->docentes->nombre }}
								</option>
								@foreach($docente as $x)
									<option value="{{ $x->idD }}">
										{{ $x->nombre }}
									</option>
								@endforeach
							</select>
							<span style="color: red;">{!! $errors->first('idD') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Materia') !!}
							
							<select class="form-control" name="idMateria">
								<option value="{{ $edit->materias->idMateria }}">

									{{ $edit->materias->nombreDelamateria }}

								</option>
								@foreach($materia as $x)
									<option value="{{ $x->idMateria }}">
										{{ $x->nombreDelamateria }}
									</option>
								@endforeach
							</select>
							<span style="color: red;">{!! $errors->first('idMateria') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Grupo') !!}
							
							<select class="form-control" name="idGrupo">
								<option value="{{ $edit->grupo->idGrupo }}">
									{{ $edit->grupo->nombreG }}
								</option>
								@foreach($grupo as $y)
									<option value="{{ $y->idGrupo }}">
										{{ $y->nombreG }}
									</option>
								@endforeach
								<span style="color: red;">{!! $errors->first('idGrupo') !!}</span>
							</select>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Hora de Inicio') !!}

							
							{{ Form::time('horaInicio',$edit->horaInicio, ['class'=>'form-control','placeholder'=>'hora'], 'H:i:s' ) }}
							<span style="color: red;">{!! $errors->first('horaInicio') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Hora de Termino') !!}

							{{ Form::time('horaFin',$edit->horaFin, ['class'=>'form-control','placeholder'=>'hora'], 'H:i:s' ) }}
							<span style="color: red;">{!! $errors->first('horaFin') !!}</span>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop