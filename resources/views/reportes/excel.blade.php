<h2>
	Grupo: {{ $reporte[0]->grupo }}
</h2>
<h2>
	Materia: {{ $reporte[0]->materia }}
</h2>
<table>
					<thead>
						<tr>
							<th>Alumno</th>
							<th>Primer Parcial</th>
							<th>Segundo Parcial</th>
							<th>Tercer Parcial</th>
							<th>Cuarto Parcial</th>
							<th>Quinto Parcial</th>
							<th>Promedio General</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($reporte as $x)
						<tr>
							<td>
								{{ $x->nombre }}
							</td>
							<td>
								{{ $x->PrimerParcial }}
							</td>
							<td>
								{{ $x->SegundoParcial }}
							</td>
							<td>
								{{ $x->TercerParcial }}
							</td>
							<td>
								{{ $x->CuartoParcial }}
							</td>
							<td>
								{{ $x->QuintoParcial }}
							</td>
						@if($x->promedioGral >= 8)
							<td style="background-color: #58d8a3;">
								{{ $x->promedioGral }}
							</td>
						@else	
							<td style="background-color: #FF0017;">
								{{ $x->promedioGral }}
							</td>
						@endif					
						</tr>
						@endforeach
					</tbody>
				</table>