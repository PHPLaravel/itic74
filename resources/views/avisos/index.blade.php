@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Avisos &nbsp;
        			<a href="{{ route('avisos.create') }}"> 
        				<button class="btn btn-success">
        					New
        				</button>
        			</a>
        		</p></h3>
        	</div>
            <div class="card-body">
                  <table class="table table-hover">
                  	<thead>
                  		<tr>
                  			<th>ID##</th>
                  			<th>Aviso Escolar</th>
                  			<th>Docente</th>
                        <th>Archivo</th>
                  			<th>Especialidad</th>
                  			<th>Grupo</th>
                  			<th>Options</th>
                  		</tr>
                  	</thead>
                  	<tbody>

                  		@forelse ($gralquery as $i)
                  			<tr>
                  				<td>
                  					{!! $i->id !!}
                  				</td>
                  				<td>
                  					{!! $i->avisoEscolar !!}
                  				</td>
                  				<td>
                  					{!! $i->nombre !!}
                  				</td>
                          <td>
                            <img src="{{ $i->ruta }}/{{ $i->Foto }}" alt="" width="60px" height="60px">
                          </td>
                  				<td>
                  					{!! $i->especialidadOmateria !!}
                  				</td>
                  				<td>
                  					{!! $i->nombreG !!}
                  				</td>
                  				<td>
                  					<a href="{{ route('avisos.edit', $i->id ) }}">
                  						<button class="btn btn-info">Edit</button>
                  					</a>
                  					&nbsp;
                  					<form action="{{ route('avisos.destroy', $i->id ) }}" method="POST">
                  						@csrf

										          {{ method_field('DELETE') }}
                  						<button class="btn btn-danger">Delete</button>
                  					</form>
                  				</td>
                  			</tr>
                  		@empty
                  			<p>Sin Registros...</p>	
                  		@endforelse
                  	</tbody>
                  </table>
            </div>
        </div>
    </div>
@stop
@section('script')

@stop
