@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Avisos &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>'avisos.store', 'method' => 'POST', 'enctype' => 'multipart/form-data' ]) !!}
						<div class="form-group">
							{!! Form::label('Aviso Escolar', 'Aviso Escolar') !!}

							{!! Form::text('avisoEscolar',null,['class'=>'form-control','placeholder'=>'avisoEscolar']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('docente', 'Docente') !!}
							<select class="form-control" name="idD">
							@foreach ($docentes as $i)
								<option value="{{ $i->idD }}">{{ $i->nombre }}</option>
							@endforeach
							</select>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Grupo') !!}
							<select class="form-control" name="idGrupo">
							@foreach ($grupos as $g)
								<option value="{{ $g->idGrupo }}">{{ $g->nombreG }}</option>
							@endforeach
							</select>
						</div>
						<div class="form-group">
							{!! Form::label('Archivo', 'Archivo') !!}
							{!! Form::file('archivo',null,['class'=>'form-control' ]) !!}
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop