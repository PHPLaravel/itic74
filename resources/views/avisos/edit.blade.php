@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Avisos &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>array('avisos.update',$edit->id), 'method' => 'POST']) !!}
                  		@method('PUT')
						<div class="form-group">
							{!! Form::label('Aviso Escolar', 'Aviso Escolar') !!}

							{!! Form::text('avisoEscolar',$edit->avisoEscolar,['class'=>'form-control','placeholder'=>'avisoEscolar']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('docente', 'Docente') !!}
							<select class="form-control" name="idD">
								<option value="{{ $edit->docentes->idD }}">
									{{ $edit->docentes->nombre }}
								</option>
								@foreach ($docente as $i)
                					<option value="{{ $i->idD }}">
                						{{ $i->nombre }}
                					</option>
              					@endforeach
							</select>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Grupo') !!}
							<select class="form-control" name="idGrupo">
								<option>{{ $edit->grupo->nombreG }}</option>
								@foreach ($grupo as $g)
                					<option value="{{ $g->idGrupo }}">
                						{{ $g->nombreG }}
                					</option>
              					@endforeach
							</select>

						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop