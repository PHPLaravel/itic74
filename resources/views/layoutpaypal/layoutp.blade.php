<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="{{ asset('template/css/mdb.min.css') }}">
    <!-- Estilos Personalizados -->
    <link rel="stylesheet" href="{{ asset('template/css/estilos.css') }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark gris scrolling-navbar fixed-top">
  		
		<div class="container">
			<a class="navbar-brand" data-scroll href="#inicio">Secundaria 382</a>
  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    			<span class="navbar-toggler-icon"></span>
  			</button>

 		<div class="collapse navbar-collapse" id="navbarSupportedContent">
    		<ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav">
      			<li class="nav-item">
       				<a class="nav-link" href="{{ asset('template/archivos/calendario escolar.pdf') }}">Calendario Escolar</a>
      			</li>
            <li class="nav-item">
              <a class="nav-link" data-scroll href="#conocenos">Conocenos</a>
            </li>
      			<li class="nav-item">
        			<a class="nav-link" data-scroll href="#instalaciones">Instalaciones</a>
      			</li>
            <li class="nav-item">
              <a class="nav-link" data-scroll href="#avisos">Avisos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-scroll href="#contactos">Contactos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-scroll href="{{ route('login') }}">Sistema</a>
            </li>
    		</ul>
  		</div>
		</div>
	</nav>

  <section class="fondo view" id="inicio">
    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
      <div class="container">
        <div class="row">
        <div class="col-md-6 text-white">
          <h1 class="h1-responsive">Esc. Sec. Ofic. No. 0382 "Dr. José Ma. Luis Mora"</h1>
          <hr class="bg-light">
          <p class="h4-responsive text-center text-md-left">Te presentamos la página de la Escuela Secundaria Oficial No. 0382 del Ocotal Analco, Estado de México. 
          La intención de la misma es abrir un espacio de comunicación entre el personal académico, alumnos y padres de familia, que nos permita fortalecernos como institución de vanguardia que somos. </p>
        </div>
        
      </div>
      </div>
    </div>
  </section>


  <main class="container">

    <section class="row">
      <div class="col-md-6">
        @yield('contenido')
      </div>
    </section>
  </main>


<!-- Footer -->
<footer class="page-footer font-small green pt-4">

  <!-- Footer Links -->
  <div class="container">

    <!-- Grid row-->
    <div class="row text-center d-flex justify-content-center pt-5 mb-3">

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="#inicio">Inicio</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a href="{{ asset('template/archivos/calendario escolar.pdf') }}">Calendario Escolar</a>
        </h6>
      </div>
      <!-- Grid column -->
      
      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="#conocenos">Conocenos</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="#instalaciones">Instalaciones</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="#avisos">Avisos</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="#contactos">Contactos</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="{{ route('login') }}">Sistema</a>
        </h6>
      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row-->
    <hr class="rgba-white-light" style="margin: 0 15%;">

    <!-- Grid row-->
    <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">

      <!-- Grid column 
      <div class="col-md-8 col-12 mt-5">
        <p style="line-height: 1.7rem">Sed ut perspiciatis unde omnis iste natus error sit voluptatem
          accusantium doloremque laudantium, totam rem
          aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
          explicabo.
          Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</p>
      </div>
       Grid column -->

    </div>
    <!-- Grid row-->
    <hr class="clearfix d-md-none rgba-white-light" style="margin: 10% 15% 5%;">

  </div>
  <!-- Footer Links -->

 <!-- Copyright -->
  <div class="footer-copyright text-center py-3">Esc. Sec. Ofic. No. 0382 "Dr. José Ma. Luis Mora"<br>© 2019 Todos los derechos reservados
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->

	<!-- JQuery -->
<script src="{{ asset('template/js/jquery.min.js') }}"></script>
<!-- Bootstrap tooltips -->
<script src="{{ asset('template/js/popper.min.js') }}"></script>
<!-- Bootstrap core JavaScript -->
<script src="{{ asset('template/js/bootstrap.min.js') }}"></script>
<!-- MDB core JavaScript -->
<script src="{{ asset('template/js/mdb.min.js') }}"></script>
<!-- Get patch fixes within a minor version -->
<script src="{{ asset('template/js/smooth-scroll.polyfills.min.js') }}"></script>

<script>
  var scroll = new SmoothScroll('a[href*="#"]', {
    offset: 50,
    speed: 1000
  });


</script>
 @yield('js')
</body>
</html>
