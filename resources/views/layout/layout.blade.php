<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <script src="{{ asset('star/node_modules/jquery/dist/jquery.min.js') }}"></script>
  <script type="text/javascript" src="{{ url('star/bower_components/dropzone/downloads/dropzone.min.js')}}"></script>

  <link rel="stylesheet" type="text/css" href="{{ url('star/bower_components/dropzone/downloads/css/dropzone.css') }}">

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>GEMElite</title>
  <link rel="stylesheet" href="{{ url('star/node_modules/font-awesome/css/font-awesome.min.css')}}" />
  <link rel="stylesheet" href="{{ url('star/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css') }}" />
  <link rel="stylesheet" href="{{ url('star/css/style.css') }}" />
  <link rel="shortcut icon" href="{{ url('plantilla/img/Imagenes/CCFISCAL.jpg') }}" />

  <!-- Toggle  -->

  <!---- dataTables --->
  

  <style type="text/css">
    html, body
    {
      height: 100%;
    }
    .button{
    position: relative;
    background-color: #1093EC;
    border: none;
    font-size: 12px;
    color: #FFFFFF;
    padding: 20px;
    width: 200px;
    text-align: center;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    text-decoration: none;
    overflow: hidden;
    cursor: pointer;
}

.button:after {
    content: "";
    background: #F4F7F9;
    display: block;
    position: absolute;
    padding-top: 300%;
    padding-left: 350%;
    margin-left: -20px !important;
    margin-top: -120%;
    opacity: 0;
    transition: all 0.8s
}

.button:active:after {
    padding: 0;
    margin: 0;
    opacity: 1;
    transition: 0s
}

  </style>
</head>

<body>
  <div class=" container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar navbar-default col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="bg-white text-center navbar-brand-wrapper">
        <a class="navbar-brand brand-logo" href="{{ route('alumno.index') }}"><img src="{{ asset('star/images/logo_star_black.pn') }}g" /></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="{{ asset('star/images/logo_star_mini.jpg') }}" alt=""></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler d-none d-lg-block navbar-dark align-self-center mr-3" type="button" data-toggle="minimize">
          <span class="navbar-toggler-icon"></span>
        </button>
        <form class="form-inline mt-2 mt-md-0 d-none d-lg-block">
          <input class="form-control mr-sm-2 search" type="text" placeholder="Search">
        </form>
        <ul class="navbar-nav ml-lg-auto d-flex align-items-center flex-row">
          <li class="nav-item">
            <a class="nav-link profile-pic" href="#"><img class="rounded-circle" src="{{ asset('star/images/face.jpg') }}" alt=""></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa fa-th"></i></a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-dark navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
    </nav>

    <!-- partial -->
    <div class="container-fluid">
      <div class="row row-offcanvas row-offcanvas-right">
        <!-- partial:partials/_sidebar.html -->
        <nav class="bg-white sidebar sidebar-offcanvas" id="sidebar">
          <div class="user-info">
            <img src="{{ asset('star/images/face.jpg') }}" alt="">
            @if( Auth::check() )
            <p class="name">Welcome {{ Auth::user()->name }} </p>

                

                @if( Auth::user()->roles_id === 1)
                <p class="designation" style="color: red;">
                  Manager
                </p>
                @else
                  <p class="designation">
                    Client
                  </p>
                @endif

            <a href="{{ route('logout') }}">
              <p class="designation" style="color: black;">Cerrar Sesion</p>
            </a>
            <span class="online"></span>
            @endif
          </div>
          <ul class="nav">
            @if( Auth::user()->roles_id == 1 or Auth::user()->roles_id == 2)
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#sample-pages" aria-expanded="false" aria-controls="sample-pages">
                <img src="{{ asset('star/images/icons/9.png') }}" alt="">
                <span class="menu-title">Modulos<i class="fa fa-sort-down"></i></span>
              </a>
              <div class="collapse" id="sample-pages">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('modulo') }}">
                      Grupos-- Alumnos
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="">
                      Alumnos -- Profesores
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="pages/samples/register.html">
                      Alumnos -- Horarios 
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('mM') }}">
                      Maestros -- Materia
                    </a>
                  </li>

                </ul>
              </div>
            </li>
            @endif
            @if( Auth::user()->roles_id == 1 or Auth::user()->roles_id == 2)
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#sample-pagess" aria-expanded="false" aria-controls="sample-pages">
                <img src="{{ asset('star/images/icons/9.png') }}" alt="">
                <span class="menu-title">Reportes<i class="fa fa-sort-down"></i></span>
              </a>
              <div class="collapse" id="sample-pagess">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('reporte') }}">
                      Calificaciones
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('GAR') }}">
                      Grupos-- Alumnos
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="">
                      Alumnos -- Profesores
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="pages/samples/register.html">
                      Alumnos -- Horarios 
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route('MM') }}">
                      Maestros -- Materia
                    </a>
                  </li>

                </ul>
              </div>
            </li>
            @endif
            @if( Auth::user()->roles_id == 1 or Auth::user()->roles_id == 2)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('avisos') }}">
                <img src="{{ asset('star/images/icons/2.png') }}" alt="">
                <span class="menu-title">Avisos</span>
              </a>
            </li>
            @endif
            @if( Auth::user()->roles_id == 1 or Auth::user()->roles_id == 2)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('banner.index') }}">
                <img src="{{ asset('star/images/icons/2.png') }}" alt="">
                <span class="menu-title">Publicitarios</span>
              </a>
            </li>
            @endif
             @if( Auth::user()->roles_id == 1 or Auth::user()->roles_id == 2)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('eventos.index') }}">
                <img src="{{ asset('star/images/icons/2.png') }}" alt="">
                <span class="menu-title">Eventos</span>
              </a>
            </li>
            @endif

            @if( Auth::user()->roles_id == 1 or Auth::user()->roles_id == 2)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('alumno.index') }}">
                <img src="{{ asset('star/images/icons/005-forms.png') }}" alt="">
                <span class="menu-title">Alumnos</span>
              </a>
            </li>
            @endif

            @if( Auth::user()->roles_id == 1 or Auth::user()->roles_id == 2)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('Docentes.index') }}">
                <img src="{{ asset('star/images/icons/4.png') }}" alt="">
                <span class="menu-title">Docentes</span>
              </a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link" href="{{ route('materias.index') }}">
                <img src="{{ asset('star/images/icons/5.png') }}" alt="">
                <span class="menu-title">Materias</span>
              </a>
            </li>
             @if( Auth::user()->roles_id == 1 or Auth::user()->roles_id == 2)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('gradoescolar.index') }}">
                <img src="{{ asset('star/images/icons/6.png') }}" alt="">
                <span class="menu-title">Grado Escolar</span>
              </a>
            </li>
            @endif
             @if( Auth::user()->roles_id == 1 or Auth::user()->roles_id == 2)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('grupos.index') }}">
                <img src="{{ asset('star/images/icons/7.png') }}" alt="">
                <span class="menu-title">Grupos</span>
              </a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link" href="{{ route('horarioAlumnos.index') }}">
                <img src="{{ asset('star/images/icons/8.png') }}" alt="">
                <span class="menu-title">Horario Alumnos</span>
              </a>
            </li>
             @if( Auth::user()->roles_id == 1 or Auth::user()->roles_id == 2)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('horarioDocentes.index') }}">
                <img src="{{ asset('star/images/icons/9.png') }}" alt="">
                <span class="menu-title">Horario Docentes</span>
              </a>
            </li>
            @endif
            @if( Auth::user()->roles_id == 1)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('users.index') }}">
                <img src="{{ asset('star/images/icons/9.png') }}" alt="">
                <span class="menu-title">Usuarios</span>
              </a>
            </li>
            @endif
            @if( Auth::user()->roles_id == 3)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('beki') }}">
                <img src="{{ asset('star/images/icons/9.png') }}" alt="">
                <span class="menu-title">Tramites Escolares</span>
              </a>
            </li>
            @endif
            @if( Auth::user()->roles_id == 3 or Auth::user()->roles_id == 2 or Auth::user()->roles_id == 1)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('ventas') }}">
                <img src="{{ asset('star/images/icons/9.png') }}" alt="">
                <span class="menu-title">Mis Compras</span>
              </a>
            </li>
            @endif

            <!--
            <li class="nav-item">
              <a class="nav-link" href="">
                <img src="" alt="">
                <span class="menu-title">Calificaciones</span>
              </a>
            </li>
            -->
          </ul>
        </nav>

        <!-- partial -->
        <div class="content-wrapper">
          <h3 class="page-heading mb-4">Dashboard</h3>
          <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 mb-4">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <h4 class="text-danger">
                        <i class="fa fa-bar-chart-o highlight-icon" aria-hidden="true"></i>
                      </h4>
                    </div>
                    <div class="float-right">
                      <p class="card-text text-dark">Alumnos</p>
                      <h4 class="bold-text">
                        @php
                        $alumnos = DB::table('alumnos')->get();
                        echo count($alumnos);
                        @endphp
                      </h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 mb-4">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <h4 class="text-success">
                        <a href="{{ URL('recib') }}">
                          <i class="fa fa-envelope highlight-icon" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div class="float-right">
                      <p class="card-text text-dark">Correos</p>
                      <h4 class="bold-text">

                      </h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 mb-4">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <h4 class="text-success">
                        <i class="fa fa-dollar highlight-icon" aria-hidden="true"></i>
                      </h4>
                    </div>
                    <div class="float-right">
                      <p class="card-text text-dark">Materias</p>
                      <h4 class="bold-text">
                        @php
                        $materias = App\Materias::all();
                        echo count($materias);
                        @endphp
                      </h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 mb-4">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <h4 class="text-primary">
                        <i class="fa fa-twitter highlight-icon" aria-hidden="true"></i>
                      </h4>
                    </div>
                    <div class="float-right">
                      <p class="card-text text-dark">Docentes</p>
                      <h4 class="bold-text">
                        @php
                        $docente = App\Docentes::all();
                        echo count($docente);
                        @endphp
                      </h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            @yield('centro')
          </div>

          
        </div>
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="float-right">
                <a href="#">Star Admin</a> &copy; 2017
            </span>
          </div>
        </footer>

        <!-- partial -->
      </div>
    </div>

  </div>

  
  <script src="{{ url('star/node_modules/popper.js/dist/umd/popper.min.js')}}"></script>
  <script src="{{ url('star/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <script src="{{ url('star/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
  <script src="{{ url('star/js/off-canvas.js')}}"></script>
  <script src="{{ url('star/js/hoverable-collapse.js')}}"></script>
  <script src="{{ url('star/js/misc.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.js"></script>
  <script type="text/javascript">
    @yield('scrip')
  </script>
</body>

</html>
