@yield('header')
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  <script type="text/javascript" src="{{ url('star/bower_components/dropzone/downloads/dropzone.min.js')}}"></script>

  <link rel="stylesheet" type="text/css" href="{{ url('star/bower_components/dropzone/downloads/css/dropzone.css') }}">

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>GEMElite</title>
  <link rel="stylesheet" href="{{ url('star/node_modules/font-awesome/css/font-awesome.min.css')}}" />
  <link rel="stylesheet" href="{{ url('star/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css') }}" />
  <link rel="stylesheet" href="{{ url('star/css/style.css') }}" />
  <link rel="shortcut icon" href="{{ url('plantilla/img/Imagenes/CCFISCAL.jpg') }}" />

  <!---- AUTOCOMPLET --->
  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

  <style type="text/css">
    html, body
    {
      height: 100%;
    }
    .button{
    position: relative;
    background-color: #1093EC;
    border: none;
    font-size: 12px;
    color: #FFFFFF;
    padding: 20px;
    width: 200px;
    text-align: center;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    text-decoration: none;
    overflow: hidden;
    cursor: pointer;
}

.button:after {
    content: "";
    background: #F4F7F9;
    display: block;
    position: absolute;
    padding-top: 300%;
    padding-left: 350%;
    margin-left: -20px !important;
    margin-top: -120%;
    opacity: 0;
    transition: all 0.8s
}

.button:active:after {
    padding: 0;
    margin: 0;
    opacity: 1;
    transition: 0s
}

  </style>
</head>
  
@yield('menu')
<body>
  <div class=" container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar navbar-default col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="bg-white text-center navbar-brand-wrapper">
          @if(Session::has('sesiontipo'))
           <?php if(Session::get('sesiontipo') == 'Sub Usuario'):?>
               <a class="navbar-brand brand-logo" href="#"><img src="{{ url('star/images/subusuario.png')}}" /></a>
            <?php endif; ?>
          @endif
          @if(Session::has('sesiontipo'))
           <?php if(Session::get('sesiontipo') == 'Usuario'):?>
                <a class="navbar-brand brand-logo" href="#"><img src="{{ url('star/images/usuario.png')}}" /></a>
            <?php endif; ?>
          @endif
        <a class="navbar-brand brand-logo-mini" href="#"><img src="{{ url('star/images/logo_star_mini.jpg')}}" alt=""></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler d-none d-lg-block navbar-dark align-self-center mr-3" type="button" data-toggle="minimize">
          <span class="navbar-toggler-icon"></span>
        </button>
        <form class="form-inline mt-2 mt-md-0 d-none d-lg-block">
          <input class="form-control mr-sm-2 search" type="text" placeholder="Buscar">
        </form>
        <ul class="navbar-nav ml-lg-auto d-flex align-items-center flex-row">
          <li class="nav-item">
            <a class="nav-link profile-pic" href="#"><img src="{{ url('star/images/user.png')}}"/></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">  Cerrar Sesión <i class="fa fa-sign-out"></i></a>
          </li>
           <button class="navbar-toggler navbar-dark navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="navbar-toggler-icon"></span>
        </button>
        </ul>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row row-offcanvas row-offcanvas-right">
        <nav class="bg-white sidebar sidebar-offcanvas" id="sidebar">
          <div class="user-info">
            <img src="{{ url('star/images/user.png')}}" />
            <p class="name"> 
             @if (Session::has('sesionname'))
                 {{Session::get('sesionname')}} {{Session::get('sesionap_pat')}} {{Session::get('sesionap_mat')}}
             @endif
           </p>
            <p class="designation">
              @if (Session::has('sesiontipo'))
                   {{Session::get('sesiontipo')}}</span>
              @endif
              <br>
                 @if (Session::has('rfcs'))
                  <label>RFC:</label> {{Session::get('rfcs')}}</span>
                  
              @endif
            </p>
            @if (Session::has('sesiontipo'))
               <?php if(Session::get('sesiontipo') == 'Sub Usuario'):?> 
            <a href="" class="btn btn-primary mr-2">Cambiar de RFC</a>
            <?php endif; ?>
              @endif
              @if (Session::has('sesiontipo'))
               <?php if(Session::get('sesiontipo') == 'Usuario'):?> 
            <a href="" class="btn btn-primary mr-2">Cambiar de RFC</a>
            <?php endif; ?>
              @endif
          </div>

          <ul class="nav">
                @if (Session::has('sesiontipo'))
               <?php if(Session::get('sesiontipo') == 'Sub Usuario'):?> 
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/empresasup') }}">
                <img src="{{ url('star/images/icons/empresa.png') }}" alt="">
                <span class="menu-title">Empresas</span>
              </a>
            </li> 
                    <?php endif; ?>
              @endif
                @if (Session::has('sesiontipo'))
               <?php if(Session::get('sesiontipo') == 'Usuario'):?> 
             <li class="nav-item">
              <a class="nav-link" href="{{ url('/empresa') }}">
                <img src="{{ url('star/images/icons/empresa.png') }}" alt="">
                <span class="menu-title">Empresas</span>
              </a>
            </li> 
                    <?php endif; ?>
              @endif 
                       <li class="nav-item">
              <a class="nav-link" href="#">
                <img src="{{ url('star/images/icons/6.png') }}" alt="">
                <span class="menu-title">Contabilidad</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#sample-pages" aria-expanded="false" aria-controls="sample-pages">
                 <img src="{{ url('star/images/icons/005-forms.png')}}" alt="">
                <span class="menu-title">Facturas<i class="fa fa-sort-down"></i></span>
              </a>
              <div class="collapse" id="sample-pages">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item">
                    <a class="nav-link" href="" id="XML">
                      <i class="fa fa-upload"></i>Genera tu XML o <br> Factura 
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#" id="modal">
                      <i class="fa fa-upload"></i>Subir archivos 
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ url('SAT') }}">
                      <i class="fa fa-upload"></i>Prueba CFDI
                    </a>
                  </li>
                   <li class="nav-item">
                     <a class="nav-link" href="#" id="modalreport">

                      <i class="fa fa-folder-open-o"></i>Reportes 
                    </a>
                  </li>
                </ul>
              </div>
            </li> 
              <li class="nav-item">
              <a class="nav-link" href="#" id="modalnomina">
                <img src="{{ url('star/images/icons/nomina.png') }}" alt="">
                <span class="menu-title">Nomina</span>
              </a>
            </li>           
              @if (Session::has('sesiontipo'))
               <?php if(Session::get('sesiontipo') == 'Usuario'):?> 
              <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#sample-pagesj" aria-expanded="false" aria-controls="sample-pagesj">
                 <i class="fa fa-spin fa-gear"></i>
                <span class="menu-title">Configuracion<i class="fa fa-sort-down"></i></span>
              </a>
              <div class="collapse" id="sample-pagesj">
                <ul class="nav flex-column sub-menu">
                   <li class="nav-item">
                    <a class="nav-link" href="{{ url('config') }}">
                      <i class="fa fa-user-circle-o"></i>Ver Usuarios 
                    </a>
                  </li>
                </ul>
              </div>
            </li>
                    <?php endif; ?>
              @endif
              <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#sample-pagesk" aria-expanded="false" aria-controls="sample-pages">
                 <img src="{{ url('star/images/icons/ayuda.png') }}" alt="">
                <span class="menu-title">Ayuda<i class="fa fa-sort-down"></i></span>
              </a>
              <div class="collapse" id="sample-pagesk">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item">
                    <a class="nav-link" href="">
                      <i class="fa fa-exclamation-triangle"></i>Acerca de 
                    </a>
                  </li>
                   <li class="nav-item">
                     <a class="nav-link" href="{{ url('/manual') }}">

                      <i class="fa fa-file-pdf-o"></i>Manual de usuario
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ url('/soporteinicio') }}">
                      <i class="fa fa-smile-o"></i>Soporte tecnico
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ url('/comentarios') }}">
                      <i class="fa fa-comments-o"></i>comentarios y/o<br> sugerencias 
                    </a>
                  </li>
                </ul>
              </div>
            </li> 
            
          </ul>
        </nav>
<!-- /////////////////////////// FIN MENU /////////////////////////////////////////////////// -->
<!--      FUNCION PARA MODAL DE FACTURAS EMITIDAS Y RECIBIDAS      -->
    <script type="text/javascript">
        $(document).ready(function(){
           $('#modal').click(function(e){
             e.preventDefault();
             $("#myModal").modal();
           });
           $('#PC').click(function(e){
               e.preventDefault();
               $("#my").modal();
           });
           $('#emitido').click(function(e){
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var url = host+"/Ccfiscal/public/";

            //var url = $("#url").attr('value');
            window.open(url+'dropzoneE');
           });
           $('#reciv').click(function(e){
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var url = host+"/Ccfiscal/public/";

            window.open(url+'dropzoneR');
           });
           $('#SAT').click(function(e){
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var url = host+"/cfdi/ejemplos/html/index.php";

            window.location.replace(url);
           });

        });
    </script>
    <!-- MODAL PARA SELECCIONAR QUE VA A REALIZAR CON LAS FACTUAS -->
    <div class="modal fade" id="myModal" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">
            <div class="modal-content" align="center">
                <div class="modal-header">
                    <h2>¿Que accion desea realizar?</h2>
                </div>
                <div class="modal-body">
                     <div class="row">
                            <div class="col-md-5">
                              <div>
                                  <label>Cargar CFDI !!!</label>
                                  <br />
                                  <button class="btn btn-outline-primary mr-2" id="PC">
                                      <i class="fa fa-laptop"></i> PC
                                  </button>
                                  <button class="btn btn-outline-primary mr-2" id="SAT">
                                      <i class="fa fa-th-large"></i> SAT
                                  </button>
                              </div>
                            </div>
                            <div class="col-md-7">
                              <div>
                                  <label>Descargar CFDI !!!!</label>
                                  <br />
                                   <a href="{{ URL('respaldo') }}">
                                  <button class="btn btn-outline-success mr-2">
                                      <i class="fa fa-cloud-download"></i> Respaldo
                                  </button>
                                  </a>
                                  <button class="btn btn-outline-success mr-2">
                                      <i class="fa fa-cloud-download"></i> Reportes
                                  </button>
                              </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL PARA SELECCIONAR SI SE VA A SUBIR FACTURAS EMITIDAS O RECIBIDAS -->
    <div class="modal fade" id="my" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" align="center">
                <div class="modal-header">
                    OPCIONES
                </div>
                <div class="modal-body">
                    <h2> Tipo de Estado!</h2>
                    <hr>
                     <div class="row">
                       <div class="col-md-6">
                          <button class="btn btn-outline-success mr-2" id="emitido">
                              <i class="fa fa-cloud-upload"></i> Emitidos
                          </button>
                       </div>
                       <div class="col-md-6">
                          <button class="btn btn-outline-warning mr-2" id="reciv">
                              <i class="fa fa-cloud-upload"></i> Recibidos
                          </button>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FUNCION PARA MANDAR AL MODAL DE AÑOS Y RFC -->
        <script type="text/javascript">
        $(document).ready(function(){
           $('#modalreport').click(function(e){
             e.preventDefault();
             $("#reportem").modal();
           });
          
           $('#RF').click(function(e){
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var ano  = $(this).attr("value");
            var url = host+"/Ccfiscal/public/showdir/";
            
            window.location.replace(url+'/'+ano);
           });
           $('#RF2').click(function(e){
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var ano  = $(this).attr("value");
            var url = host+"/Ccfiscal/public/showdir/";
            
            window.location.replace(url+'/'+ano);
           });
           $('#RF3').click(function(e){
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var ano  = $(this).attr("value");
            var url = host+"/Ccfiscal/public/showdir/";
            
            window.location.replace(url+'/'+ano);
           });
           $('#RF4').click(function(e){
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var ano  = $(this).attr("value");
            var url = host+"/Ccfiscal/public/showdir/";
            
            window.location.replace(url+'/'+ano);
           });
           $('#RF5').click(function(e){

            e.preventDefault();

            var host = "http://" + window.location.hostname;

            var ano  = $(this).attr("value");

            var url = host+"/Ccfiscal/public/showdir/";
            
            window.location.replace(url+'/'+ano);

           });
        });
    </script>
    <!-- MODAL PARA SELECCIONAR AÑO -->
        <div class="modal fade" id="reportem" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">
            <div class="modal-content" align="center">
                <div class="modal-header">
                    <h2>Continuar como  </h2>
                </div>
                <div class="modal-body">
                     <div class="row">
                            <div class="col-md-12">
                             
                                  <label>Selecciona Periodo</label>
                                  <br />
                                    <?php  $ano= date("Y"); ?>
                  <button type="button" class="btn btn-outline-secondary mr-4" value='<?= $ano-4; ?>' name="ano" id="RF"><?php echo $ano-4; ?></button>
                  <button type="button" class="btn btn-outline-success mr-4" value='<?= $ano-3; ?>' name="ano" id="RF2"><?php echo $ano-3; ?></button>
                  <button type="button" class="btn btn-outline-info mr-4" value='<?= $ano-2; ?>' name="ano" id="RF3"><?php echo $ano-2; ?></button>
                  <button type="button" class="btn btn-outline-warning mr-4" value='<?= $ano-1; ?>' name="ano" id="RF4"><?php echo $ano-1; ?></button>
                  <button type="button" class="btn btn-outline-danger" value='<?= $ano; ?>' name="ano" id="RF5"><?php echo $ano; ?></button> 
                                  
                              
                            </div>
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- FIN DE LA FUNCION Y MODAL  -->
<!-- INICIO DE LA FUNCION Y MODAL PARA REPORTE DE NOMINAS -->
  <script type="text/javascript">
        $(document).ready(function(){
           $('#modalnomina').click(function(e){
             e.preventDefault();
             $("#nomina").modal();
           });
           $('#PCNO').click(function(e){
               e.preventDefault();
              var host = "http://" + window.location.hostname;
            var url = host+"/Ccfiscal/public/dropNomina";

             window.location.replace(url);
           });
        });
    </script>
    <!-- MODAL PARA CARGAR LAS NOMINAS Y LOS REPORTES-->
    <div class="modal fade" id="nomina" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">
            <div class="modal-content" align="center">
                <div class="modal-header">
                    <h2>¿Que accion desea realizar?</h2>
                </div>
                <div class="modal-body">
                     <div class="row">
                            <div class="col-md-6">
                              <div>
                                  <label>Cargar Nomina!</label>
                                  <br />
                                  <button class="btn btn-outline-primary mr-2" id="PCNO">
                                      <i class="fa fa-laptop"></i> PC
                                  </button>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div>
                                  <label>Descargar Nomina!</label>
                                  <br />
                                  <button class="btn btn-outline-success mr-2" id="reportnomina">
                                      <i class="fa fa-cloud-download"></i> Reportes
                                  </button>
                              </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FUNCION PARA MANDAR AL MODAL DE AÑOS -->
    <script type="text/javascript">
        $(document).ready(function(){
           $('#reportnomina').click(function(e){
             e.preventDefault();
             $("#reportno").modal();
           });
          
           $('#RFC').click(function(e){
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var ano  = $(this).attr("value");
            var url = host+"/Ccfiscal/public/no/";
            
            window.location.replace(url+'/'+ano);
           });
           $('#RFC2').click(function(e){
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var ano  = $(this).attr("value");
            var url = host+"/Ccfiscal/public/no/";
            
            window.location.replace(url+'/'+ano);
           });
           $('#RFC3').click(function(e){
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var ano  = $(this).attr("value");
            var url = host+"/Ccfiscal/public/no/";
            
            window.location.replace(url+'/'+ano);
           });
           $('#RFC4').click(function(e){
            e.preventDefault();
            var host = "http://" + window.location.hostname;
            var ano  = $(this).attr("value");
            var url = host+"/Ccfiscal/public/no/";
            
            window.location.replace(url+'/'+ano);
           });

           $('#RFC5').click(function(e){

            e.preventDefault();

            var host = "http://" + window.location.hostname;

            var ano  = $(this).attr("value");

            var url = host+"/Ccfiscal/public/no/";
            
            window.location.replace(url+'/'+ano);

           });
        });
        
        $(document).ready(function(){

          $('#XML').click(function(e){

            e.preventDefault();

            let confirmacion = confirm("Antes de Empezar prefieres Retirarte ???? ");

            //console.log(confirmacion);
            
            if(confirmacion){

              window.location.href = '';

            }else{

              console.error("Error: " + confirmacion);
              
            }
            

          });

        });
    </script>
    <!-- MODAL PARA MANDAR EL RFC Y EL AÑO EN EL QUE SE TRABAJARA -->
    <div class="modal fade" id="reportno" role="dialog" style="overflow-y: scroll;">
        <div class="modal-dialog">
            <div class="modal-content" align="center">
                <div class="modal-header">
                    <h2>RFC  </h2>
                </div>
                <div class="modal-body">
                     <div class="row">
                            <div class="col-md-12">
                             
                                  <label>Selecciona Periodo</label>
                                  <br />
                                    <?php  $ano= date("Y"); ?>
                  <button type="button" class="btn btn-outline-secondary mr-4" value='<?= $ano-4; ?>' name="ano" id="RFC"><?php echo $ano-4; ?></button>
                  <button type="button" class="btn btn-outline-success mr-4" value='<?= $ano-3; ?>' name="ano" id="RFC2"><?php echo $ano-3; ?></button>
                  <button type="button" class="btn btn-outline-info mr-4" value='<?= $ano-2; ?>' name="ano" id="RFC3"><?php echo $ano-2; ?></button>
                  <button type="button" class="btn btn-outline-warning mr-4" value='<?= $ano-1; ?>' name="ano" id="RFC4"><?php echo $ano-1; ?></button>
                  <button type="button" class="btn btn-outline-danger" value='<?= $ano; ?>' name="ano" id="RFC5"><?php echo $ano; ?></button> 
                                  
                              
                            </div>
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- FINDEL MODAL Y FUNCION NOMIDA -->

  @yield('centro')



    @yield('pie')
  <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="float-right">
                <a href="#">GEMElite</a> &copy; 2018
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
    </div>
</div>
  <script src="{{ url('star/node_modules/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ url('star/node_modules/popper.js/dist/umd/popper.min.js')}}"></script>
  <script src="{{ url('star/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <script src="{{ url('star/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
  <script src="{{ url('star/js/off-canvas.js')}}"></script>
  <script src="{{ url('star/js/hoverable-collapse.js')}}"></script>
  <script src="{{ url('star/js/misc.js')}}"></script>

</body>
</html>