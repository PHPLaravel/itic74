<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="{{ asset('template/css/mdb.min.css') }}">
    <!-- Estilos Personalizados -->
    <link rel="stylesheet" href="{{ asset('template/css/estilos.css') }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark gris scrolling-navbar fixed-top">
  		
		<div class="container">
			<a class="navbar-brand" data-scroll href="#inicio">Secundaria 382</a>
  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    			<span class="navbar-toggler-icon"></span>
  			</button>

 		<div class="collapse navbar-collapse" id="navbarSupportedContent">
    		<ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav">
      			<li class="nav-item">
       				<a class="nav-link" href="{{ asset('template/archivos/calendario escolar.pdf') }}">Calendario Escolar</a>
      			</li>
            <li class="nav-item">
              <a class="nav-link" data-scroll href="#conocenos">Conocenos</a>
            </li>
      			<li class="nav-item">
        			<a class="nav-link" data-scroll href="#instalaciones">Instalaciones</a>
      			</li>
            <li class="nav-item">
              <a class="nav-link" data-scroll href="#avisos">Avisos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-scroll href="#contactos">Contactos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-scroll href="#citas">Agenda tu Cita</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-scroll href="{{ route('login') }}">Sistema</a>
            </li>
    		</ul>
  		</div>
		</div>
	</nav>

  <section class="fondo view" id="inicio">
    <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
      <div class="container">
        <div class="row">
        <div class="col-md-6 text-white">
          <h1 class="h1-responsive">Esc. Sec. Ofic. No. 0382 "Dr. José Ma. Luis Mora"</h1>
          <hr class="bg-light">
          <p class="h4-responsive text-center text-md-left">Te presentamos la página de la Escuela Secundaria Oficial No. 0382 del Ocotal Analco, Estado de México. 
          La intención de la misma es abrir un espacio de comunicación entre el personal académico, alumnos y padres de familia, que nos permita fortalecernos como institución de vanguardia que somos. </p>
        </div>
        
      </div>
      </div>
    </div>
  </section>

<main class="container">
  <section class="section pb-5" id="conocenos">
    <div class="mask rgba-light d-flex justify-content-center align-items-center">
      <h2 class="section-heading h2 pt-4">Conocenos</h2>
    </div>
  </section>
  <section class="row">
    <div class="col-md-6">
      <img src="{{ asset('template/images/inicios.jpg') }}" class="img-fluid z-depth-1-half">
    </div>
    <div class="col-md-6">
      <h3 class="h3">inicios</h3>
      <p>La Escuela Secundaria Ofic. No. 0382 desde su fundación, se erigió como una institución, cuya fundamental meta fue la capacitación del alumno para su incorporación a la vida productiva. 
      Ha pasado por una serie de cambios de sostenimiento, ubicación oficial, así como de estructura curricular, acorde a las modificaciones propias de un Sistema Educativo Progresivo.</p>
    </div>
  </section>
  <hr class="my-5">
  <section>
    <div class="row">
      <div class="col-md-6">
        <h3 class="h3">Misión: </h3>
        <p>
        Ofrecer una educación de calidad, mediante la articulación de los diferentes niveles de educación básica realizando prácticas innovadoras, sustentada en el marco legal vigente, en apego a nuestra identidad nacional, formando individuos competitivos en distintos ámbitos de la vida, incidiendo en la práctica de valores éticos y morales, capaces de adecuarse a los cambios y retos del mundo globalizado.</p>
      </div>
      <div class="col-md-6">
        <img src="{{ asset('template/images/logos/mision.png') }}" class="img-fluid rounded mx-auto d-block">
      </div>
  </section>
  <hr class="my-5">
  <section>
    <div class="row">
      <div class="col-md-6">
        <img src="{{ asset('template/images/logos/vision.png') }}" class="img-fluid rounded mx-auto d-block">
      </div>
      <div class="col-md-6">
        <h3 class="h3">Visión: </h3>
        <p>Ser una  institución de excelencia académica  que trabaja a favor de la calidad educativa de nuestra comunidad, para formar educandos, que desarrollen competencias para la vida, que les permitan insertarse de manera positiva en la sociedad, y ser protagonistas del desarrollo de México en el siglo XXI.</p>
      </div>
    </div>
    </div>
  </section>
  <hr class="my-5">
</main>

<!--Carousel-->
<main class="container">
<section class="section pb-5" id="instalaciones">
  <div class="mask rgba-light d-flex justify-content-center align-items-center">
    <h2 class="section-heading h2 pt-4">Instalaciones</h2>
  </div>
  <!--Carousel Wrapper-->
<div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
  <!--Indicators-->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-2" data-slide-to="1"></li>
    <li data-target="#carousel-example-2" data-slide-to="2"></li>
    <li data-target="#carousel-example-2" data-slide-to="3"></li>
    <li data-target="#carousel-example-2" data-slide-to="4"></li>
    <li data-target="#carousel-example-2" data-slide-to="5"></li>
    <li data-target="#carousel-example-2" data-slide-to="6"></li>
    <li data-target="#carousel-example-2" data-slide-to="7"></li>
    <li data-target="#carousel-example-2" data-slide-to="8"></li>
    <li data-target="#carousel-example-2" data-slide-to="9"></li>
    <li data-target="#carousel-example-2" data-slide-to="10"></li>
  </ol>
  <!--/.Indicators-->
  <!--Slides-->
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <div class="view">
        <img class="d-block w-100" src="{{ asset('template/images/carousel/aula.jpg') }}"
          alt="First slide">
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Aulas</h3>
        <p>contamos con instalaciones de buena calidad</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('template/images/carousel/biblioteca.jpg') }}"
          alt="Second slide">
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Biblioteca</h3>
        <p>Contamos con una biblioteca para que nuestros alumnos puedan consultar información necesaria</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('template/images/carousel/area_administrativa.jpg') }}"
          alt="Second slide">
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Area Administrativa</h3>
        <p>Secondary text</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('template/images/carousel/direccion.jpg') }}"
          alt="Second slide">
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Dirección Escolar</h3>
        <p>oficina del director</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('template/images/carousel/cafeteria.jpg') }}"
          alt="Third slide">
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Cafeteria</h3>
        <p>Ofrecemos servicio de cafeteria tanto para alumnos como profesores</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('template/images/carousel/basquet.jpg') }}"
          alt="First slide">
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Canchas de basquetbol</h3>
        <p>Contamos con canchas de basquetbol para asi fomentar el deporte en nuestros alumnos</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('template/images/carousel/futbol.jpg') }}"
          alt="Second slide">
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Canchas de futbol</h3>
        <p>Tambien contamos con canchas de futbol para nuestros alumnos</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('template/images/carousel/aula_usos_multiples.jpg') }}"
          alt="Second slide">
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Aula de usos multiples</h3>
        <p>Secondary text</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('template/images/carousel/Computo.jpg') }}"
          alt="Third slide">
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Taller de computación</h3>
        <p>Actividad tecnológica que te permite conocer y capacitarte en el manejo de las máquinas de escribir, taquigrafía, correspondencia, entre otras.</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('template/images/carousel/manualidades.jpg') }}">
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Laboratorio multimedia</h3>
        <p>Contamos con equipos de cómputo conectados a internet, equipo de televisión, videos educativos y diverso material impreso a fin de dar uso a las tecnologías en el aula.</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('template/images/carousel/laboratorio.jpg') }}">
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Laboratorio</h3>
        <p>Contamos con el equipo necesario para practicas escolares de las materias de Biologia, Fisica y Quimica</p>
      </div>
    </div>
  </div>
  <!--/.Slides-->
  <!--Controls-->
  <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  </a>
  <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
  </a>
  <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->
</section>
<hr class="my-5">
<!-- Termina Carousel -->
</main>
<main class="container">
<!-- Inicia Avisos-->
<section class="section pb-5" id="avisos">
 <div class="mask rgba-light d-flex justify-content-center align-items-center">
    <h2 class="section-heading h2 pt-4">Avisos</h2>
  </div>
  <!-- Grid row -->
  @php
  $avisos = \App\Avisos::all();
  @endphp
<div class="row">

  <!-- Grid column -->
  @foreach ($avisos as $a)
  <div class="col-lg-4 col-md-6">

    <!--Card Narrower-->
    <div class="card card-cascade narrower">

      <!--Card image-->
      <div class="view view-cascade overlay">
        <!--<img src="{{ asset('template/images/avisos/honores.jpg') }}" class="card-img-top"
          alt="narrower">-->
          <img src="{{ $a->ruta }}/{{ $a->Foto }}" class="card-img-top"
          alt="narrower">
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>
      <!--/.Card image-->

      <!--Card content-->
      <div class="card-body card-body-cascade">
        <h5 class="green-text"><i class="far fa-sticky-note"></i> Avisos</h5>
        <!--Title-->
        <h4 class="card-title">{{ $a->avisoEscolar }}</h4>
        <!--Text-->
        <!--<p class="card-text">Los honores a la bandera estaran a cargo del grupo 3° "A"</p>-->
        <!--<a class=""  value="{{ $a->id }}">Ver Más</a>-->
        <button class="btn btn-success variable" data-toggle="modal" data-target="#myModal" value="{{ $a->id }}">Ver Más</button>
      </div>
      <!--/.Card content-->
      
    </div>
    <!--/.Card Narrower-->
    
  </div>
  @endforeach
  <!-- Modal column -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <!-- Modal column -->
  
  <!-- Grid column -->
</div>
<!-- Grid row -->
</section>
<hr class="my-5">
<!-- Termina Avisos-->

</main>
<!-- Inicio Documentos -->
<main class="container">
<!-- Inicia Avisos-->
<section class="section pb-5" id="avisos">
 <div class="mask rgba-light d-flex justify-content-center align-items-center">
    <h2 class="section-heading h2 pt-4">Documentos</h2>
  </div>
  <!-- Grid row -->
  @php
  $d = \App\Documents::all();
  @endphp
<div class="row">

  <!-- Grid column -->
  @foreach ($d as $doc)
  <div class="col-lg-4 col-md-6">

    <!--Card Narrower-->
    <div class="card card-cascade narrower">

      <!--Card image-->
      <div class="view view-cascade overlay">
        <!--<img src="{{ asset('template/images/avisos/honores.jpg') }}" class="card-img-top"
          alt="narrower">-->
          @if($doc->extencion == 'xlsx' or $doc->extencion = 'xls')
          	<img src="https://lh3.googleusercontent.com/proxy/6PT-UodJvq0lvQ6XwGInZymihmCZEc-ixoQIOxFny39dJNnrJbuZ_hj1qLq7jbwRqwRn6FMTR28IDHc6h0vsffBz_b-PPOv1T8VSnq5aVJfU4q5Mh1riVTrHQXu9ZT_iM4tIGapuw_aGJ0cT2084fUDcyl6R" class="card-img-top"
          alt="narrower">
          @endif
          
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>
      <!--/.Card image-->

      <!--Card content-->
      <div class="card-body card-body-cascade">
        <h5 class="green-text"><i class="far fa-sticky-note"></i> Documento</h5>
        <!--Title-->
        <h4 class="card-title">{{ $doc->name }}</h4>
        <!--Text-->
        <!--<p class="card-text">Los honores a la bandera estaran a cargo del grupo 3° "A"</p>-->
        <!--<a class=""  value="{{ $a->id }}">Ver Más</a>-->
        <a href="download/{{ $doc->id }}">
        	<button class="btn btn-success documento">Ver Más</button>
    	</a>
      </div>
      <!--/.Card content-->
      
    </div>
    <!--/.Card Narrower-->
    
  </div>
  @endforeach
  <!-- Modal column -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <!-- Modal column -->
  
  <!-- Grid column -->
</div>
<!-- Grid row -->
</section>
<hr class="my-5">
<!-- Termina Avisos-->
</main>
<!-- Fin Documentos -->

<main class="container">
<!-- Inicia Contactos-->

  <!--Section: Contact v.1-->
<section class="section pb-5" id="contactos">
<div class="mask rgba-light d-flex justify-content-center align-items-center">
  
  <!--Section heading-->
  <h2 class="section-heading h2 pt-4">Contactos</h2>
</div>
  <!--Section description-->
  <p class="section-description pb-4">Llene el formulario a continuación, realiza tu consulta, como deseas ser contactado y nos pondremos en contacto con usted durante nuestras horas de trabajo.</p>

  <div class="row">

    <!--Grid column-->
    <div class="col-lg-5 mb-4">

      <!--Form with header-->
      <div class="card">

        <div class="card-body">
          <!--Header-->
          <div class="form-header accent-1">
            <h3><i class="fas fa-envelope"></i> Escríbenos:</h3>
          </div>

          
          @if( session()->has('msg') )
            <p>{{ session()->get('msg') }}.</p>
          @endif
          <br>
          <form action="{{ route('mail') }}" method="POST" id="correo">
            @csrf
            <!--Body-->
            <div class="md-form">
              <i class="fas fa-user prefix grey-text"></i>
              <input type="text" id="form-name" class="form-control" name="name">
              <label for="form-name">Su Nombre</label>
            </div>

            <div class="md-form">
              <i class="fas fa-envelope prefix grey-text"></i>
              <input type="text" id="form-email" class="form-control" name="email">
              <label for="form-email">Su Correo</label>
            </div>

            <div class="md-form">
              <i class="fas fa-tag prefix grey-text"></i>
              <input type="text" id="form-Subject" class="form-control" name="subject">
              <label for="form-Subject">Asunto</label>
            </div>

            <div class="md-form">
              <i class="fas fa-pencil-alt prefix grey-text"></i>
              <textarea name="message" id="form-text" class="form-control md-textarea" rows="3"></textarea>
              <label for="form-text">Mensaje</label>
            </div>

            <div class="text-center mt-4">
              <button type="submit" class="btn btn-success">Enviar</button>
            </div>
          </form>
        </div>

      </div>
      <!--Form with header-->

    </div>
    <!--Grid column-->

    <!-- Avisos Grid column-->

    <div class="mask rgba-light d-flex justify-content-center align-items-center">
  
      <!--Section heading-->
      <h2 class="section-heading h2 pt-4">Agenda tu Cita..!!</h2>

    </div>

    <p class="section-description pb-4">Llene el formulario a continuación, para agendar tu cita.</p>

    <div class="col-lg-5 mb-4">

      <!--Form with header-->
      <div class="card">

        <div class="card-body">
          <!--Header-->
          <div class="form-header accent-1">
            <h3><i class="fas fa-envelope"></i> Escríbenos:</h3>
          </div>

          
          @if( session()->has('msg') )
            <p>{{ session()->get('msg') }}.</p>
          @endif
          <br>
          <form action="{{ route('citas.store') }}" method="POST" >
            @csrf
            <!--Body-->
            <div class="md-form">
              <i class="fas fa-calendar-alt prefix grey-text"></i>
              <input type="date" id="form-name" class="form-control" name="dia">
              <label for="form-name">Su Fecha</label>
              <span style="color: red;">{!! $errors->first('dia') !!}</span>
            </div>

            <div class="md-form">
              <i class="fas fa-user prefix grey-text"></i>
              <input type="text" id="form-Subject" class="form-control" name="name">
              <label for="form-Subject">Su Nombre</label>
              <span style="color: red;">{!! $errors->first('name') !!}</span>
            </div>

            <div class="md-form">
              <i class="fas fa-phone prefix grey-text"></i>
              <input type="text" name="telefono" class="form-control">
              <label for="form-text">Su Telefono</label>
              <span style="color: red;">{!! $errors->first('telefono') !!}</span>
            </div>

            <div class="md-form">
              <i class="fas fa-envelope prefix grey-text"></i>
              <input type="text" id="form-email" class="form-control" name="email">
              <label for="form-email">Su Correo</label>
              <span style="color: red;">{!! $errors->first('email') !!}</span>
            </div>

            <div class="md-form">
              <i class="fas fa-pencil-alt prefix grey-text"></i>
              <textarea name="asunto" id="" cols="30" rows="10" class="form-control"></textarea>
              <label for="form-text">Su Asunto</label>
              <span style="color: red;">{!! $errors->first('asunto') !!}</span>
            </div>

            <div class="md-form">
              <i class="fas fa-pencil-alt prefix grey-text"></i>
              <select name="roleschool_id" id="roleschool_id" class="form-control">
                
              </select>
              <label for="form-text">Area</label>
              <span style="color: red;">{!! $errors->first('roleschool_id') !!}</span>
            </div>

            <div class="text-center mt-4">
              <button type="submit" class="btn btn-success">Reservar Cita...!!!</button>
            </div>
          </form>
        </div>

      </div>
      <!--Form with header-->

    </div>
    <!-- Avisos Grid column-->

    <!--Grid column-->
    <div class="col-lg-7">

      <!--Google map-->
      <div id="map-container-google-11" class="z-depth-1-half map-container-6" style="height: 400px">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14697.806361088824!2d-99.45730427713568!3d19.339501585468167!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4fa735d7c949a36c!2sOFIC.%20SEC.%200382%20%22DR.%20JOSE%20MA.%20LUIS%20MORA%22!5e0!3m2!1ses-419!2smx!4v1574792382693!5m2!1ses-419!2smx"
          frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <br>
      <!--Buttons-->
      <div class="row text-center">
        <div class="col-md-4">
          <a class="btn-floating accent-1"><i class="fas fa-map-marker-alt"></i></a>
          <p>Santiago Analco, Méx 52047</p>
          <p>México</p>
        </div>

        <div class="col-md-4">
          <a class="btn-floating accent-1"><i class="fas fa-phone"></i></a>
          <p>+52 729 324 0172</p>
          <p>Lunes - Viernes, 7:00-16:00</p>
        </div>

        <div class="col-md-4">
          <a class="btn-floating accent-1"><i class="fas fa-envelope"></i></a>
          <p>info@gmail.com</p>
          <p>sale@gmail.com</p>
        </div>
      </div>

    </div>
    <!--Grid column-->

  </div>

</section>
<!--Section: Contact v.1-->
<!-- Termina Contactos-->

<!-- Inicia Citas-->
<section class="section pb-5" id="citas">
<div class="mask rgba-light d-flex justify-content-center align-items-center">
  
  <!--Section heading-->
  <h2 class="section-heading h2 pt-4">Contactos</h2>
</div>
  <!--Section description-->
  <p class="section-description pb-4">Llene el formulario a continuación, realiza tu consulta, como deseas ser contactado y nos pondremos en contacto con usted durante nuestras horas de trabajo.</p>

  <div class="row">

    <!--Grid column-->

    <!-- Avisos Grid column-->
    @php
      use \App\RoleSchool;

      $rol = RoleSchool::all();
    @endphp
    <div class="mask rgba-light d-flex justify-content-center align-items-center">
  
      <!--Section heading-->
      <h2 class="section-heading h2 pt-4">Agenda tu Cita..!!</h2>

    </div>

    <p class="section-description pb-4">Llene el formulario a continuación, para agendar tu cita.</p>

    <div class="col-lg-5 mb-4">

      <!--Form with header-->
      <div class="card">

        <div class="card-body">
          <!--Header-->
          <div class="form-header accent-1">
            <h3><i class="fas fa-envelope"></i> Escríbenos:</h3>
          </div>

          
          @if( session()->has('msg') )
            <p>{{ session()->get('msg') }}.</p>
          @endif
          <br>
          <form action="{{ route('citas.store') }}" method="POST" >
            @csrf
            <!--Body-->
            <div class="md-form">
              <i class="fas fa-calendar-alt prefix grey-text"></i>
              <input type="date" id="form-name" class="form-control" name="dia">
              <label for="form-name">Su Fecha</label>
              <span style="color: red;">{!! $errors->first('dia') !!}</span>
            </div>

            <div class="md-form">
              <i class="fas fa-user prefix grey-text"></i>
              <input type="text" id="form-Subject" class="form-control" name="name">
              <label for="form-Subject">Su Nombre</label>
              <span style="color: red;">{!! $errors->first('name') !!}</span>
            </div>

            <div class="md-form">
              <i class="fas fa-phone prefix grey-text"></i>
              <input type="text" name="telefono" class="form-control">
              <label for="form-text">Su Telefono</label>
              <span style="color: red;">{!! $errors->first('telefono') !!}</span>
            </div>

            <div class="md-form">
              <i class="fas fa-envelope prefix grey-text"></i>
              <input type="text" id="form-email" class="form-control" name="email">
              <label for="form-email">Su Correo</label>
              <span style="color: red;">{!! $errors->first('email') !!}</span>
            </div>

            <div class="md-form">
              <i class="fas fa-pencil-alt prefix grey-text"></i>
              <textarea name="asunto" id="" cols="30" rows="10" class="form-control"></textarea>
              <label for="form-text">Su Asunto</label>
              <span style="color: red;">{!! $errors->first('asunto') !!}</span>
            </div>

            <div class="md-form">
              <i class="fas fa-pencil-alt prefix grey-text"></i>
              <select name="roleschool_id" id="roleschool_id" class="form-control">
                @foreach ($rol as $i)
                  <option value="{{ $i->id }}">{{ $i->roles }}</option>
                @endforeach
              </select>
              <label for="form-text">Area</label>
              <span style="color: red;">{!! $errors->first('roleschool_id') !!}</span>
            </div>

            <div class="text-center mt-4">
              <button type="submit" class="btn btn-success">Reservar Cita...!!!</button>
            </div>
          </form>
        </div>

      </div>
      <!--Form with header-->

    </div>
    <!-- Avisos Grid column-->

    <!--Grid column-->
    <div class="col-lg-7">

      <!--Google map-->
      <div id="map-container-google-11" class="z-depth-1-half map-container-6" style="height: 400px">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14697.806361088824!2d-99.45730427713568!3d19.339501585468167!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4fa735d7c949a36c!2sOFIC.%20SEC.%200382%20%22DR.%20JOSE%20MA.%20LUIS%20MORA%22!5e0!3m2!1ses-419!2smx!4v1574792382693!5m2!1ses-419!2smx"
          frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <br>
      <!--Buttons-->
      <div class="row text-center">
        <div class="col-md-4">
          <a class="btn-floating accent-1"><i class="fas fa-map-marker-alt"></i></a>
          <p>Santiago Analco, Méx 52047</p>
          <p>México</p>
        </div>

        <div class="col-md-4">
          <a class="btn-floating accent-1"><i class="fas fa-phone"></i></a>
          <p>+52 729 324 0172</p>
          <p>Lunes - Viernes, 7:00-16:00</p>
        </div>

        <div class="col-md-4">
          <a class="btn-floating accent-1"><i class="fas fa-envelope"></i></a>
          <p>info@gmail.com</p>
          <p>sale@gmail.com</p>
        </div>
      </div>

    </div>
    <!--Grid column-->

  </div>

</section>
</main>

<!-- Cierre Citas-->

<!-- Footer -->
<footer class="page-footer font-small green pt-4">

  <!-- Footer Links -->
  <div class="container">

    <!-- Grid row-->
    <div class="row text-center d-flex justify-content-center pt-5 mb-3">

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="#inicio">Inicio</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a href="{{ asset('template/archivos/calendario escolar.pdf') }}">Calendario Escolar</a>
        </h6>
      </div>
      <!-- Grid column -->
      
      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="#conocenos">Conocenos</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="#instalaciones">Instalaciones</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="#avisos">Avisos</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="#contactos">Contactos</a>
        </h6>
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-3">
        <h6 class="text-uppercase font-weight-bold">
          <a data-scroll href="{{ route('login') }}">Sistema</a>
        </h6>
      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row-->
    <hr class="rgba-white-light" style="margin: 0 15%;">

    <!-- Grid row-->
    <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">

      <!-- Grid column 
      <div class="col-md-8 col-12 mt-5">
        <p style="line-height: 1.7rem">Sed ut perspiciatis unde omnis iste natus error sit voluptatem
          accusantium doloremque laudantium, totam rem
          aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
          explicabo.
          Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</p>
      </div>
       Grid column -->

    </div>
    <!-- Grid row-->
    <hr class="clearfix d-md-none rgba-white-light" style="margin: 10% 15% 5%;">

  </div>
  <!-- Footer Links -->

 <!-- Copyright -->
  <div class="footer-copyright text-center py-3">Esc. Sec. Ofic. No. 0382 "Dr. José Ma. Luis Mora"<br>© 2019 Todos los derechos reservados
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->

	<!-- JQuery -->
<script src="{{ asset('template/js/jquery.min.js') }}"></script>
<!-- Bootstrap tooltips -->
<script src="{{ asset('template/js/popper.min.js') }}"></script>
<!-- Bootstrap core JavaScript -->
<script src="{{ asset('template/js/bootstrap.min.js') }}"></script>
<!-- MDB core JavaScript -->
<script src="{{ asset('template/js/mdb.min.js') }}"></script>
<!-- Get patch fixes within a minor version -->
<script src="{{ asset('template/js/smooth-scroll.polyfills.min.js') }}"></script>

<script>
  var scroll = new SmoothScroll('a[href*="#"]', {
    offset: 50,
    speed: 1000
  });

  $('.variable').click(function(){

    //e.preventDefault();

    var variable = parseInt(this.value)

    

     $.ajax({

      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },

      url:'{{ route('avisos.show') }}',

      type:'POST',

      data:{id:variable},

      success:function(ok){
        //$('.modal-body').append() 
        
        let data = `<table class='table-hover' width='100%'>
                        <thead>
                        </thead>
                        <tbody>
                          <tr>
                            <td colspan='4'>
                              <center><img src='${ok.ruta}/${ok.Foto}' width='200px' height='200px'></center>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              Mensaje: ${ok.avisoEscolar}
                            </td>
                          </tr>
                        </tbody>
                    </table>`
        $('.modal-body').html(data)
      },
      error:function(error){
        alert()
      }

     })

  })




</script>

</body>
</html>
