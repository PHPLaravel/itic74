@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Materias &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>array('materias.update',$edit->idMateria), 'method' => 'POST']) !!}
                  	@method('PUT')
						<div class="form-group">
							{!! Form::label('cmateria', 'Clave de Materia') !!}

							{!! Form::text('claveDemateria',$edit->claveDemateria,['class'=>'form-control','placeholder'=>'materia']) !!}
							<span style="color: red;">{!! $errors->first('claveDemateria') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('nombre', 'Nombre Materia') !!}

							{!! Form::text('nombreDelamateria',$edit->nombreDelamateria,['class'=>'form-control','placeholder'=>'nombre']) !!}
							<span style="color: red;">{!! $errors->first('nombreDelamateria') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('creditos', 'Creditos') !!}

							{!! Form::text('creditos',$edit->creditos,['class'=>'form-control','placeholder'=>'apellidos']) !!}
							<span style="color: red;">{!! $errors->first('creditos') !!}</span>
						</div>
						{!! Form::submit('Update!',['class'=>'btn btn-info']) !!}
					{!! Form::close() !!}
				
            </div>
        </div>
    </div>
@stop

@section('script')

@stop
