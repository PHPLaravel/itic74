@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
            @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Materias &nbsp;
        			<a href="{{ route('materias.create') }}"> 
        				<button class="btn btn-success">
        					New
        				</button>
        			</a>
        		</p></h3>
        	</div>
            <div class="card-body">
                  <table class="table table-hover">
                  	<thead>
                  		<tr>
                  			<th>Clave de Materia</th>
                  			<th>Nombre</th>
                  			<th>Creditos</th>
                  			<th>Options</th>
                  		</tr>
                  	</thead>
                  	<tbody>
                  		@forelse ($materias as $i)
                  			<tr>
                  				<td>
                  					{!! $i->claveDemateria !!}
                  				</td>
                  				<td>
                  					{!! $i->nombreDelamateria !!}
                  				</td>
                  				<td>
                  					{!! $i->creditos !!}
                  				</td>

                  				<td>
                  					<a href="{{ route('materias.edit', $i->idMateria ) }}">
                  						<button class="btn btn-info">Edit</button>
                  					</a>
                  					&nbsp;
                  					<form action="{{ route('materias.destroy', $i->idMateria ) }}" method="POST">
                  						@csrf

										{{ method_field('DELETE') }}
                  						<button class="btn btn-danger">Delete</button>
                  					</form>
                  				</td>
                  			</tr>
                  		@empty
                  			<p>Sin Registros...</p>	
                  		@endforelse
                  	</tbody>
                  </table>
            </div>
        </div>
    </div>
@stop
@section('js')
@stop