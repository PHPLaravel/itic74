@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Materias &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>'materias.store', 'method' => 'POST']) !!}
						<div class="form-group">
							{!! Form::label('cmateria', 'Clave de Materia') !!}

							{!! Form::text('claveDemateria',$llavequesigue,['class'=>'form-control','placeholder'=>'materia','readonly'=>'readonly']) !!}
							<span style="color: red;">{!! $errors->first('claveDemateria') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('nombre', 'Nombre Materia') !!}

							{!! Form::text('nombreDelamateria',null,['class'=>'form-control','placeholder'=>'nombre']) !!}
							<span style="color: red;">{!! $errors->first('nombreDelamateria') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('creditos', 'Creditos') !!}

							{!! Form::text('creditos',null,['class'=>'form-control','placeholder'=>'creditos']) !!}
							<span style="color: red;">{!! $errors->first('creditos') !!}</span>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop