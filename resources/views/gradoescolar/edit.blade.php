@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Grado Escolar &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>array('gradoescolar.update',$edit->id), 'method' => 'POST']) !!}
                    @method('PUT')
                  		<div class="form-group">
							{!! Form::label('cmateria', 'ID') !!}

							{!! Form::text('id',$edit->id,['class'=>'form-control','placeholder'=>'id','readonly']) !!}
                             <span style="color: red;">{!! $errors->first('id') !!}</span>
						</div>
						<div class="form-group">

							{!! Form::label('cmateria', 'Grado Escolar') !!}

							{!! Form::text('gradoEscolar',$edit->gradoEscolar,['class'=>'form-control','placeholder'=>'grado']) !!}
                            <span style="color: red;">{!! $errors->first('gradoEscolar') !!}</span>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop