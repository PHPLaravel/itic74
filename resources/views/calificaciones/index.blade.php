@extends('layout.layout')

@section('centro')
<div class="col-md-12">
	@if( session()->has('ok') )
	<div class="alert alert-success" role="alert">
	  {{ session()->get('ok') }}
	</div>
	@elseif(session()->has('error'))
	<div class="alert alert-danger" role="alert">
  	  {{ session()->get('error') }}
	</div>
	@endif
	<div class="card card-statistics">
		<div class="card-header">
			<h3><p>
        			Calificacion &nbsp;
        			<a href="{{ route('calificaciones.create') }}"> 
        				<button class="btn btn-success">
        					New
        				</button>
        			</a>
        	</p></h3>
		</div>
		<div class="card-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Alumno</th>
						<th>Docente</th>
						<th>Especialidad</th>
						<th>Clave de Materia</th>
						<th>Materia</th>
						<th>Calificacion primer bimestre</th>
						<th>Calificacion segundo bimestre</th>
						<th>Calificacion tercero bimestre</th>
						<th>Calificacion cuarto bimestre</th>
						<th>Calificacion quinto bimestre</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($calificacion as $i)
					<tr>
						<td>
							{{ $i->alumnos->nombre }}
						</td>
						<td>
							{{ $i->docentes->nombre }}
						</td>
						<td>
							{{ $i->docentes->especialidadOmateria }}
						</td>
						<td>
							{{ $i->materias->claveDemateria }}
						</td>
						<td>
							{{ $i->materias->nombreDelamateria }}
						</td>
						<td>
							{{ $i->C1 }}
						</td>
						<td>
							{{ $i->C2 }}
						</td>
						<td>
							{{ $i->C3 }}
						</td>
						<td>
							{{ $i->C4 }}
						</td>
						<td>
							{{ $i->C5 }}
						</td>
						<td>
							<a href="{{ route('calificaciones.edit',$i->idCalificacion) }}">
								<button class="btn btn-info">
									Edit
								</button>
							</a>
							&nbsp;
							<form action="{{ route('calificaciones.destroy',$i->idCalificacion) }}" method="POST">
								@csrf
								{{ method_field('DELETE') }}
								<button class="btn btn-danger">
									Delete
								</button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@stop