@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Calificaciones &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>array('calificaciones.update',$edit->idCalificacion), 'method' => 'POST']) !!}
                  	@method('PUT')

						<div class="form-group">
							{!! Form::label('docente', 'Alumno') !!}
							<select class="form-control" name="idA">
								<option value="{{ $edit->alumnos->idA }}">
                  					{{ $edit->alumnos->nombre }}
                				</option>
                				@foreach ($alumno as $a)
                				<option value="{{ $a->idA }}">
                					{{ $a->nombre }}
                				</option>
                				@endforeach
							</select>
							<span style="color:red;">{!! $errors->first('idA') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Docente') !!}
							<select class="form-control" name="idD">
								<option value="{{ $edit->docentes->idD }}">
                  					{{ $edit->docentes->nombre }}
                				</option>
                				@foreach ($docentes as $d)
                				<option value="{{ $d->idD }}">
                					{{ $d->nombre }}
                				</option>
                				@endforeach
							</select>
							<span style="color:red;">{!! $errors->first('idD') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Materia') !!}
							<select class="form-control" name="idMateria">
								<option>
									{{ $edit->materias->nombreDelamateria }}
								</option>
								@foreach ($materia as $m)
									<option value="{{ $m->idMateria }}">
										{{ $m->nombreDelamateria }}
									</option>
								@endforeach
							</select>
							<span style="color:red;">{!! $errors->first('idMateria') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Primer Parcial') !!}

							{!! Form::text('C1',$edit->C1,['class'=>'form-control','placeholder'=>'calificacion']) !!}
							<span style="color:red;">{!! $errors->first('C1') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Segundo Parcial') !!}

							{!! Form::text('C2',$edit->C2,['class'=>'form-control','placeholder'=>'calificacion']) !!}
							<span style="color:red;">{!! $errors->first('C2') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Tercer Parcial') !!}

							{!! Form::text('C3',$edit->C3,['class'=>'form-control','placeholder'=>'calificacion']) !!}
							<span style="color:red;">{!! $errors->first('C3') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Cuarto Parcial') !!}

							{!! Form::text('C4',$edit->C4,['class'=>'form-control','placeholder'=>'calificacion']) !!}
							<span style="color:red;">{!! $errors->first('C4') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Quinto Parcial') !!}

							{!! Form::text('C5',$edit->C5,['class'=>'form-control','placeholder'=>'calificacion']) !!}
							<span style="color: red;">{!! $errors->first('C5') !!}</span>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-info']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop