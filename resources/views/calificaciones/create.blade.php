@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Horari de Alumnos &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>'calificaciones.store', 'method' => 'POST']) !!}
						<div class="form-group">
							{!! Form::label('docente', 'Alumno') !!}
							<select class="form-control" name="idA">
                            	@foreach ($alumnos as $i)
                					<option value="{{ $i->idA }}">
                						{{ $i->nombre }}
                					</option>
              					@endforeach
							</select>
							<span style="color:red;">{!! $errors->first('idA') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Docente') !!}
							<select class="form-control" name="idD">
								@foreach ($docentes as $d)
								<option value="{{ $d->idD }}">
									{{ $d->nombre }}
								</option>
								@endforeach
							</select>
							<span style="color:red;">{!! $errors->first('idD') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Materia') !!}
							<select class="form-control" name="idMateria">
								@foreach ($materias as $m)
                					<option value="{{ $m->idMateria }}">
                  						{{ $m->claveDemateria }}
                  							&nbsp;
                  						{{ $m->nombreDelamateria }}
                					</option>
              					@endforeach
							</select>
							<span style="color:red;">{!! $errors->first('idMateria') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Primer Parcial') !!}

							
							{{ Form::text('C1',null, ['class'=>'form-control','placeholder'=>'calificacion'] ) }}
							<span style="color:red;">{!! $errors->first('C1') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Segundo Parcial') !!}

							
							{{ Form::text('C2',null, ['class'=>'form-control','placeholder'=>'calificacion'] ) }}
							<span style="color:red;">{!! $errors->first('C2') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Tercero Parcial') !!}

							
							{{ Form::text('C3',null, ['class'=>'form-control','placeholder'=>'calificacion'] ) }}
							<span style="color:red;">{!! $errors->first('C3') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Cuarto Parcial') !!}

							
							{{ Form::text('C4',null, ['class'=>'form-control','placeholder'=>'calificacion'] ) }}
							<span style="color:red;">{!! $errors->first('C4') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Quinto Parcial') !!}

							
							{{ Form::text('C5',null, ['class'=>'form-control','placeholder'=>'calificacion'] ) }}
							<span style="color: red;">{!! $errors->first('C5') !!}</span>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop