@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Alumnos &nbsp;
        			<a href="{{ route('alumno.create') }}"> 
        				<button class="btn btn-success">
        					New
        				</button>
        			</a>
              &nbsp;

        		</p></h3>
        	</div>
            <div class="card-body">
                  <table class="table table-hover">
                  	<thead>
                  		<tr>
                  			<th>Matricula</th>
                  			<th>Nombre</th>
                  			<th>Apellidos</th>
                  			<th>Email</th>
                  			<th>Options</th>
                  		</tr>
                  	</thead>
                  	<tbody>
                  		@forelse ($alumnos as $i)
                  			<tr>
                  				<td>
                  					{!! $i->matricula !!}
                  				</td>
                  				<td>
                  					{!! $i->nombre !!}
                  				</td>
                  				<td>
                  					{!! $i->apellidos !!}
                  				</td>
                  				<td>
                  					{!! $i->email !!}
                  				</td>
                  				<td>
                  					<a href="{{ route('alumno.edit', $i->idA ) }}">
                  						<button class="btn btn-info">Edit</button>
                  					</a>
                  					&nbsp;
                  					<form action="{{ route('alumno.destroy', $i->idA ) }}" method="POST">
                  						@csrf

										          {{ method_field('DELETE') }}
                  						<button class="btn btn-danger">Delete</button>
                  					</form>
                  				</td>
                  			</tr>
                  		@empty
                  			<p>Sin Registros...</p>	
                  		@endforelse
                  	</tbody>
                  </table>
            </div>
        </div>
    </div>
@stop

@section('script')

@stop

