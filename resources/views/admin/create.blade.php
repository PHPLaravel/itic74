@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
		        @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Alumnos &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>'alumno.store', 'method' => 'POST']) !!}
						<div class="form-group">
							{!! Form::label('matricula', 'Matricula') !!}

							{!! Form::text('matricula',null,['class'=>'form-control','placeholder'=>'matricula']) !!}
							<span style="color: red;">{!! $errors->first('matricula') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('nombre', 'Nombre') !!}

							{!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'nombre']) !!}
							<span style="color: red;">{!! $errors->first('nombre') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('apellidos', 'Apellidos') !!}

							{!! Form::text('apellidos',null,['class'=>'form-control','placeholder'=>'apellidos']) !!}
							<span style="color: red;">{!! $errors->first('apellidos') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('telefono', 'Telefono') !!}

							{!! Form::text('telefono',null,['class'=>'form-control','placeholder'=>'telefono']) !!}
							<span style="color: red;">{!! $errors->first('telefono') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('telefonodecasa', 'Telefono de Casa') !!}

							{!! Form::text('homePhone',null,['class'=>'form-control','placeholder'=>'telefonodecasa']) !!}
							<span style="color: red;">{!! $errors->first('homePhone') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('Email:') !!}
							{!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Enter Email']) !!}
							<span style="color: red;">{!! $errors->first('email') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('curp', 'Curp') !!}

							{!! Form::text('curp',null,['class'=>'form-control','placeholder'=>'curp']) !!}
							<span style="color: red;">{!! $errors->first('curp') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('estatus', 'Estatus') !!}

							<input type="radio" name="estatus" value="1" data-toggle="toggle">Active
							<input type="radio" name="estatus" value="0" data-toggle="toggle">Inactivo
							<span style="color: red;">{!! $errors->first('estatus') !!}</span>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop