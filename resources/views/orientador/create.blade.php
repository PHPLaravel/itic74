@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Orientadores &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>'orientador.store', 'method' => 'POST']) !!}
						<div class="form-group">
							{!! Form::label('matricula', 'Nombre') !!}

							{!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'nombre']) !!}
							<span style="color: red;">{!! $errors->first('nombre') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('nombre', 'Apellido Paterno') !!}

							{!! Form::text('apellidoPaterno',null,['class'=>'form-control','placeholder'=>'apellidoPaterno']) !!}
							<span style="color: red;">{!! $errors->first('apellidoPaterno') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('apellidos', 'Apellido Materno') !!}

							{!! Form::text('apellidoMaterno',null,['class'=>'form-control','placeholder'=>'apellidoMaterno']) !!}
							<span style="color: red;">{!! $errors->first('apellidoMaterno') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('telefono', 'Telefono') !!}

							{!! Form::text('telefono',null,['class'=>'form-control','placeholder'=>'telefono']) !!}
							<span style="color: red;">{!! $errors->first('telefono') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('Email:') !!}
							{!! Form::text('correo', null, ['class'=>'form-control', 'placeholder'=>'Enter Email']) !!}
							<span style="color: red;">{!! $errors->first('correo') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('sexo', 'Sexo') !!}

							{!! Form::text('sexo',null,['class'=>'form-control','placeholder'=>'sexo']) !!}
							<span style="color: red;">{!! $errors->first('sexo') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('calle', 'calle') !!}

							{!! Form::text('calle',null,['class'=>'form-control','placeholder'=>'calle']) !!}
							<span style="color: red;">{!! $errors->first('calle') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('municipio', 'municipio') !!}

							{!! Form::text('municipio',null,['class'=>'form-control','placeholder'=>'municipio']) !!}
							<span style="color: red;">{!! $errors->first('municipio') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('colonia', 'colonia') !!}

							{!! Form::text('colonia',null,['class'=>'form-control','placeholder'=>'colonia']) !!}
							<span style="color: red;">{!! $errors->first('colonia') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('CP', 'CP') !!}

							{!! Form::text('CP',null,['class'=>'form-control','placeholder'=>'CP']) !!}
							<span style="color: red;">{!! $errors->first('CP') !!}</span>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('js')

@stop