@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
            @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Orientadores &nbsp;
        			<a href="{{ route('orientador.create') }}"> 
        				<button class="btn btn-success">
        					New
        				</button>
        			</a>
        		</p></h3>
        	</div>
            <div class="card-body">
                  <table class="table table-hover">
                  	<thead>
                  		<tr>
                  			<th>Nombre</th>
                  			<th>Apellidos</th>
                  			<th>Email</th>
                  			<th>Options</th>
                  		</tr>
                  	</thead>
                  	<tbody>
                  		@forelse ($orientadores as $i)
                  			<tr>
                  				<td>
                  					{!! $i->nombre !!}
                  				</td>
                  				<td>
                  					{!! $i->apellidoPaterno !!}
                  				</td>
                          <td>
                            {{ $i->apellidoMaterno }}
                          </td>
                  				<td>
                  					{!! $i->correo !!}
                  				</td>
                  				<td>
                  					<a href="{{ route('orientador.edit', $i->id ) }}">
                  						<button class="btn btn-info">Edit</button>
                  					</a>
                  					&nbsp;
                  					<form action="{{ route('orientador.destroy', $i->id ) }}" method="POST">
                  						@csrf

										          {{ method_field('DELETE') }}
                  						<button class="btn btn-danger">Delete</button>
                  					</form>
                  				</td>
                  			</tr>
                  		@empty
                  			<p>Sin Registros...</p>	
                  		@endforelse
                  	</tbody>
                  </table>
            </div>
        </div>
    </div>
@stop

@section('script')

@stop

