@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Orientadores &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>array('orientador.update',$edit->id), 'method' => 'POST']) !!}
                  		@method('PUT')
						<div class="form-group">
							{!! Form::label('matricula', 'Nombre') !!}

							{!! Form::text('nombre',$edit->nombre,['class'=>'form-control','placeholder'=>'nombre']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('nombre', 'Apellido Paterno') !!}

							{!! Form::text('apellidoPaterno',$edit->apellidoPaterno,['class'=>'form-control','placeholder'=>'apellidoPaterno']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('apellidos', 'Apellido Materno') !!}

							{!! Form::text('apellidoMaterno',$edit->apellidoMaterno,['class'=>'form-control','placeholder'=>'apellidoMaterno']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('telefono', 'Telefono') !!}

							{!! Form::text('telefono',$edit->telefono,['class'=>'form-control','placeholder'=>'telefono']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Email:') !!}
							{!! Form::text('correo', $edit->correo, ['class'=>'form-control', 'placeholder'=>'Enter Email']) !!}
							<span>{!! $errors->first('correo') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('sexo', 'Sexo') !!}

							{!! Form::text('sexo',$edit->sexo,['class'=>'form-control','placeholder'=>'sexo']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('calle', 'calle') !!}

							{!! Form::text('calle',$edit->calle,['class'=>'form-control','placeholder'=>'calle']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('municipio', 'municipio') !!}

							{!! Form::text('municipio',$edit->municipio,['class'=>'form-control','placeholder'=>'municipio']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('colonia', 'colonia') !!}

							{!! Form::text('colonia',$edit->colonia,['class'=>'form-control','placeholder'=>'colonia']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('CP', 'CP') !!}

							{!! Form::text('CP',$edit->CP,['class'=>'form-control','placeholder'=>'CP']) !!}
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('js')

@stop