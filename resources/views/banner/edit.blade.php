@extends('layout.layout')
@section('centro')
	<div class="col-md-12">
        <div class="card card-success">
        	<div class="card-header">
        		<h3><p>
        			Docentes &nbsp;
        		</p></h3>
        	</div>
            <div class="card-body">
                 <form action="{{ route('banner.update',$banner->id) }}" method="POST" enctype="multipart/form-data">

                 	@csrf
                    @method('PUT')
                 	
                 	<div class="form-group">
                 		<label>Foto:</label>
                        <img src="{{ $banner->ruta.$banner->archivo }}" width="80px;" height="80px;">
                 		<input type="file" name="archivo" class="form-control">
                 		<span style="color: red;">{!! $errors->first('foto') !!}</span>
                 	</div>
                    <div class="form-group">
                        <label>Nombre:</label>
                        <input type="text" name="descripcion" class="form-control" value="{{ $banner->descripcion }}">
                        <span style="color: red;">{!! $errors->first('descripcion') !!}</span>
                    </div>
                 	<button type="submit" class="btn btn-success">Save...!!!</button>
                 </form>
            </div>
        </div>
    </div>
@stop