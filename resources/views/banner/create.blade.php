@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Banners &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>'banner.store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                  		@csrf
						<div class="form-group">
							{!! Form::label('Descripcion', 'Descripcion') !!}

							{!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Agrega una descripcion']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('Archivo', 'Archivo') !!}
							{!! Form::file('archivo',null,['class'=>'form-control' ]) !!}
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop