@extends('layout.layout')

@section('centro')
<div class="container">
	<div class="col-md-12">	
		<div class="card">
			<div class="card-header">
				<h2>Asignacion de Grupos</h2>
			</div>
			<div class="card-body">	
			  <div class="row">
				    <div class="col-sm">
				      <label>Grupos</label>
				      <select class="form-control" id="grupos" name="grupos">
				      	<option>Selecciona Opcion</option>
				      	@foreach ($grupos as $g)
					      	<option value="{{ $g->idGrupo }}" data-capacidad="{{ $g->capacidad }}">
					      		{{ $g->nombreG }}
					      	</option>
				      	@endforeach

				      </select>
				    </div>
					<!--
				    <div class="col-sm">
				      <label>Docentes</label>
				      <select class="form-control" id="docentes" name="docentes">
				      	<option>Selecciona Opcion</option>
				      	@foreach ($docente as $d)
				      	<option value="{{ $d->idD }}">
				      		{{ $d->nombre }}
				      	</option>
				      	@endforeach
				      </select>
				    </div>

				    <div class="col-sm">
				      <label>Materias</label>
				      <select class="form-control" id="materias" name="materias">
				      	<option>Selecciona Opcion</option>
				      	@foreach ($materia as $m)
				      	<option value="{{ $m->idMateria }}">
				      		{{ $m->nombreDelamateria }}
				      	</option>
				      	@endforeach
				      </select>
				    </div>
					-->
				    <button class="btn btn-success btn-sm add" style="height: 40px; margin-top: 20px;" id="bote">
				    	Agregar alumnos
					</button>
			  </div>

			  <div class="row">

			  	<div id="terror">

			  	</div>

			  </div>

			</div>  
		</div>  
	</div>  
</div>
<hr>
<br>

<div id="susano" class="col-md-12">
	
</div>

<div id="cal">
	
</div>
@stop

@section('scrip')

$('#susano').load(`{{ route('XD') }}`)



$('#bote').attr('disabled','disabled')

let docentes;
let grupos;
let materias;
let capacidad;
$('#grupos').change(function(){

	capacidad = parseInt( $(this.options[this.selectedIndex]).attr('data-capacidad') )

	grupos = {
				'value':this.options[this.selectedIndex].text, 
				'id':this.value
	}
	
	$('#bote').prop("disabled", false);
});

$('#docentes').change(function(){

	docentes = {'value':this.options[this.selectedIndex].text, 'id':this.value}
	

});

$('#materias').change(function(){

	materias = {'value':this.options[this.selectedIndex].text, 'id':this.value}

	//$('#bote').prop("disabled", false);
})

$('#bote').click(function(){

	//console.log(docentes)
	
	
	
	$.ajax({

		url: '{{ route('send') }}',
		
		data: { grupo:grupos,capacidad:capacidad },
		
		success:function(ok){
			$('#terror').html(ok)
		}


	})
	

})

@stop
