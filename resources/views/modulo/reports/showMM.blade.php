@extends('layout.layout')

@section('centro')
<div class="container">
	<div class="col-md-12">	
		<div class="card">
			<div class="card-header">
				<h2>
					<center>
						Materia: {{ Session::get('materia') }}
					</center>
				</h2>
			</div>
			<div class="card-body">
				<table class="table table-hover" >
					<thead>
							<tr>
								<th>Numero de Cedula</th>
								<th>Nombre</th>
								<th>Especialidad</th>
								<th>Telefono</th>
								<th>Direccion</th>
								<th>Email</th>
							</tr>
						</thead>	
					<tbody>
						@foreach ($query as $i)
							<tr>
								<td>
									{{ $i->numCedula }}
								</td>
								<td>
									{{ $i->nombre }}	
								</td>
								<td>
									{{ $i->especialidadOmateria }}
								</td>
								<td>
									{{ $i->telefono }}
								</td>
								<td>
									{{ $i->direccion }}
								</td>
								<td>
									{{ $i->email }}
								</td>
							</tr>
						@endforeach
 					</tbody>
 				</table>
			</div>  
		</div>  
	</div>
</div>
@stop