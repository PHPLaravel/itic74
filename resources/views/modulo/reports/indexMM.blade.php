@extends('layout.layout')

@section('centro')
<div class="container">
	<div class="col-md-12">	
		<div class="card">
			<div class="card-header">
				<h2>
					<center>
						Reportes
					</center>
				</h2>
			</div>
			<div class="card-body">
				<table class="table">	
					<thead>
						<tr>
							<th>
								Materias
							</th>
						</tr>
					</thead>
					<tbody>
 						@foreach ($reportes as $i) 
							<tr>
								<td>
									{{ $i->nombreDelamateria }}
								</td>
								<td>
									<button class="btn btn-info reporte" value="{{ $i->idMateria }}">
										<i class="fa fa-file-text fa-lg" aria-hidden="true">
											
										</i>
									</button>
								</td>
							</tr>
 						@endforeach
 					</tbody>
 				</table>
			</div>  
		</div>  
	</div>
</div>
<div id="row" class="col-md-12">
	
</div>


@stop

@section('scrip')


$('.reporte').click(function(){

	let idMateria = $(this).val()
	$.ajax({

		url:'{{ route('reportMM') }}',

		data:{
			idMateria:idMateria,
			
		},

		success:function(response){
			//$('#row').html(response)
			var w = window.open();
			$(w.document.body).html(response);
		}

	})
})
@stop