@extends('layout.layout')

@section('centro')
<div class="container">
	<div class="col-md-12">
		@if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif	
		<div class="card">
			<div class="card-header">
				<h2>
					<center>
						Reportes
					</center>
				</h2>
			</div>
			<div class="card-body">
				<table class="table" >	
					<thead>
						<tr>
							<th>
								Grupos
							</th>
							<th>
								Alumnos
							</th>
							<th>
								Reportes
							</th>
							<th>
								Calificaciones
							</th>
						</tr>
					</thead>
					<tbody>
 						@foreach ($reportes as $i) 
							<tr>
								<td>
									{{ $i->nombreG }}

								</td>
								
								<td>
									@foreach ($i->alumnos as $casa)
									<br>{{ $casa->nombre}}
									@endforeach
									
								</td>
								
								<td>
									<button class="btn btn-info reporte" value="{{ $i->idGrupo }}">
										<i class="fa fa-file-text fa-lg" aria-hidden="true">
											
										</i>
									</button>
								</td>
								<td>
									<button class="btn btn-danger examen" value="{{ $i->idGrupo }}"  data-toggle="modal" data-target=".bd-example-modal-lg">
										<i class="fa fa-file-text fa-lg" aria-hidden="true">
											
										</i>
									</button>
								</td>
							</tr>
 						@endforeach
 					</tbody>
 				</table>
			</div>
		<div id="gotish">
			
		</div>	  
		</div>  
	</div>
</div>
<!----------  Modal ---------> 
<!--
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>	
-->
<!-- Modal -->

@stop

@section('scrip')


$('.reporte').click(function(){

	let idGrupo = $(this).val()
	$.ajax({

		url:'{{ route('report') }}',

		data:{
			idGrupo:idGrupo,
			
		},

		success:function(response){
			//$('#row').html(response)
			var w = window.open();
			$(w.document.body).html(response);
		}

	})
})

$('.examen').click(function(){

	let id = $(this).val()

	$.ajax({

		url:'{{ route('cal') }}',

		data:{
			idGrupo:id,
			
		},

		success:function(response){
			//console.log(response)
			//$('#row').html(response)
			
			//var w = window.open();
			//$(w.document.body).html(response);

			//$(".modal-body").html(response);
			$("#gotish").html(response);


		}
		
	})
})


@stop