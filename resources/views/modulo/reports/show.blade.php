@extends('layout.layout')

@section('centro')
<div class="container">
	<div class="col-md-12">	
		<div class="card">
			<div class="card-header">
				<h2>
					<center>
						Grupo: {{ Session::get('grupo') }}
					</center>
				</h2>
			</div>
			<div class="card-body">
				<table class="table table-hover" >
					<thead>
							<tr>
								<th>Matricula</th>
								<th>Nombre</th>
								<th>Apellidos</th>
								<th>Telefono</th>
								<th>Telefono de Casa</th>
								<th>Email</th>
							</tr>
						</thead>	
					<tbody>
						@foreach ($query as $i)
							<tr>
								<td>
									{{ $i->matricula }}
								</td>
								<td>
									{{ $i->nombre }}	
								</td>
								<td>
									{{ $i->apellidos }}
								</td>
								<td>
									{{ $i->telefono }}
								</td>
								<td>
									{{ $i->homePhone }}
								</td>
								<td>
									{{ $i->email }}
								</td>
							</tr>
						@endforeach
 					</tbody>
 				</table>
			</div>  
		</div>  
	</div>
</div>
@stop