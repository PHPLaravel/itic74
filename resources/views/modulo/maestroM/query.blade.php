<br>
<h3>

						<p> 						
						
								Materia: {{ $idMateria[0]->nombreDelamateria }}
							
						</p> 
						
					</h3>
<table class="table table-hober" >
		<thead>
			<tr>
				<th>
					Selecciona a los Docentes
				</th>
				<th>
					Numero de Cedula
				</th>
				<th>
					Nombre
				</th>
				<th>
					Especialidad
				</th>
			</tr>
		</thead>
		<tbody>
			
				@foreach ($resultado as $i)
				 <tr>
				 	<td>
				 		<input type="checkbox" name="habilitado" data-name="{{ $i->nombre}}" class="id" value="{{ $i->idD }}">
				 	</td>
				 	<td>
				 		{{ $i->numCedula }}
				 	</td>	
				 	<td>
				 		{{ $i->nombre}}
				 	</td>
					<td>
						{{ $i->especialidadOmateria }}
					</td>
				 </tr>
				@endforeach
				
		</tbody>
</table>
<script>
	let data,idD,idMateria;
	$('.id').change(function(){

		idD = this.value

		idMateria = `{{ $idMateria[0]->idMateria }}`;

		
		if ( $(this).is(':checked') ) {

			data = 1

			send(data,idD,idMateria);

		}else{

			data = 0

			//send(data,idD,idMateria);

		}
		

		
	});

	function send(data,idD,idMateria){

		let _token = `{{ csrf_token() }}`;

		$.ajax({

			url:'{{ route('posM') }}',

			type: 'POST',

			data:{

				_token:_token,

				data:data,

				idD:idD,

				idMateria:idMateria

			},


		})

	}
</script>