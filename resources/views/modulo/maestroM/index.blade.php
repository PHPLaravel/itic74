@extends('layout.layout')

@section('centro')
<div class="container">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h2>Asignacion de Materias a Docentes</h2>
			</div>
			<div class="card-body">
				<div class="row">
				    <div class="col-sm">
				      <label>Materia Docentes</label>
				      <select class="form-control" id="idMateria" name="idMateria">
				      	<option>Selecciona Opcion</option>
						@foreach ($grade as $i)
						<option value="{{ $i->idMateria }}">
							{{ $i->nombreDelamateria }}
						</option>
						@endforeach
				      </select>
				    </div>
				    <button class="btn btn-success btn-sm add" style="height: 40px; margin-top: 20px; " id="bote" disabled>
				    	Agregar docentes
					</button>
			  </div>
			  <br>
			  <div id="load">
			  	
			  </div>
			</div>	
		</div>
	</div>
</div>
<br>
<br>
<div id="pivot" class="col-md-12">
			  	
</div>	
@stop

@section('scrip')
let grade;
$('#idMateria').change(function(){

	grade = this.value

	$('#bote').prop("disabled", false);

})
$('#bote').click(function(){
	//console.log(grade)
	$('#pivot').hide()
	$.ajax({

		url:'{{ route('mMa') }}',

		data:{
				idMateria:grade
		},

		success:function(response){
			$('#load').append(response)
		}
	})
})

$('#pivot').load('{{ route('pivot') }}')
@stop