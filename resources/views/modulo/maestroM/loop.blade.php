<div class="container">
	<div class="col-md-12">	
		<div class="card">
			<div class="card-header">
				<h2>Asignar Docentes</h2>
			</div>
			<div class="card-body">
			<form id="form" method="POST">	
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Materias</th>
							<th>Docentes</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($grade as $i)
							<tr> 
								<td>
									{{ $i->nombreDelamateria }}
								</td>
								<td>
									@foreach ($i->docentes as $x)
											<br>{{ $x->nombre }}
									@endforeach
								</td>	
							</tr>
						@endforeach
					</tbody>
				</table>
			</form>	
			</div>  
		</div>  
	</div>  
</div>