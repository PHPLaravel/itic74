
	<table class="table">
		<thead>
			<tr>
				<th>Grupo:  {{ $grupo["value"] }} &nbsp; </th>
				
			</tr>
		</thead>
		<tbody>
			@foreach ($exce as $i)
			<tr>
			 	<td>
			 		<input type="checkbox" name="habilitado" data-name="{{ $i->nombre}}" class="id" value="{{ $i->idA }}">
			 	</td>	
			 	<td>
			 		
			 		{{ $i->matricula }}
			 	</td>
			 	<td>
			 		{{ $i->nombre}}
			 	</td>

			</tr>
			@endforeach
		</tbody>
	</table>


<script>
	
	
	$('.id').on('change',function(){

		let data;

		let idA;

		let idGrupo;

		let capacidad;		

		idA = this.value

		idGrupo = `{{ $grupo["id"] }}`;
		
		capacidad = `{{ $capacidad }}`;
		
		if ( $(this).is(':checked') ) {

			data = 1

			send(data,idA,idGrupo,capacidad);

		}else{

			data = 0


		}
		//send(data,idA,idGrupo);
		
		//console.log(data)
	})

	function send(data,idA,idGrupo,capacidad){
		let _token = `{{ csrf_token() }}`;
		$.ajax({

			url:'{{ route('pos') }}',

			type: 'POST',

			data:{

				_token:_token,

				data:data,

				idA:idA,

				idGrupo:idGrupo,

				capacidad:capacidad

			},


		})

	}
	
</script>

