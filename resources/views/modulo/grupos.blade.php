


<div class="container">
	<div class="col-md-12">	
		<div class="card">
			<div class="card-header">
				<h2>Grupos -- Alumnos</h2>
			</div>
			<div class="card-body">
			<form id="form" method="POST">	
				<table class="table table-hover" border="1">
					<thead>
						<tr>
							<th>Grupos</th>
							<th>Alumnos</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($x as $i)
							<tr > 
								<td>
									{{ $i->nombreG }}
								</td>
								<td>
									@foreach ($i->alumnos as $x)
											<br>{{ $x->nombre }} <button class="btn btn-danger btn-sm del" value="{{ $x->idA }}">
																	<i class="fa fa-window-close" aria-hidden="true"></i>
																 </button>
									@endforeach
								</td>	
							</tr>
						@endforeach
					</tbody>
				</table>
			</form>	
			</div>  
		</div>  
	</div>  
</div>
<script>
$('.del').click(function(e){

	e.preventDefault();

	let id = $(this).val();

	$.ajax({

		url:'{{ route('deleteA') }}',

		data:{idA:id},

		success:function(estatus){

			if( parseInt(estatus) == 1 ){

				alert("el alumno ha sido removido de este grupo")
				window.location.reload(true);
			}


		}


	})

})	
function datos(data){

	let grupo = data.childNodes[1].innerText;

	let alumnos = data.childNodes[3].innerText;

	let _token  = `{{ csrf_token() }}`

	let arreglo = alumnos.split('\n')

	

	//console.log( arreglo )
	
	$.ajax({

		url: '{{ route('laravel') }}',

		type: 'POST',

		data: {
			_token:_token,
			grupo:grupo,
			alumno:arreglo,
		},

		success:function(ok){
			$('#cal').html(ok)
		}
	})
	
}
	
</script>
