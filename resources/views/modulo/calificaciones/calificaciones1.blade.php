
	<style>
		
		.calificacion{
			width: 50px;
		}

	</style>
	<div class="container">
		<div class="col-md-12 col-lg-12">	
			<div class="card">
				<div class="card-header">
					<h2>
						<center>
							Grupo: {{ Session::get('grupo') }}
						</center>
					</h2>
				</div>
				<div class="card-body">
					<div class="col-md-12">
						<label for="">Seleccione Materia</label>
						<select name="idMateria" id="materia" class="form-control">
							@foreach($materias as $m)
								<option value="{{ $m->idMateria }}" >
									{{ $m->nombreDelamateria }}
								</option>
							@endforeach
						</select>
					</div>
					<br>
					<br>
					<div class="col-md-12">
						<label for="">Seleccione Docente</label>
						<select name="idD" id="maestro" class="form-control" disabled>
							
						</select>
					</div>
					<hr>
					<table class="table table-hover" id='nose'>
						<thead>
								<tr>
									<th>Nombre</th>
									<th colspan="4">Evaluar</th>
								</tr>
							</thead>	
						<tbody>
							<input type="hidden" value="{{ json_encode($query,true) }}" name='quotation'>
							@foreach ($query as $i)	
								 
									<tr id="{{ $i->idA }}" >
										<td>
											{{ $i->nombre }}	
										</td>
										<td>
											<button class="btn btn-success eva" value="{{ $i->idA }}">
												Evaluar
											</button>
										</td>
										<!--
										<td>
											<input type="text" class="calificacion">
										</td>
										<td>
											<input type="text" class="calificacion">
										</td>
										<td>
											<input type="text" class="calificacion">
										</td>
										<td>
											<input type="text" class="calificacion">
										</td>
										<td>
											<input type="text" class="calificacion">
										</td>
										-->
									</tr>
								
							@endforeach
	 					</tbody>
	 				</table>
				</div>  
			</div>  
		</div>
	</div>
<br>
<hr>
<div id="420">
	
</div>
<script>
	let maestro;
	let materia;
	let idMateria;
	let idD;
	

	$('#materia').change(function(){

		idMateria = $(this).val()

		materia = $( "#materia option:selected" ).text();

		$.ajax({

			url:'{{ route('querytodocentes') }}',

			data:{materia:materia},

			success:function(response){
				//console.log(response)
				if(response === 'Error'){

					alert("Error")

				}else{
				
					idD = response[0].idD
					
					let option = ""

					$.each(response, function(i,value){

						option+=`<option value='${value.idD}'>
									${value.nombre}
								 </option>`

						$('#maestro').html(option)		 
					})

				}
				
			}
		})
	})

	$('.eva').click(function(){

		let id = parseInt( $(this).val() )

		let data =	$( "input[name='quotation']" ).val()

	 	let obj = $.parseJSON(data).length

	 	//console.log(obj)

		
		$.ajax({

			url: '{{ route('datafromAlumno') }}',

			data:{

				idA:id, 
				idD:idD, 
				idMateria:idMateria,
				dimension:obj
			},

			success:function(res){
				$('#420').html(res)
			}

		})
		
		
	})
	/*
	$('#regex').click(function(){
			$('.mueble > td > input').each(function(i, value){

				valores = parseInt( $(this).val() )	
				values.push(valores)

			})

			//console.log()	
		
	 	let data =	$( "input[name='quotation']" ).val()

	 	let obj = $.parseJSON(data)

	 	let array = []

		
	 	let contador = 0

	 	/*
	 	while(contador < obj.length){
	 		//console.log(obj[contador].idA)

	 		let json = {
	 			"idA":obj[contador].idA,

	 		}

	 		array.push(json)
			
	 		contador++
	 		
	 		
	 		
	 	}
	 	console.log(array)
		
		
		
		for (var i = 0; i < values.length; i++) {

			console.log(values[i])
			
		}
		
	})
	*/
</script>