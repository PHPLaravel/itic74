<!--<head>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
</head>-->
<style>
	.cales{
		border-radius: 0px;
		width: 70px;
	}
</style>
<form method="post" action="{{ route('saveGrade') }}">
	@csrf
	<div class="container">
		<div class="card">

			<center>
				Grupo: <h2>{{ Session::get('grupo') }}</h2>

			</center>
			<div class="card-body">
					<input type="hidden" name="idD" value="{{ $idD }}" id="objeto">
					<input type="hidden" name="idMateria" value="{{ $idMateria }}" id="objeto">
					<input type="hidden" name="idGrupo" value="{{ Session::get('idGrupo') }}" id="objeto">
					@foreach ($alumno as $al)
					
						Alumno : <h4>{{ $al->nombre }}</h4>
						<input type="hidden" name="idA" value="{{ $al->idA }}" id="comparacion">

							<table>
								<thead>
									<tr>
										<th>1 Parcial</th>
										<th>2 Parcial</th>
										<th>3 Parcial</th>
										<th>4 Parcial</th>
										<th>5 Parcial</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<input type="text" name="ca1" id="cal1" class="cales" minlength="1" maxlength="2">
										</td>

										<td>
											<input type="text" name="ca2" id="cal2" class="cales" >
										</td>
										<td>
											<input type="text" name="ca3" id="cal3" class="cales" >
										</td>
										<td>
											<input type="text" name="ca4" id="cal4" class="cales" >
										</td>
										<td>
											<input type="text" name="ca5" id="cal5" class="cales" >
										</td>
									</tr>
								</tbody>
							</table>
					@endforeach
				<!--
				-->
				<button class="btn btn-info" style="background-color: " type="submit">Calificar</button>
			</div>
		</div>
	</div>
</form>
<script>
	
</script>