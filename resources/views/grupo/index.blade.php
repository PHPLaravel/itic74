@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
            @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Grupo &nbsp;
        			<a href="{{ route('grupos.create') }}"> 
        				<button class="btn btn-success">
        					New
        				</button>
        			</a>
        		</p></h3>
        	</div>
            <div class="card-body">
                  <table class="table table-hover">
                  	<thead>
                  		<tr>
                  			<th>Grupo</th>
                        <th>Capacidad</th>
                        <th>Turno</th>
                        <th>Grado Escolar</th>
                  			<th>Options</th>
                  		</tr>
                  	</thead>
                  	<tbody>
                  		@forelse ($grupos as $i)
                  			<tr>
                  				<td>
                  					{!! $i->nombreG !!}
                  				</td>
                          <td>
                            {!! $i->capacidad !!} alumnos
                          </td>
                          <td>
                            {!! $i->turno !!}
                          </td>
                          <td>
                            
                            {!! $i->gradoEscolar !!}
                            
                          </td>
                  				<td>
                  					<a href="{{ route('grupos.edit', $i->idGrupo ) }}">
                  						<button class="btn btn-info">Edit</button>
                  					</a>
                  					&nbsp;
                  					<form action="{{ route('grupos.destroy', $i->idGrupo ) }}" method="POST">
                  						@csrf

										          {{ method_field('DELETE') }}
                  						<button class="btn btn-danger">Delete</button>
                  					</form>
                  				</td>
                  			</tr>
                  		@empty
                  			<p>Sin Registros...</p>	
                  		@endforelse
                  	</tbody>
                  </table>
            </div>
        </div>
    </div>
@stop

@section('script')

@stop

