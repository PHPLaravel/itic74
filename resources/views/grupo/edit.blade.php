@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Grupos &nbsp;

        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>array('grupos.update',$edit->idGrupo), 'method' => 'POST']) !!}
                  	@method('PUT')
                  		<div class="form-group">
							{!! Form::label('cmateria', 'Id') !!}

							{!! Form::text('idGrupo',$edit->idGrupo,['class'=>'form-control','placeholder'=>'id','readonly']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('cmateria', 'CNombre del Grupo') !!}

							{!! Form::text('nombreG',$edit->nombreG,['class'=>'form-control','placeholder'=>'nombre']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('nombre', 'Capacidad') !!}

							{!! Form::text('capacidad',$edit->capacidad,['class'=>'form-control','placeholder'=>'capacidad']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('creditos', 'Turno') !!}
							
							<select class="form-control" name="turno">
								@if($edit->turno === "Matutino")
									<option value="{{ $edit->turno }}">
										{{ $edit->turno }}
									</option>
									<option value="vespertino">
										Vespertino
									</option>
								@elseif ($edit->turno === "vespertino")
									<option value="{{ $edit->turno }}">
										{{ $edit->turno }}
									</option>
									<option value="Matutino">
										Matutino
									</option>	
								@endif
							</select>
						</div>

						<div class="form-group">
							{!! Form::label('creditos', 'Grado Escolar') !!}
					
							<select class="form-control" name="id">
								
								@foreach ($gradoescolar as $i)
									<option value="{{ $i->id }}">
										{{ $i->gradoEscolar }}
									</option>
								@endforeach
								
								@foreach ($grados as $x)
									<option value="{!! $x->id !!}">
										{!! $x->gradoEscolar !!}
									</option>
								@endforeach
							</select>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop