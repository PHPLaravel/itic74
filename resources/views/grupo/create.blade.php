@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Grupos &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>'grupos.store', 'method' => 'POST']) !!}
                  		<div class="form-group">
							{!! Form::label('cmateria', 'Id') !!}

							{!! Form::text('idGrupo',$llavequesigue,['class'=>'form-control','placeholder'=>'id','readonly'=>'readonly']) !!}
							<span style="color: red;">{!! $errors->first('idGrupo') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('cmateria', 'CNombre del Grupo') !!}

							{!! Form::text('nombreG',null,['class'=>'form-control','placeholder'=>'nombre']) !!}
							<span style="color: red;">{!! $errors->first('nombreG') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('nombre', 'Capacidad') !!}

							{!! Form::text('capacidad',null,['class'=>'form-control','placeholder'=>'capacidad']) !!}
							<span style="color: red;">{!! $errors->first('capacidad') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('creditos', 'Turno') !!}

							<select class="form-control" name="turno" >
								<option value="Matutino">Matutino</option>
								<option value="vespertino">Vespertino</option>
							</select>
							<span style="color: red;">{!! $errors->first('turno') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('creditos', 'Grado Escolar') !!}

							<select class="form-control" name="id">
								@foreach ($grado as $gra)
									<option value="{!! $gra->id !!}">
										{!! $gra->gradoEscolar !!}
									</option>
								@endforeach
							</select>
							<span style="color: red;">{!! $errors->first('turno') !!}</span>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop