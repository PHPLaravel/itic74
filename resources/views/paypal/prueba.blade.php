@extends('layoutpaypal.layoutp')

@section('contenido')
@if ($message = Session::get('success'))
        <div class="w3-panel w3-green w3-display-container">
            <span onclick="this.parentElement.style.display='none'"
            class="w3-button w3-green w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
        <?php Session::forget('success');?>
        @endif

        @if ($message = Session::get('error'))
        <div class="w3-panel w3-red w3-display-container">
            <span onclick="this.parentElement.style.display='none'"
            class="w3-button w3-red w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
        <?php Session::forget('error');?>
        @endif
<form class="w3-container w3-display-middle w3-card-4 w3-padding-16" method="POST" id="payment-form" action="{{ URL::to('pay') }}">
  {{ csrf_field() }}
  <table border="0" class="table table-hover">

    <tr>
      <td>
        <i class="fa fa-building" aria-hidden="true"></i>
      </td>
      <td>
        <center>Esc. Sec. Ofic. No. 0382 "Dr. José Ma. Luis Mora"</center>
      </td>
    </tr>
    <tr>
      <td>
        Datos Personales
      </td>
      <td>
        <table border="0"  class="table">
            <tr>
              <td>
                <label for="">Matricula</label>
                <input type="text" name="matricula">
              </td>

              <td>
                <label for="">RFC</label>
                <input type="text" name="rfc">
              </td>

              <td>
                <label for="">Curp</label>  
                <input type="text" class="form-control" name="curp">
              </td>

            </tr>
            <tr>
              <td>
                <label for="">Apellido Paterno</label>
                <input type="text" name="ap">
              </td>
              <td>
                <label for="">Apellido Materno</label>
                <input type="text" name="am">
              </td>
              <td>
                <label for="">Nombre(s)</label>
                <input type="text" name="nombre">
              </td>
            </tr>
          </table>
      </td>
    </tr>
    <tr>
      <td>
        Domicilio
      </td>
      <td>
        <table border="0"  class="table">
            <tr>
              <td>
                <label for="">Calle</label>
                <input type="text" name="calle">
              </td>

              <td>
                <label for="">Numero Exterior</label>
                <input type="text" name="NE">
              </td>

              <td>
                <label for="">Numer Interior</label>  
                <input type="text" class="form-control" name="NI">
              </td>

            </tr>
            <tr>
              <td>
                <label for="">Colonia</label>
                <input type="text" name="colonia">
              </td>
              <td>
                <label for="">Localidad</label>
                <input type="text" name="localidad">
              </td>
              <td>
                <label for="">Municipio</label>
                <input type="text" name="municipio">
              </td>
            </tr>
            <tr>
              <td>
                <label for="">Estado</label>
                <input type="text" name="estado">
              </td>
              <td>
                <label for="">Codigo Postal</label>
                <input type="text" name="cp">
              </td>
            </tr>
          </table>
      </td>
    </tr>
    <tr>
      <td>
        Servicios
      </td>
      <td>
        <table border="0"  class="table">
            <tr>
              <td>
                <label for="">Tipo</label>
                <select name="tipo" id="tipo" class="form-control">
                  <option value="0">Selecciona Opcion</option>
                  @foreach ($servTipo as $i => $value)
                    <option value="{{ $value['idt'] }}">{{ $value['tipo'] }}</option>
                  @endforeach
                </select>
              </td>
              <td>
                <div>
                  <label for="">Monto</label>
                  $ &nbsp;<input class="iraki" id="amount" type="text" name="amount" readonly>
                </div>
              </td>
             </tr>
             <tr>
                <td>
                  <label for="">Concepto</label>
                  <select name="concepto" id="concepto" class="form-control">
                    <option value="0">Selecciona Opcion</option>

                  </select>
                </td>
               <td>
                 <label for="">Cantidad</label>
                 <input id="count_service" type="number" min="1" max="50" value="1">

                 <a class="btn btn-success btn-add-service">Agregar</a>


               </td>
             </tr>
          </table>
      </td>
    </tr>
    <tr>
      <td>

        Resumen
      </td>
      <td >
        <div id="mensaje">
          <table class="tbl" border="0">
            <thead>
              <tr>
                <th>Identificador</th>
                <th>Descripcion</th>
                <th>Monto</th>
                <th>Cantidad</th>
                <th>subtotal</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
      </td>
    </tr>
    <tr>

      <td colspan="20">
        Total 
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;        
        <input type="text" id="totalgral" name="amount" style="text-align: right; margin-right: 30px;" readonly>
      </td>
    </tr>
    <tr>

      <td>
        Operacion
      </td>
      <td>
        
        <button style="margin-left: 600px;" type="submit">Cancelar</button>
      </td>
    </tr>
  </table>         
</form>
@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.js"
        integrity="sha256-kzv+r6dLqmz7iYuR2OdwUgl4X5RVsoENBzigdF5cxtU="
        crossorigin="anonymous"></script>
<script>
  var tipo;
  var combo = "";

  var idC;
  var precioUnitario;
  let concepts_all = [];
  let current_val_concept = 0;
  let services_car = [];

  /**
   * Active trigger when click in .btn-add-service
   */
  $('.btn-add-service').click(function (event) {
    event.preventDefault();
    event.stopPropagation();
    addService();
  });

  /**
   * Function that find concept in all concepts.
   * @param id
   * @returns {boolean}
   */
  function findConcept(id) {
    let result = false;
    for (let i=0; i < concepts_all.length; i++){
      if(concepts_all[i]['idconce'] === id){
        result = concepts_all[i];
      }
    }
    return result;
  }

  /**
   * Function that add service in array service
   */
  function addService(){
    let service_type = findConcept(current_val_concept);
    let count_service = $('#count_service').val();
    let service_count = parseInt(service_type['cantidad']);

    $('.services').remove();

    services_car.push({
      'id': service_type['idconce'],
      'name': service_type['concepto'],
      'unit_cost': service_count,
      'count': count_service,
      'subtotal': service_count * count_service
    });

    for (let i=0; i<services_car.length; i++){
      let string = '<input class="services" type="hidden" name="services[]" value="'+services_car[i]['id']+'|'+services_car[i]['count']+'">';


     

      $('#payment-form').append(string);
      //$('#mensaje').remove();
      
      //$('#tbl tr:last').remove()
      

      //$('#tbl > tr').eq(i-1).after(html);

      //$("#tbl tr").last().after(contenido);  
      

      //$('#tbl').append(contenido)
      
      //$('#mensaje').append(contenido)
      //var parent = $(this).closest("myTable");
      //$('#myTable').append("<tr><td>"+services_car[i]['id']+"</td></tr>")

    }
    
    


    let total_services = _.reduce(services_car, function(sum, service) {
      return sum + service.subtotal;
    }, 0);

    //console.log(services_car)

    console.log(total_services)

    $('#totalgral').val(total_services)

    addRows(services_car,total_services)    

  }

  function addRows(services_car,total_services){

    console.log(services_car)

    //$('.tbl').remove();

    
        
          var table = $('.tbl');
          table.find("tbody tr").remove();
          services_car.forEach(function (services_car) {
              table.append("<tr><td>" 
                + services_car.id + 
                "</td><td>" 
                + services_car.name 
                + "</td><td>"
                +services_car.unit_cost
                +"</td><td>"
                +services_car.count
                +"</td><td>"
                +services_car.subtotal
                +"</td></tr>");
          });
          
      
      //$('#mensaje').remove();
      
      //$('#tbl tr:last').remove()
      

      //$('#tbl > tr').eq(i-1).after(html);

      //$("#tbl tr").last().after(contenido);  
      

      //$('#tbl').append(contenido)
      
      //$('#mensaje').append(contenido)
      //var parent = $(this).closest("myTable");
      //$('#myTable').append("<tr><td>"+services_car[i]['id']+"</td></tr>")

    


      //$('#payment-form').append(string);
    }

   

  

  $('#amount').val(0);

  $('#tipo').change(function(){

    tipo = parseInt(this.value)

    $.ajax({

      url: '{{ route('combo') }}',

      data:{
        "_token": "{{ csrf_token() }}",
        "id":tipo
      },

      type: 'POST',

      success:function(concepts){
        concepts_all = concepts;
        combo = '';
        combo += '<option>Selecciona una opción</option>';
        $.each(concepts_all, function(i, item) {
            combo += `<option value='${item.idconce}'>
                        ${item.concepto}
                      </option>`
           //console.log()
        });

        $('#concepto').html(combo)
      },

      error:function(error){
        console.error('Error')
      }

    })

  });

  $('#concepto').change(function(){

    current_val_concept = parseInt(this.value);


    console.log('current ', current_val_concept);

    let result = findConcept(current_val_concept);

    console.log('result ', result);

    if (result){
      $('#amount').val(result['cantidad']);
    }


    

    return result;

  });

</script>
@stop