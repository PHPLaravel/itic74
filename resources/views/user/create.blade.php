@extends('layout.layout')

@section('centro')
	
	<div class="col-md-12">
            @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-success">
        	<div class="card-header">
        		<h3><p>
        			Users &nbsp;
        		</p></h3>
        	</div>
            <div class="card-body">
                 <form action="{{ route('users.store') }}" method="POST">

                   <div class="form-group">
                     <label>Name</label>
                     <input type="text" name="name" class="form-control">
                   </div>
                   <div class="form-group">
                     <label>Email</label>
                     <input type="email" name="email" class="form-control">
                   </div>
                   <div class="form-group">
                     <label>Password</label>
                     <input type="password" name="password" class="form-control">
                   </div>
                   <div class="form-group">
                     <label>Roles</label>
                     <select class="form-control" name="roles_id">
                       @foreach ($roles as $r)
                        <option value="{{ $r->id }}">
                          {{ $r->role }}
                        </option>
                       @endforeach
                     </select>
                   </div>
                   <button type="submit" class="btn btn-success">Save</button>
                 </form>
            </div>
        </div>
    </div>

@stop