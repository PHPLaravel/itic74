@extends('layout.layout')

@section('centro')
	
	<div class="col-md-12">
            @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-success">
        	<div class="card-header">
        		<h3><p>
        			Users &nbsp;
        			<a href="{{ route('users.create') }}"> 
        				<button class="btn btn-success">
        					New
        				</button>
        			</a>
        		</p></h3>
        	</div>
            <div class="card-body">
                 <table class="table table-hover">
                   <thead>
                     <tr style="background-color: #FF5733;">
                      <th>Name</th>
                      <th>Email</th>
                      <th>OPT</th>
                     </tr>
                   </thead>
                   <tbody>
                    @foreach ($users as $i)
                      <tr>
                        <td>
                          
                        	{{ $i->name }}
                        </td>
                        <td>
                          {{ $i->email }}
                        </td>

                        <td>
                          <a href="{{ route('users.edit',$i->id) }}">
                            <button class="btn btn-info edit">
                              Edit
                            </button>
                          </a>
                          &nbsp;
                          <form action="{{ route('users.destroy',$i->id) }}" method="POST">
                            @csrf
                            {{ method_field('DELETE') }}
                            <button class="btn btn-danger">
                              Delete
                            </button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                   </tbody>
                 </table> 
            </div>
        </div>
    </div>

@stop