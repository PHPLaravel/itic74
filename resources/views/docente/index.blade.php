@extends('layout.layout')
@section('centro')
	<div class="col-md-12">
        @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-success">
        	<div class="card-header">
        		<h3><p>
        			Docentes &nbsp;
        			<a href="{{ route('Docentes.create') }}"> 
        				<button class="btn btn-success">
        					New
        				</button>
        			</a>
        		</p></h3>
        	</div>
            <div class="card-body">
                 <table class="table table-hover">
                   <thead>
                     <tr style="background-color: #FF5733;">
                      <th>Foto</th>
                      <th>Numero de Cedula</th>
                      <th>Nombre</th>
                      <th>Especialidad</th>
                      <th>OPT</th>
                     </tr>
                   </thead>
                   <tbody>
                    @foreach ($docentes as $i)
                      <tr>
                        <td>
                          @if($i->Foto == "sombra")
                          <img src="{{ asset($i->ruta.$i->Foto) }}" width="60px;" height="60px;">
                          @else
                          <img src="{{ asset($i->ruta.$i->Foto) }}" width="60px;" height="60px;">
                          @endif
                        </td>
                        <td>
                          {{ $i->numCedula }}
                        </td>
                        <td>
                          {{ $i->nombre }}
                        </td>
                        <td>
                          {{ $i->especialidadOmateria }}
                        </td>
                        <td>
                          <a href="{{ route('Docentes.edit',$i->idD) }}">
                            <button class="btn btn-info edit">
                              Edit
                            </button>
                          </a>
                          &nbsp;
                          <form action="{{ route('Docentes.destroy',$i->idD) }}" method="POST">
                            @csrf
                            {{ method_field('DELETE') }}
                            <button class="btn btn-danger">
                              Delete
                            </button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                   </tbody>
                 </table> 
            </div>
        </div>
    </div>
@endsection 
@section('scrip')

@stop



