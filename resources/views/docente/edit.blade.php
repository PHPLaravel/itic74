@extends('layout.layout')
@section('centro')
	<div class="col-md-12">
        <div class="card card-success">
        	<div class="card-header">
        		<h3><p>
        			Docentes &nbsp;
        		</p></h3>
        	</div>
            <div class="card-body">
                 <form action="{{ route('Docentes.update',$edit->idD) }}" method="POST" enctype="multipart/form-data">

                 	@csrf
                    @method('PUT')
                 	<div class="form-group">
                 		<label>Nombre:</label>
                 		<input type="text" name="nombre" class="form-control" value="{{ $edit->nombre }}">
                 		<span style="color: red;">{!! $errors->first('nombre') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>Especialidad:</label>
                 		<input type="text" name="especialidadOmateria" class="form-control" value="{{ $edit->especialidadOmateria }}">
                 		<span style="color: red;">{!! $errors->first('especialidadOmateria') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>genero:</label>
                 		@if ($edit->genero == "M")
                        <input type="radio" name="genero" value="M" checked>M 
                        <input type="radio" name="genero" value="F">F
                        @else
                        <input type="radio" name="genero" value="M">M 
                        <input type="radio" name="genero" value="F" checked>F
                        @endif
                 		<span style="color: red;">{!! $errors->first('genero') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>fechaNacimiento:</label>
                 		<input type="date" name="fechaNacimiento" class="form-control" value="{{ $edit->fechaNacimiento }}">
                 		<span style="color: red;">{!! $errors->first('fechaNacimiento') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>direccion:</label>
                 		<input type="text" name="direccion" class="form-control" value="{{ $edit->direccion}}">
                 		<span style="color: red;">{!! $errors->first('direccion') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>telefono:</label>
                 		<input type="text" name="telefono" class="form-control" value="{{ $edit->telefono}}">
                 		<span style="color: red;">{!! $errors->first('telefono') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>email:</label>
                 		<input type="email" name="email" class="form-control" value="{{ $edit->email }}">
                 		<span style="color: red;">{!! $errors->first('email') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>numCedula:</label>
                 		<input type="text" name="numCedula" class="form-control" value="{{ $edit->numCedula }}">
                 		<span style="color: red;">{!! $errors->first('numCedula') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>diaDeingreso:</label>
                 		<input type="date" name="diaDeingreso" class="form-control" value="{{ $edit->diaDeingreso }}">
                 		<span style="color: red;">{!! $errors->first('diaDeingreso') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>Foto:</label>
                        <img src="{{ $edit->ruta.$edit->Foto }}" width="80px;" height="80px;">
                 		<input type="file" name="foto" class="form-control">
                 		<span style="color: red;">{!! $errors->first('foto') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>Usuario:</label>
                 		<input type="text" name="nameUser" class="form-control" value="{{ $edit->nameUser }}">
                 		<span style="color: red;">{!! $errors->first('nameUser') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>Contraseña:</label>
                 		<input type="password" name="password" class="form-control" value="{{ $edit->password }}">
                 		<span style="color: red;">{!! $errors->first('password') !!}</span>
                 	</div>
                 	<div class="form-group">
                 		<label>Estatus:</label>
                        @if($edit->estatus == 1)
                            <input type="radio" name="estatus" checked  value="1" >Active
                            <input type="radio" name="estatus"  value="0" >InActive
                        @elseif($edit->estatus == 0)
                            <input type="radio" name="estatus"  value="1" >Active
                            <input type="radio" name="estatus" checked value="0" >Inactive
                        @endif
                 		<span style="color: red;">{!! $errors->first('estatus') !!}</span>
                 	</div>
                 	<button type="submit" class="btn btn-success">Save...!!!</button>
                 </form>
            </div>
        </div>
    </div>
@stop