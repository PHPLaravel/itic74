@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
            @if( session()->has('ok') )
        <div class="alert alert-success" role="alert">
          {{ session()->get('ok') }}
        </div>
        @elseif(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
        @endif
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Horarios Alumnos &nbsp;
        			<a href="{{ route('horarioAlumnos.create') }}"> 
        				<button class="btn btn-success">
        					New
        				</button>
        			</a>
        		</p></h3>
        	</div>
            <div class="card-body">
                  <table class="table table-hover">
                  	<thead>
                  		<tr>
                  			<th>Nombre del Alumno</th>
                  			<th>Dia de clase</th>
                  			<th>Clave de la materia</th>
                        <th>Nombre de la Materia</th>
                        <th>Hora de inicio</th>
                        <th>Hora de Fin</th>
                  			<th>Options</th>
                  		</tr>
                  	</thead>
                  	<tbody>
                      @forelse ($query as $i)
                        <tr>
                          <td>
                            {!! $i->nombre !!}
                          </td>
                          <td>
                            {{ $i->dia }}
                          </td>
                          <td>
                            {{ $i->claveDemateria }}
                          </td>
                          <td>
                            {!! $i->nombreDelamateria !!}
                          </td>
                          <td>
                            {!! $i->horaInicio !!}
                          </td>
                          <td>
                            {{ $i->horaFin }}
                          </td>
                          <td>
                            <a href="{{ route('horarioAlumnos.edit',$i->idHorarioA ) }}">
                              <button class="btn btn-info">Edit</button>
                            </a>
                            &nbsp;
                            <form action="{{ route('horarioAlumnos.destroy',$i->idHorarioA ) }}" method="POST">
                              @csrf

                              {{ method_field('DELETE') }}
                              <button class="btn btn-danger">Delete</button>
                            </form>
                          </td>
                        </tr>
                      @empty
                        <p>Sin Registros...</p> 
                      @endforelse
                  	</tbody>
                  </table>
            </div>
        </div>
    </div>
@stop
@section('js')
@stop