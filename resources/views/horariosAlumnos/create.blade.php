@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Horari de Alumnos &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>'horarioAlumnos.store', 'method' => 'POST']) !!}
						<div class="form-group">
							{!! Form::label('dia', 'Dia') !!}

							{!! Form::text('dia',null,['class'=>'form-control','placeholder'=>'Dia']) !!}
							<span style="color: red;">{!! $errors->first('dia') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('docente', 'Alumno') !!}
							<select class="form-control" name="idA">
							@foreach ($alumnos as $i)
								<option value="{{ $i->idA }}">{{ $i->nombre }}</option>
							@endforeach
							</select>
							<span style="color: red;">{!! $errors->first('idA') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Materia') !!}
							<select class="form-control" name="idMateria">
							@foreach ($materia as $m)
								<option value="{{ $m->idMateria }}">
									{{ $m->claveDemateria }}
									&nbsp;
									{{ $m->nombreDelamateria }}
								</option>
							@endforeach
							</select>
							<span style="color: red;">{!! $errors->first('idMateria') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Hora de Inicio') !!}

							
							{{ Form::time('horaInicio', null, ['class'=>'form-control','placeholder'=>'hora'] ) }}
							<span style="color: red;">{!! $errors->first('horaInicio') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Hora de Termino') !!}

							{{ Form::time('horaFin', null, ['class'=>'form-control','placeholder'=>'hora'] ) }}
							<span style="color: red;">{!! $errors->first('horaFin') !!}</span>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop