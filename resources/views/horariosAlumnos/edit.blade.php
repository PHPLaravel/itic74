@extends('layout.layout')

@section('centro')
	<div class="col-md-12">
        <div class="card card-statistics">
        	<div class="card-header">
        		<h3><p>
        			Horario de Alumnos &nbsp;
        			
        		</p></h3>
        	</div>
            <div class="card-body">
                  	{!!  Form::open(['route'=>array('horarioAlumnos.update',$edit->idHorarioA), 'method' => 'POST']) !!}
                  	@method('PUT')
						<div class="form-group">
							{!! Form::label('dia', 'Dia') !!}

							{!! Form::text('dia',$edit->dia,['class'=>'form-control','placeholder'=>'Dia']) !!}
							<span style="color: red;">{!! $errors->first('dia') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('docente', 'Alumno') !!}
							<select class="form-control" name="idA">
								<option value="{{ $edit->alumnos->idA }}">
									{{ $edit->alumnos->nombre }}
								</option>
								@foreach ($alumnos as $i)
									<option value="{{ $i->idA }}">
										{{ $i->nombre }}
									</option>
								@endforeach
							</select>
							<span style="color: red;">{!! $errors->first('idA') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('grupo', 'Grupo') !!}
							
							<select class="form-control" name="idMateria">
								<option value="{{ $edit->materias->idMateria }}">

									{{ $edit->materias->nombreDelamateria }}
									
								</option>
								@foreach($materias as $x)
									<option value="{{ $x->idMateria }}">
										{{ $x->nombreDelamateria }}
									</option>
								@endforeach
							</select>
							<span style="color: red;">{!! $errors->first('idMateria') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Hora de Inicio') !!}

							
							{{ Form::time('horaInicio',$edit->horaInicio, ['class'=>'form-control','placeholder'=>'hora'] ) }}
							<span style="color: red;">{!! $errors->first('horaInicio') !!}</span>
						</div>
						<div class="form-group">
							{!! Form::label('dia', 'Hora de Termino') !!}

							{{ Form::time('horaFin',$edit->horaFin, ['class'=>'form-control','placeholder'=>'hora'] ) }}
							<span style="color: red;">{!! $errors->first('horaFin') !!}</span>
						</div>
						{!! Form::submit('Save!',['class'=>'btn btn-success']) !!}
					{!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('script')

@stop