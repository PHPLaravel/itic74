
<head>

  <meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bienvenido a GEMElite</title>
   <link rel="shortcut icon" href="{{ url('plantilla/img/Imagenes/CCFISCAL.jpg') }}" />
  
  <link href="{{ url('plantilla/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ url('plantilla/font-awesome/css/font-awesome.css') }}" rel="stylesheet"> 
  <!-- Toastr style -->
  <link href="{{ url('plantilla/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
  <link href="{{ url('plantilla/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
  <link href="{{ url('plantilla/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
  <link href="{{ url('plantilla/css/animate.css') }}" rel="stylesheet">
  <link href="{{ url('plantilla/css/style.css') }}" rel="stylesheet">
  <link href="{{ url('plantilla/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
  <link href="{{ url('plantilla/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
  <link href="{{ url('plantilla/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">

<link href="{{ url('plantilla//css/plugins/blueimp/css/blueimp-gallery.min.css')}}" rel="stylesheet">

    <link href="{{ url('plantilla/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
  <!-- Mainly scripts -->
  <script src="{{ url('plantilla/js/jquery-2.1.1.js') }}"></script>
  <script src="{{ url('plantilla/js/bootstrap.min.js') }}"></script>
  <script src="{{ url('plantilla/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
  <script src="{{ url('plantilla/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

  <!-- Custom and plu gin javascript -->
  <script src="{{ url('plantilla/js/inspinia.js') }}"></script>

  <script src="{{ url('plantilla/js/plugins/pace/pace.min.js') }}"></script>

  <!-- jQuery UI -->
  <script src="{{ url('plantilla/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
  <!-- Toastr -->
  <script src="{{ url('plantilla/js/plugins/toastr/toastr.min.js') }}"></script>
  <script src="{{ url('plantilla/js/plugins/select2/select2.full.min.js') }}"></script>
  <link href="{{ url('plantilla/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
  <script src="{{ url('plantilla/js/validator.min.js') }}"></script>
  <script src="{{ url('plantilla/js/libreria.js') }}"></script>
  <link href="{{ url('plantilla/js/libreria.js') }}" rel="stylesheet">
  <link href="{{ url('plantilla/js/dist/facebook.css') }}" rel="stylesheet">
  <script src="{{ url('plantilla/js/dist/sweetalert.min.js') }}"></script> 
  <link href="{{ url('plantilla/css/plugins/steps/jquery.steps.css') }}" rel="stylesheet">
  
   <style>

        .wizard > .content > .body  position: relative; }

    </style>

  <script language="javascript">
    var base = '{{ url('/') }}';
    var site = '{{ url('/index') }}';   

    function ImagenOk(img) {
      if (!img.complete) return false;
      if (typeof img.naturalWidth != "undefined" && img.naturalWidth == 0) return false;
      return true;
    }
    function RevisarImagenesRotas() {
      var replacementImg ="{{url('../plantilla/img/no_image_default.jpg') }}";
      for (var i = 0; i < document.images.length; i++) {
        if (!ImagenOk(document.images[i])) {
          document.images[i].src = replacementImg;
        }}}
        window.onload=RevisarImagenesRotas;
  </script>
  <style type="text/css"> #map { height: 350px; width:100%; }  </style>
</head>
<br>
@php
use App\Log;
$role = Log::all();
@endphp
<body class="login-bg" >
    <div class="loginColumns animated fadeInDown">
        <div class="row">
            <div class="col-md-3" >
                <h2 class="font-bold">Bienvenido a <br> Gem-Elite </h2>
                <hr>
                <p>
                    Tus datos estan protegidos.
                </p>
                <p>
                   Sistema facil de usar.
                </p>
                <p>
                    Administración empresarial.
                </p>
                <p>
                    Administración operativa.
                </p>
            </div>
            <div class="col-md-8">
                 <div class="ibox-title">
                            <h2>Registro de nuevo cuenta de usuario.</h2>
                </div>
                <div class="ibox-content">
                	@if(session()->has('sessionclear'))
                    <div class="alert alert-danger" role="alert">
                  	   <p>{{ session()->get('sessionclear') }}</p>
                    </div>
                	@endif
    				
    						<form id="form" action="{{ route('register') }}" method="POST" class="wizard-big">
                                 {{ csrf_field() }}
                               <h1>Cuenta</h1>
                                <fieldset>
                                    <h2>Información de la cuenta</h2>
                                    <div class="row" style="overflow: auto; ">
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label>Nombre *</label>
                                                <input name="nombre"  type="text" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Apellido paterno *</label>
                                                <input name="ap_paterno" type="text" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Apellido materno *</label>
                                                <input  name="ap_materno" type="text" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Correo electronico *</label>
                                                <input  name="email" type="email" class="form-control required email">
                                            </div>
                                             
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label>Nombre de usuario *</label>
                                                <input type='text' id='username' name="name" title="La primera letra debe ser mayuscula y minimo 5 caracteres."  minlength="5" class="form-control required">  

                                            <div class="form-group">
                                                <label>Contraseña *</label>
                                                <input id="password" name="password"  minlength="8" type="password" title="Debe de contener 8 caracteres y almenos un numero y un signo (?=.*)" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Confirme Contraseña *</label>
                                                <input id="confirm" name="password" type="password" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Rol *</label>
                                                <select name="roles" id="roles_id" class="form-control">
                                                  @foreach ($role as $r)
                                                  <option value="{{ $r->id }}">{{ $r->role }}</option>
                                                  @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-lg-5">
                                        <div class="form-group">
                                          <button class="btn btn-praimary" type="submit">Crear</button>      
                                        </div>
                                    </div>
                                </fieldset>
                            </form>	               
                   
                    <p class="m-t">
                        <small>GEM-Elite &copy; 2019</small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
    </div>
</body>