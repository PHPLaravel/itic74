<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use App\User;


Route::get('/', function () {
    //return view('welcome');
	//$rol = RoleSchool::all();
    return view('admin');
})->name('/');

Route::any('template',function(){
	return view('admin.index');
})->name('template');

Route::any('show',function(Request $request){

	$id = $request->get('id');

	$aviso = \App\Avisos::findOrFail($id);

	return $aviso;
})->name('avisos.show');

/*------------- Nuevo Registro -----------*/
Route::any('registro','Auth\RegisterController@showRegistrationForm')->name('nuevo');

Route::any('newuser', function(Request $request){

	$client = new User();
	$client->name = $request->get('name');
    $client->email = $request->get('email');
    $client->password = Hash::make($request->get('password'));       
	$client->roles_id = $request->get('roles');
	$addUser = $client->save();

	if ($addUser) {
        return redirect()->route('login')->with('mensajeDeusuario','Ya creaste un usuario ahora logueate');
    }
        return redirect()->route('login')->with('mensajeDeusuarioError','Pofavor Contacta al Administrador');
})->name('register');

/* ------------ Documentos ---------------*/

Route::any('index','DocumentsController@index')->name('documentos.index');
Route::any('add','DocumentsController@create')->name('documentos.create');
Route::any('save','DocumentsController@store')->name('documentos.store');
Route::any('show/{id}','DocumentsController@edit')->name('documentos.edit');
Route::any('download/{id}','DocumentsController@download')->name('documentos.download');

/* ------------ Documentos ---------------*/

//Route::resource('data','FirstController')->name('data');
Route::resource('alumno','FirstController');

/*  ---------- Avisos ------------ */
Route::any('avisos',['uses'=>'AvisosController@index'])->name('avisos');
Route::any('create',['uses'=>'AvisosController@create'])->name('avisos.create');
Route::any('store',['uses'=>'AvisosController@store'])->name('avisos.store');
Route::any('edit/{id}',['uses'=>'AvisosController@edit'])->name('avisos.edit');
Route::any('update/{id}',['uses'=>'AvisosController@update'])->name('avisos.update');
Route::any('destroy/{id}',['uses'=>'AvisosController@destroy'])->name('avisos.destroy');
//Route::any('show',['uses'=>'AvisosController@show'])->name('avisos.show');
/*  ---------- Avisos ------------ */

Route::resource('materias','MateriasController');
Route::resource('gradoescolar','GradoEscolarController');
Route::resource('grupos','GruposController');
Route::resource('horarioAlumnos','HorarioAlumnosController');
Route::resource('horarioDocentes','HorarioDocentesController');
Route::resource('Docentes','DocentesController');
Route::resource('calificaciones','CalificacionesController');
Route::resource('orientador','OrientadoresController');
Route::resource('users','UsersController');

/*  ---------- Eventos ------------ */

Route::any('eventos','EventosController@index')->name('eventos.index');

Route::any('create','EventosController@create')->name('eventos.create');
Route::any('store','EventosController@store')->name('eventos.store');

Route::any('editEventos/{id}','EventosController@edit')->name('editEventos');

Route::any('updateEventos/{id}','EventosController@update')->name('updateEventos');

Route::any('down/{id}','EventosController@download')->name('download');

Route::any('destroy/{id}','EventosController@destroy')->name('delete');

/*  ---------- Eventos ------------ */


/* -------- LOGIN ---------- */
Route::any('login','Auth\LoginController@showLoginForm')->name('login');

Route::any('ac','Auth\LoginController@login');

Route::any('logout','Auth\LoginController@logout')->name('logout');
/* -------- LOGIN ---------- */



/* -------- Mail ---------- */
Route::any('mail','MailController@index')->name('mail');


/* ---------- Calendar --------- */

Route::any('calendarAdd','CalendarEventAddController@add')->name('calendarAdd');

Route::any('calendarEdit','CalendarEventAddController@edit')->name('calendarEdit');

Route::any('calendarDelete','CalendarEventAddController@destroy')->name('calendarDelete');


//Route::any('validate','LogController@validacion');



Route::any('recib','MailController@imap');

Route::group(['prefix' => 'modulos'], function() {
	/* -------- Modulo ---------- */
	Route::any('modulo',['uses'=>'ModuloController@index'])->name('modulo');
	Route::any('data',['uses'=>'ModuloController@store'])->name('data');
	/* -------- Modulo ---------- */

	Route::any('ajax','LogController@allData')->name('send');

	Route::any('pos','LogController@slave')->name('pos');

	Route::any('kenshidog','LogController@index')->name('XD');


	Route::any('ajaxR','LogController@showAlu')->name('laravel');

	Route::any('deleteA','LogController@deleteRow')->name('deleteA');

	
	/*  --------- Materias Docentes -------- */
	Route::any('materiaMaestro','LogController@maestrosMaterias')->name('mM');
	Route::any('mMaestro','LogController@query')->name('mMa');
	Route::any('posM','LogController@pinaple')->name('posM');
	Route::any('pivot','LogController@getPivot')->name('pivot');
	/*  --------- Materias Docentes -------- */
});

Route::group(['prefix' => 'reportes'], function(){

	Route::any('gruposalumnosR',['uses'=>'LogController@GAR'])->name('GAR');
	Route::any('report',['uses'=>'LogController@show'])->name('report');

	Route::any('maestrosMaterias',['uses'=>'LogController@reporteMM'])->name('MM');
	Route::any('reportMM',['uses'=>'LogController@showMM'])->name('reportMM');
	
});

Route::group(['prefix' => 'Calificaciones'], function(){

	Route::any(
		'asignacionCalificaciones', 
		['uses'=>'LogController@cal']
	)->name('cal');

	Route::any('query','LogController@querytodocentes')->name('querytodocentes');

	Route::any('getAlumno','LogController@showA')->name('datafromAlumno');

	Route::any('save','LogController@save')->name('saveGrade');
	
});




Route::any('reporte', 'LogController@reporteExcel')->name('reporte');


use Maatwebsite\Excel\Facades\Excel;

Route::any('exportToexcel/{id}', function($id){

	
	$reporte = \DB::select("SELECT
	gru.nombreG AS grupo,
	mat.nombreDelamateria AS materia,
	alumno.nombre,
	calificacion.C1 AS PrimerParcial,
	calificacion.C2 AS SegundoParcial,
	calificacion.C3 AS TercerParcial,
	calificacion.C4 AS CuartoParcial,
	calificacion.C5 AS QuintoParcial,
	setCalificaciones(alumno.nombre) AS promedioGral
	FROM calificaciones AS calificacion

	INNER JOIN alumnos AS alumno
	ON alumno.idA = calificacion.idA

	INNER JOIN materia AS mat
	ON mat.idMateria = calificacion.idMateria

	INNER JOIN detalle_grupos_calificaciones AS detalle
	ON detalle.idCalificacion = calificacion.idCalificacion

	INNER JOIN grupo AS gru
	ON gru.idGrupo = detalle.idGrupo

	INNER JOIN docentes AS docente
	ON docente.idD = calificacion.idD

	WHERE gru.idGrupo = $id ");
	//dd($reporte);
    $file = date('d-m-Y');

	Excel::create("$file", function($excel) use($reporte) {

	    $excel->sheet('Sheetname', function($sheet) use($reporte) {

	        $sheet->loadView('reportes.excel',compact('reporte'));

	    });

	})->export('xlsx');
	
	
})->name('exportToexcel');

/*  ---------- Dates Controller -------- */
Route::resource('citas','DateController');
/*  ---------- Dates Controller -------- */



/*  ---------- PayPal -------- */
Route::any('paypal','PaypalController@index')->name('beki');

Route::any('pay','PaypalController@payWithpaypal')->name('payd');

Route::any('status','PaypalController@getPaymentStatus')->name('status');
/*  ---------- PayPal -------- */


/*  ---------- Combo de retroalimentacion PayPal -------- */
Route::any('paymethod','PaypalController@comboPay')->name('combo');
Route::any('consulta', 'PaypalController@cantidadP')->name('consultaP');
/*  ---------- Combo de retroalimentacion PayPal -------- */

/* ------------ Banner Publicitario ------------- */
Route::any('banner','BannersController@index')->name('banner.index');
Route::any('create','BannersController@create')->name('banner.create');
Route::any('store','BannersController@store')->name('banner.store');
Route::any('edit/{id}','BannersController@edit')->name('banner.edit');
Route::any('update/{id}','BannersController@update')->name('banner.update');
Route::any('destroy/{id}','BannersController@destroy')->name('banner.destroy');
/* ------------ Banner Publicitario ------------- */

/*------------- Sales Registro -----------*/
Route::any('Sales','SalesController@index')->name('ventas');

Route::any('seach','SalesController@buscar')->name('busqueda');
