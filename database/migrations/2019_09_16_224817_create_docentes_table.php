<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docentes', function (Blueprint $table) {
            $table->bigIncrements('idD');
            $table->string('nombre');
            $table->string('especialidadOmateria');
            $table->string('genero');
            $table->date('fechaNacimiento');
            $table->string('direccion');
            $table->string('telefono');
            $table->string('email');
            $table->string('numCedula');
            $table->string('diaDeingreso');
            $table->string('Foto');
            $table->string('nameUser');
            $table->string('password');
            $table->integer('estatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docentes');
    }
}
