/*
SQLyog Ultimate v8.61 
MySQL - 5.7.24 : Database - it74
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`it74` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `it74`;

/*Table structure for table `alumnos` */

DROP TABLE IF EXISTS `alumnos`;

CREATE TABLE `alumnos` (
  `idA` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `matricula` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `homePhone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `curp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estatus` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idA`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `alumnos` */

insert  into `alumnos`(`idA`,`matricula`,`nombre`,`apellidos`,`telefono`,`homePhone`,`email`,`curp`,`estatus`,`created_at`,`updated_at`) values (1,'221411868','jose alberto','carmona colin','7225601533','7225601533','space_jam170@hotmail.com','CXCA950929HMCRLL04',1,'2019-09-24 05:31:44','2019-11-12 01:09:59'),(2,'22141186','Jose Alberto Jaxx','Carmona Colin','7225601533','7225601533','elora_dana_princess@hotmail.com','CXCA950929HMCRLL04',1,'2019-11-12 13:44:11','2019-11-12 13:44:45'),(3,'221310114','Elias','Martinez Cruz','7225548097','7225548097','al221310114@gmail.com','CXCA950929HMCRLL04',1,'2019-11-12 13:44:11','2019-11-12 13:44:45');

/*Table structure for table `aulas` */

DROP TABLE IF EXISTS `aulas`;

CREATE TABLE `aulas` (
  `idAu` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nombreDelAula` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edificio` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idAu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `aulas` */

/*Table structure for table `avisos` */

DROP TABLE IF EXISTS `avisos`;

CREATE TABLE `avisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `avisoEscolar` varchar(200) DEFAULT NULL,
  `idD` int(11) unsigned NOT NULL,
  `idGrupo` int(11) DEFAULT NULL,
  `ruta` varchar(100) DEFAULT NULL,
  `Foto` varchar(500) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_avisos` (`idD`),
  KEY `dsfdsf` (`idGrupo`),
  CONSTRAINT `FK_avisos` FOREIGN KEY (`idD`) REFERENCES `docentes` (`idD`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `avisos` */

insert  into `avisos`(`id`,`avisoEscolar`,`idD`,`idGrupo`,`ruta`,`Foto`,`updated_at`,`created_at`) values (2,'firma de boletas',5,2,'http://localhost:8000/template/images/','logo_edomex.png','2020-02-20 05:45:13','2020-02-20 05:45:13'),(3,'laravel',3,1,'http://localhost:8000/template/images/','logo_edomex.png','2020-02-20 05:47:00','2020-02-20 05:47:00'),(4,'junta academica',3,1,'http://localhost:8000/template/images/','logo_edomex.png',NULL,NULL),(5,'AJAX',4,3,'http://localhost:8000/template/images/','logo_edomex.png','2020-02-20 18:44:06','2020-02-20 18:44:06');

/*Table structure for table `banners` */

DROP TABLE IF EXISTS `banners`;

CREATE TABLE `banners` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `archivo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ruta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `banners` */

insert  into `banners`(`id`,`archivo`,`ruta`,`descripcion`,`created_at`,`updated_at`) values (1,'componentes1.png','http://localhost:8000/publicidad/','certificacion Cambrigs','2020-04-23 22:29:30','2020-04-23 22:51:51');

/*Table structure for table `calificaciones` */

DROP TABLE IF EXISTS `calificaciones`;

CREATE TABLE `calificaciones` (
  `idCalificacion` int(11) NOT NULL AUTO_INCREMENT,
  `idA` int(11) unsigned NOT NULL,
  `idD` int(11) unsigned NOT NULL,
  `idMateria` int(11) unsigned NOT NULL,
  `C1` varchar(200) DEFAULT NULL,
  `C2` varchar(200) DEFAULT NULL,
  `C3` varchar(200) DEFAULT NULL,
  `C4` varchar(200) DEFAULT NULL,
  `C5` varchar(200) DEFAULT NULL,
  `promedioGral` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idCalificacion`),
  KEY `calificaciones` (`idA`),
  KEY `FK_d` (`idD`),
  KEY `FK_m` (`idMateria`),
  CONSTRAINT `FK_d` FOREIGN KEY (`idD`) REFERENCES `docentes` (`idD`),
  CONSTRAINT `FK_m` FOREIGN KEY (`idMateria`) REFERENCES `materia` (`idMateria`),
  CONSTRAINT `calificaciones` FOREIGN KEY (`idA`) REFERENCES `alumnos` (`idA`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `calificaciones` */

insert  into `calificaciones`(`idCalificacion`,`idA`,`idD`,`idMateria`,`C1`,`C2`,`C3`,`C4`,`C5`,`promedioGral`,`created_at`,`updated_at`) values (11,1,3,2,'9','9','9','9','9','9','2020-02-21 06:01:50','2020-02-21 06:01:50'),(12,2,3,2,'10','10','10','10','10','10','2020-02-21 06:04:23','2020-02-21 06:04:23'),(13,3,3,2,'8','8','8','8','8','8','2020-02-21 06:04:54','2020-02-21 06:04:54');

/*Table structure for table `dates` */

DROP TABLE IF EXISTS `dates`;

CREATE TABLE `dates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dia` date DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roleschool_id` int(11) DEFAULT NULL,
  `asunto` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dates` (`roleschool_id`),
  CONSTRAINT `FK_dates` FOREIGN KEY (`roleschool_id`) REFERENCES `school_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `dates` */

insert  into `dates`(`id`,`dia`,`email`,`name`,`telefono`,`roleschool_id`,`asunto`,`created_at`,`updated_at`) values (1,'2020-03-13','22:10','Carmona','7225601533',NULL,'sdfsdfsdf','2020-03-10 20:31:14','2020-03-10 20:31:14'),(2,'2020-03-20','14:00','Joaquin','9999999999',2,'convesacion','2020-03-10 20:37:04','2020-03-10 20:37:04'),(9,'2020-05-01','14:00','LOLsito','lol',2,'lol','2020-03-10 21:43:35','2020-03-10 21:43:35'),(10,'2070-01-01','elora_dana_princess@hotmail.com','CArmona','7225601533',2,'AJAX','2020-03-16 19:42:51','2020-03-16 19:42:51');

/*Table structure for table `detalle_alumnos_grupos` */

DROP TABLE IF EXISTS `detalle_alumnos_grupos`;

CREATE TABLE `detalle_alumnos_grupos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idA` int(11) unsigned DEFAULT NULL,
  `idGrupo` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_detalle_alumnos_grupos` (`idA`),
  KEY `FK_detalle_` (`idGrupo`),
  CONSTRAINT `FK_detalle_` FOREIGN KEY (`idGrupo`) REFERENCES `grupo` (`idGrupo`),
  CONSTRAINT `FK_detalle_alumnos_grupos` FOREIGN KEY (`idA`) REFERENCES `alumnos` (`idA`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `detalle_alumnos_grupos` */

insert  into `detalle_alumnos_grupos`(`id`,`idA`,`idGrupo`,`created_at`,`updated_at`) values (1,1,1,NULL,NULL),(5,2,1,'2019-12-13 01:39:48','2019-12-13 01:39:48'),(7,3,2,'2020-02-19 06:11:13','2020-02-19 06:11:13');

/*Table structure for table `detalle_citasr_copy` */

DROP TABLE IF EXISTS `detalle_citasr_copy`;

CREATE TABLE `detalle_citasr_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idD` int(11) DEFAULT NULL,
  `idR` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `detalle_citasr_copy` */

insert  into `detalle_citasr_copy`(`id`,`idD`,`idR`,`created_at`,`updated_at`) values (1,3,NULL,NULL,NULL),(5,8,1,NULL,NULL),(6,9,2,'2020-03-10 15:43:35',NULL),(7,10,2,'2020-03-16 13:42:51',NULL);

/*Table structure for table `detalle_grupos_calificaciones` */

DROP TABLE IF EXISTS `detalle_grupos_calificaciones`;

CREATE TABLE `detalle_grupos_calificaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCalificacion` int(11) DEFAULT NULL,
  `idGrupo` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_detalle_grupos_calificaciones` (`idCalificacion`),
  KEY `FK_detalle_gruposlol` (`idGrupo`),
  CONSTRAINT `FK_detalle_grupos_calificaciones` FOREIGN KEY (`idCalificacion`) REFERENCES `calificaciones` (`idCalificacion`),
  CONSTRAINT `FK_detalle_gruposlol` FOREIGN KEY (`idGrupo`) REFERENCES `grupo` (`idGrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `detalle_grupos_calificaciones` */

insert  into `detalle_grupos_calificaciones`(`id`,`idCalificacion`,`idGrupo`,`created_at`,`updated_at`) values (6,11,1,'2020-02-21 06:01:50','2020-02-21 06:01:50'),(7,12,1,'2020-02-21 06:04:23','2020-02-21 06:04:23'),(8,13,2,'2020-02-21 06:04:55','2020-02-21 06:04:55');

/*Table structure for table `detalle_materia_docentes` */

DROP TABLE IF EXISTS `detalle_materia_docentes`;

CREATE TABLE `detalle_materia_docentes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idMateria` int(11) unsigned NOT NULL,
  `idD` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_detalle_materia_docentes` (`idMateria`),
  KEY `FK_detalle_materia_docentes2` (`idD`),
  CONSTRAINT `FK_detalle_materia_docentes` FOREIGN KEY (`idMateria`) REFERENCES `materia` (`idMateria`),
  CONSTRAINT `FK_detalle_materia_docentes2` FOREIGN KEY (`idD`) REFERENCES `docentes` (`idD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `detalle_materia_docentes` */

/*Table structure for table `docentes` */

DROP TABLE IF EXISTS `docentes`;

CREATE TABLE `docentes` (
  `idD` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `especialidadOmateria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `genero` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `numCedula` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diaDeingreso` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ruta` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nameUser` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estatus` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idD`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `docentes` */

insert  into `docentes`(`idD`,`nombre`,`especialidadOmateria`,`genero`,`fechaNacimiento`,`direccion`,`telefono`,`email`,`numCedula`,`diaDeingreso`,`ruta`,`Foto`,`nameUser`,`password`,`estatus`,`created_at`,`updated_at`) values (1,'Martin ','Español','M','2019-01-01','lajoya','23342342','sapce@fdsdf.com','435543523423423','2016-12-31','http://localhost:8000/foto/docentes/','sombra.png','laravel','123',0,'2019-09-29 02:29:41','2019-11-12 18:24:29'),(3,'Javier','Matematicas','M','2019-01-01','la joya','67786876574','sasasdasd@sdfsfsd.com','444445','2016-12-31','http://localhost:8000/foto/docentes/','CwzfPLVUQAAh5Tq.jpg','laravelM','artisan',0,'2019-09-29 02:29:41','2019-11-12 18:24:10'),(4,'Francizco Sael Cuevas','programacion','M','2001-02-10','p 5 de mayo','7225601533','elora_dana_princess@hotmail.com','12345678','2019-08-31','http://localhost:8000/foto/docentes/','admin.png','laravel','123',0,'2019-11-12 14:09:45','2019-11-12 18:24:02'),(5,'Janden Acosta Sanchez','UML','M','2021-12-31','joya 5 col centro','7225601533','elora_dana_princess@hotmail.com','12345678','2019-01-01','http://localhost:8000/foto/docentes/','photo.jpg','index','admin',1,'2019-11-12 18:23:40','2019-12-05 16:39:57');

/*Table structure for table `documents` */

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idD` int(11) DEFAULT NULL,
  `idGrupo` int(11) DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extencion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `documents` */

insert  into `documents`(`id`,`idD`,`idGrupo`,`path`,`name`,`extencion`,`created_at`,`updated_at`) values (1,5,3,'http://localhost:8000/UploadedFiles/','Calendario Escolar 2017.xlsx','xlsx','2020-02-23 23:54:39','2020-02-23 23:54:39');

/*Table structure for table `eventos` */

DROP TABLE IF EXISTS `eventos`;

CREATE TABLE `eventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventoEscolar` varchar(200) DEFAULT NULL,
  `idD` int(11) DEFAULT NULL,
  `idGrupo` int(11) DEFAULT NULL,
  `ruta` varchar(100) DEFAULT NULL,
  `Foto` varchar(500) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `eventos` */

insert  into `eventos`(`id`,`eventoEscolar`,`idD`,`idGrupo`,`ruta`,`Foto`,`updated_at`,`created_at`) values (2,'Exposicion de calaveras literarias',3,1,'http://localhost:8000/template/documents/','admin.png','2020-02-23 20:50:33','2020-02-21 23:02:55');

/*Table structure for table `gradoescolar` */

DROP TABLE IF EXISTS `gradoescolar`;

CREATE TABLE `gradoescolar` (
  `id` int(11) NOT NULL,
  `gradoEscolar` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `gradoescolar` */

insert  into `gradoescolar`(`id`,`gradoEscolar`,`created_at`,`updated_at`) values (1,'segundo','2019-09-24 05:31:44','2019-09-24 05:31:44'),(2,'primero','2019-09-24 05:31:44','2019-09-24 05:31:44'),(3,'Tercer Año','2019-11-14 05:55:39','2019-11-14 05:55:39');

/*Table structure for table `grupo` */

DROP TABLE IF EXISTS `grupo`;

CREATE TABLE `grupo` (
  `idGrupo` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `nombreG` varchar(200) DEFAULT NULL,
  `capacidad` varchar(200) DEFAULT NULL,
  `turno` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idGrupo`),
  KEY `FK` (`id`),
  CONSTRAINT `FK` FOREIGN KEY (`id`) REFERENCES `gradoescolar` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `grupo` */

insert  into `grupo`(`idGrupo`,`id`,`nombreG`,`capacidad`,`turno`,`created_at`,`updated_at`) values (1,2,'Itic-74','300','vespertino',NULL,'2019-11-26 00:32:46'),(2,2,'Itic-72','245','vespertino',NULL,'2019-11-14 18:26:52'),(3,3,'Itic-73','10','vespertino','2019-11-14 05:46:07','2019-11-14 18:27:27');

/*Table structure for table `horario_alumno` */

DROP TABLE IF EXISTS `horario_alumno`;

CREATE TABLE `horario_alumno` (
  `idHorarioA` int(11) NOT NULL AUTO_INCREMENT,
  `idA` int(11) unsigned NOT NULL,
  `idMateria` int(11) unsigned NOT NULL,
  `dia` varchar(200) DEFAULT NULL,
  `horaInicio` time DEFAULT NULL,
  `horaFin` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idHorarioA`),
  KEY `FK_horario_alumno` (`idA`),
  KEY `FK_horari` (`idMateria`),
  CONSTRAINT `FK_horari` FOREIGN KEY (`idMateria`) REFERENCES `materia` (`idMateria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_horario_alumno` FOREIGN KEY (`idA`) REFERENCES `alumnos` (`idA`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `horario_alumno` */

insert  into `horario_alumno`(`idHorarioA`,`idA`,`idMateria`,`dia`,`horaInicio`,`horaFin`,`created_at`,`updated_at`) values (1,1,1,'lunes','16:40:00','17:00:00','2019-09-27 16:26:39','2019-09-28 19:17:44'),(2,2,3,'Martes','17:00:00','17:50:00','2019-11-13 05:32:56','2019-11-14 18:14:35');

/*Table structure for table `horario_profesor` */

DROP TABLE IF EXISTS `horario_profesor`;

CREATE TABLE `horario_profesor` (
  `idHorario` int(11) NOT NULL,
  `idD` int(11) unsigned NOT NULL,
  `idMateria` int(11) unsigned NOT NULL,
  `dia` varchar(100) DEFAULT NULL,
  `horaInicio` time DEFAULT NULL,
  `horaFin` time DEFAULT NULL,
  `idGrupo` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idHorario`),
  KEY `FK_horario` (`idD`),
  KEY `FK_horario_profesor` (`idMateria`),
  KEY `FK_` (`idGrupo`),
  CONSTRAINT `FK_` FOREIGN KEY (`idGrupo`) REFERENCES `grupo` (`idGrupo`),
  CONSTRAINT `FK_horario` FOREIGN KEY (`idD`) REFERENCES `docentes` (`idD`),
  CONSTRAINT `FK_horario_profesor` FOREIGN KEY (`idMateria`) REFERENCES `materia` (`idMateria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `horario_profesor` */

insert  into `horario_profesor`(`idHorario`,`idD`,`idMateria`,`dia`,`horaInicio`,`horaFin`,`idGrupo`,`created_at`,`updated_at`) values (1,4,1,'Lunes','16:40:00','18:00:00',3,'2019-09-27 16:26:39','2019-11-14 19:02:23');

/*Table structure for table `materia` */

DROP TABLE IF EXISTS `materia`;

CREATE TABLE `materia` (
  `idMateria` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `claveDemateria` int(11) DEFAULT NULL,
  `nombreDelamateria` varchar(255) DEFAULT NULL,
  `creditos` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idMateria`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `materia` */

insert  into `materia`(`idMateria`,`claveDemateria`,`nombreDelamateria`,`creditos`,`created_at`,`updated_at`) values (1,1,'Español','400','2019-09-27 04:29:38','2019-11-13 05:14:51'),(2,4939,'Matematicas','50','2019-09-27 04:29:38','2019-09-27 04:41:50'),(3,55793,'Historial','50','2019-09-27 04:29:38','2019-11-13 05:22:44');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (7,'2014_10_12_000000_create_users_table',1),(8,'2014_10_12_100000_create_password_resets_table',1),(9,'2019_09_16_224226_create_alumnos_table',1),(10,'2019_09_16_224817_create_docentes_table',1),(11,'2019_09_16_225750_create_aulas_table',2),(12,'2019_09_16_230005_create_clases_table',3),(13,'2019_09_27_035600_add_times_to_gradoescolar',4),(14,'2019_09_27_040119_add_times_to_grupo',5),(15,'2019_09_27_040248_add_times_to_horario_alumno',6),(16,'2019_09_27_040500_add_times_to_horario_profesor',6),(17,'2019_09_27_040607_add_times_to_materia',6),(19,'2019_09_29_183648_add_times_to_calificaciones_table',7),(20,'2019_09_30_030829_create_orientadores_table',8),(21,'2019_10_17_225233_create_detalle_alumnos_grupos_table',9),(23,'2019_11_13_165759_create_events_table',10),(24,'2019_12_10_061602_create_detalle_materia_docentes',11),(25,'2020_02_23_211735_create_documents_table',12),(26,'2020_03_10_204828_create__school_role_table',13),(27,'2020_04_19_054851_create_banners_table',14),(28,'2020_04_21_065547_create_sales_table',15);

/*Table structure for table `orientadores` */

DROP TABLE IF EXISTS `orientadores`;

CREATE TABLE `orientadores` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidoPaterno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidoMaterno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sexo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `calle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `municipio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colonia` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CP` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `orientadores` */

insert  into `orientadores`(`id`,`nombre`,`apellidoPaterno`,`apellidoMaterno`,`telefono`,`correo`,`sexo`,`calle`,`municipio`,`colonia`,`CP`,`created_at`,`updated_at`) values (1,'juanita','perez','ortiz','7225601533','space_ja@dfdsf.com','F','privada alcratraces verdes','iztapalapa','la merced','88390',NULL,NULL);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`role`,`created_at`,`updated_at`) values (1,'Admin','2019-10-01 14:44:43','2019-10-01 14:44:43'),(2,'User','2019-10-01 14:44:43','2019-10-01 14:44:43'),(3,'Alumno','2019-10-01 14:44:43','2019-10-01 14:44:43');

/*Table structure for table `sales` */

DROP TABLE IF EXISTS `sales`;

CREATE TABLE `sales` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `claveTransaccion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombreAlumno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sales` */

insert  into `sales`(`id`,`claveTransaccion`,`nombreAlumno`,`correo`,`total`,`currency`,`status`,`created_at`,`updated_at`) values (2,'PAYID-L2PKEXQ09U44969V4075253G','Carmona','joelHC@gmail.com','270.00','MXN','VERIFIED','2020-04-21 07:43:38','2020-04-21 07:43:38'),(3,'PAYID-L2PKFSA2VF969348E259593V','Joel','joelHC@gmail.com','100.00','MXN','VERIFIED','2020-04-21 07:43:38','2020-04-21 07:43:38'),(4,'PAYID-L4D4JNY78U72832GG137432R',NULL,'sb-tmitl2571132@business.example.com','410.00','MXN','VERIFIED','2020-07-10 01:31:42','2020-07-10 01:31:42');

/*Table structure for table `school_role` */

DROP TABLE IF EXISTS `school_role`;

CREATE TABLE `school_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roles` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `school_role` */

insert  into `school_role`(`id`,`roles`,`created_at`,`updated_at`) values (1,'Direccion','2019-10-01 14:44:43','2019-10-01 14:44:43'),(2,'Orientacion','2019-10-01 14:44:43','2019-10-01 14:44:43');

/*Table structure for table `serviciosconcepto` */

DROP TABLE IF EXISTS `serviciosconcepto`;

CREATE TABLE `serviciosconcepto` (
  `idconce` int(11) NOT NULL,
  `concepto` varchar(100) DEFAULT NULL,
  `cantidad` int(100) DEFAULT NULL,
  `idt` int(11) DEFAULT NULL,
  PRIMARY KEY (`idconce`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `serviciosconcepto` */

insert  into `serviciosconcepto`(`idconce`,`concepto`,`cantidad`,`idt`) values (1,'Renovacion de Credencial',90,2),(2,'Tramite de credencial',100,2),(3,'Reinscripcion al Tercer Año',NULL,1),(4,'Reinscripcion al Primer Año',NULL,1),(5,'Reinscripcion al Segundo Año',NULL,1);

/*Table structure for table `serviciostipo` */

DROP TABLE IF EXISTS `serviciostipo`;

CREATE TABLE `serviciostipo` (
  `idt` int(11) NOT NULL,
  `tipo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `serviciostipo` */

insert  into `serviciostipo`(`idt`,`tipo`) values (1,'Reinscripciones'),(2,'Tramites y Documentos'),(3,'Inscripciones');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `FK_users` (`roles_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`roles_id`,`remember_token`,`created_at`,`updated_at`) values (2,'Jorge','space_jam170@hotmail.com',NULL,'$2y$10$k2BtXIhUIk9BUk85hANcDOxI1oI5VZu8em3H4165Bk/jro/d5NfJS',1,NULL,'2019-10-01 14:44:43','2019-10-01 14:44:43'),(3,'Carmona','al221411868@gmail.com',NULL,'$2y$12$DRw2pBCUbZrH0AvnpuVRcuSBqhX.eh0xbB6EaYCX5VW4JssapK01S',3,NULL,'2019-10-01 14:44:43','2019-10-01 14:44:43'),(4,'Luis Antonio','tonitoLuiyi@gmail.com',NULL,'$2y$12$vAIP2Nf8K6lEo5aP1Nh2g.Hkgwpm39mvSwBLjTWLBTi.h1UjtnqO6',1,NULL,'2019-10-01 14:44:43','2019-10-01 14:44:43'),(6,'secundaria','jaccdeveloper20@gmail.com',NULL,'$2y$12$iMHWliSpr0PadcYa/iZ1weIJ5ZFJhMKLvrnyF9RK7eDmcrJuV7paS',3,NULL,'2020-04-20 20:33:52','2020-04-20 20:33:52');

/* Trigger structure for table `dates` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `citas_AI` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `citas_AI` AFTER INSERT ON `dates` FOR EACH ROW insert into detalle_citasr_copy(idD,idR,created_at) values (new.id,new.roleschool_id,now() ) */$$


DELIMITER ;

/* Function  structure for function  `setCalificaciones` */

/*!50003 DROP FUNCTION IF EXISTS `setCalificaciones` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `setCalificaciones`(nombreAlumno varchar(200) ) RETURNS int(100)
begin
declare total int(100);
SELECT
SUM(calificacion.C1 + calificacion.C2 + calificacion.C3 + calificacion.C4 + calificacion.C5 ) / 5 into total
FROM calificaciones AS calificacion
INNER JOIN alumnos AS alumno
ON alumno.idA = calificacion.idA
WHERE alumno.nombre = nombreAlumno COLLATE utf8_unicode_ci;
return total; 
end */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
