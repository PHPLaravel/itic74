<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiciosConcepto extends Model
{
    //
    protected $table = 'serviciosConcepto';

    protected $primaryKey = 'idconce';

}
