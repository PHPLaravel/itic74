<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    //
    protected $table = 'roles';

    protected $primaryKey = 'id';
    
    protected $fillable = [
    	'role',
    ];
}
