<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    //
    protected $primaryKey = 'idGrupo';

    protected $table = 'grupo';

    protected $fillable = [
    	'idGrupo',
        'id',
    	'nombreG',
    	'capacidad',
    	'turno'
    ];

    public function gradosescolares(){
    	return $this->hasOne(GradoEscolar::class, 'id');
    }

    public function alumnos(){
        return $this->belongsToMany(Alumnos::class,'detalle_alumnos_grupos','idGrupo','idA');
    }

    public function calificaciones(){
        return $this->belongsToMany(Calificaciones::class,'detalle_grupos_calificaciones','idGrupo','idCalificacion');
    }
    

}
