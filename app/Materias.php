<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materias extends Model
{
    //
    protected $primaryKey = 'idMateria';
    
    protected $table = 'materia';

    protected $fillable = [
    	'claveDemateria',
    	'nombreDelamateria',
    	'creditos'
    ];

    public function horarioMateria(){
        return $this->hasMany(HorarioAlumno::class, 'idMateria');
    }

    public function docentes(){
        return $this->belongsToMany(Docentes::class,'detalle_materia_docentes','idMateria','idD');
    }
}
