<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiciosTipo extends Model
{
    //
    protected $table = 'serviciosTipo';

    protected $primaryKey = 'idt';
}
