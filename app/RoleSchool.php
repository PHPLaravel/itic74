<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleSchool extends Model
{
    //

    protected $table = 'roles';

    protected $primaryKey = 'id';
}
