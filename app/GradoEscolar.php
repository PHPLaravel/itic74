<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradoEscolar extends Model
{
    //

    protected $primaryKey = 'id';

    protected $table = 'gradoescolar';

    protected $fillable = [
    	'id',
    	'gradoEscolar',
    ];

    public function grupos(){
    	return $this->belongsToMany(Grupo::class, 'id');
    }
}
