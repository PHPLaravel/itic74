<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumnos extends Model
{
    //
    protected $table = 'alumnos';

    protected $primaryKey = 'idA';

    protected $fillable = [
    	'matricula',
		'nombre',
		'apellidos',
		'telefono',
		'homePhone',
		'email',
		'curp',
		'estatus'
    ];

    public function horarioAlumno(){
        return $this->hasMany(HorarioAlumno::class, 'idA');
    }

    public function grupos() {
        return $this->belongsToMany(Grupo::class,'detalle_alumnos_grupos','idGrupo','idA');
    }
}
