<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorarioProfesor extends Model
{
    //
    protected $primaryKey = 'idHorario';
    
    protected $table = 'horario_profesor';

    protected $fillable = [
    	'idD',
		'idMateria',
		'dia',
		'horaInicio',
		'horaFin',
        'idGrupo'
    ];
    public function docentes(){
        return $this->belongsTo(Docentes::class, 'idD');
    }

    public function materias(){
        return $this->belongsTo(Materias::class, 'idMateria');
    }

    public function grupo(){
        return $this->belongsTo(Grupo::class, 'idGrupo');
    }
}
