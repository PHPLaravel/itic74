<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificaciones extends Model
{
    //
    protected $table = 'calificaciones';

    protected $primaryKey = 'idCalificacion';

    protected $fillable = [
    	'idA',
		'idD',
		'idMateria',
		'C1',
		'C2',
		'C3',
		'C4',
		'C5',
		'promedioGral'

    ];

    public function docentes(){
        return $this->belongsTo(Docentes::class, 'idD');
    }

    public function alumnos(){
        return $this->belongsTo(Alumnos::class, 'idA');
    }

    public function materias(){
        return $this->belongsTo(Materias::class, 'idMateria');
    }
}
