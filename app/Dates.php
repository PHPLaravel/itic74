<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dates extends Model
{
    //
    protected $table = 'dates';

    protected $primaryKey = 'id';

    protected $guarded = [];


}
