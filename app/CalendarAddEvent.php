<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarAddEvent extends Model
{
    //
    protected $table = 'events';

    protected $primaryKey = 'id';

    protected $fillable = [
    	
    	'title',
		'start',
		'end',
		'color',

    ];

}
