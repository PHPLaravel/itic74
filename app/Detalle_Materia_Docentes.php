<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_Materia_Docentes extends Model
{
    //
    protected $table = 'detalle_materia_docentes';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
