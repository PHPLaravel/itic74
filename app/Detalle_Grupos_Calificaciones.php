<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_Grupos_Calificaciones extends Model
{
    //
    protected $table = 'detalle_grupos_calificaciones';

    protected $primaryKey = 'id';

    protected $guarded = [];
}
