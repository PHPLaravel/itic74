<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avisos extends Model
{
    //
    protected $fillable = [
    	'avisoEscolar',
    	'idD',
    	'idGrupo',
        'ruta',
        'Foto'
    ];

    public function docentes(){
    	return $this->belongsTo(Docentes::class,'idD');
    }
    
    public function grupo(){
    	return $this->belongsTo(Grupo::class,'idGrupo');
    }
}
