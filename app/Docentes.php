<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Docentes extends Model
{
    //
    protected $table = 'docentes';
    
    protected $primaryKey = 'idD';

    protected $fillable = [
    	'nombre',
		'especialidadOmateria',
		'genero',
		'fechaNacimiento',
		'direccion',
		'telefono',
		'email',
		'numCedula',
		'diaDeingreso',
		'Foto',
		'nameUser',
		'password',
		'estatus'
    ];

    public function calificaciones(){
    	return $this->hasMany(Calificaciones::class,'idD'); 	
    }
}
