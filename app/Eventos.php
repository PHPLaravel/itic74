<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eventos extends Model
{
    //
    protected $table = 'eventos';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function docentes(){
    	return $this->belongsTo(Docentes::class,'idD');
    }
    
    public function grupo(){
    	return $this->belongsTo(Grupo::class,'idGrupo');
    }
}
