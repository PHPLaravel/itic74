<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControleModule extends Model
{
    //
    protected $table = 'detalle_alumnos_grupos';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public function docentes(){
    	return $this->hasOne(Docentes::class, 'idD');
    }

    public function alumnos(){
    	return $this->hasOne(Alumnos::class, 'idA');
    }

    public function grupos(){
    	return $this->hasOne(Grupo::class, 'idGrupo');
    }

    public function materias(){
    	return $this->hasOne(Materias::class, 'idMateria');
    }
}
