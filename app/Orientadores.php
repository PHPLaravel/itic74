<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orientadores extends Model
{
    //
    protected $table = 'orientadores';
    /*
    protected $fillable = [
    	'nombre',
		'apellidoPaterno',
		'apellidoMaterno',
		'telefono',
		'correo',
		'sexo',
		'calle',
		'municipio',
		'colonia',
		'CP'
    ];
    */
    protected $guarded = [];
}
