<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! \Auth::check()) {
            return redirect()->route('login');
        }
        if( \Auth::user()->roles_id == 1 ){
            return $next($request);
            return redirect()->route('alumno.index');
        }
        if(\Auth::user()->roles_id == 2){
            return $next($request);
            return redirect()->route('alumno.index');
        }
        if( \Auth::user()->roles_id == 3){
            return $next($request);
            return redirect()->route('materias.index');
        }
        
    }
}
