<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Grupo;
use App\GradoEscolar;

class GruposController extends Controller
{
    function __construct(){
        $this->middleware('roles');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $grupos = Grupo::select(
            'grupo.idGrupo',
            'grupo.nombreG',
            'grupo.capacidad',
            'grupo.turno',
            'gradoescolar.gradoEscolar'
        )
        ->join('gradoescolar', 'grupo.id', '=', 'gradoescolar.id')
        ->get();
        //dd($grupos);
        return view('grupo.index',compact('grupos'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $grado = GradoEscolar::all();

        $llavequesigue = Grupo::all()->last()->idGrupo + 1;

        //dd($llavequesigue);
        return view('grupo.create',compact('grado','llavequesigue') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $this->validate($request,[
            'idGrupo' => 'required|numeric',
            'nombreG' => 'required|regex:/^[A-Za-z]+[-]+[0-9]+$/',
            'capacidad' => 'required|numeric',
            'turno' => 'required|not_in:0'
        ]);
        
        $store = Grupo::create($request->all() );

        if($store){
            return redirect()->route('grupos.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('grupos.index')->with('error','A ocurrido un error');
        
        //dd($request->all() );    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = Grupo::findOrFail($id);

        $gradoescolar = GradoEscolar::where('id','=',$edit->id)->get();

        
        $grados = GradoEscolar::where('id','<>',$edit->id)->get();

        //dd($edit->id);
        return view('grupo.edit',compact('edit','grados','gradoescolar') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'idGrupo' => 'required|numeric',
            'nombreG' => 'required|regex:/^[A-Za-z]+[-]+[0-9]+$/',
            'capacidad' => 'required|numeric',
            'turno' => 'required|regex:/^[A-Za-z]+$/'
        ]);
        $update = Grupo::findOrFail($id)->update($request->all() );

        if($update){
            return redirect()->route('grupos.index')->with('ok','Modificado Exitosamente');
        }
            return redirect()->route('grupos.index')->with('error','A ocurrido un error');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //4
        $destroy = Grupo::findOrFail($id)->delete();

        if($destroy){
            return redirect()->route('grupos.index')->with('ok','Eliminado Exitosamente');
        }
            return redirect()->route('grupos.index')->with('error','A ocurrido un error');
    }
}
