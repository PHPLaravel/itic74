<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;
use Session;
use App\Mail\CallReceived;

use Webklex\IMAP\Facades\Client;

class MailController extends Controller
{
    //
    public function index(Request $request){
        /*
    	"_token" => "n8xT8IVgyN4mcaz5WtYLkxWp3AJKCooliL61zPfE"
  "name" => "Juan"
  "email" => "al221411868@gmail.com"
  "subject" => "examen"
  "message" => "laravel"
  */
        
        //Mail::to($request['email'])->send(new EmergencyCallReceived($msg) );

        
        $name = $request->name;
        $to  = $request->email;
        $subject = $request->subject;
        $msg = $request->message;

        
        Mail::send('emails.contacto',[ 'subject'=>$subject,'name'=>$name,'msg'=>$msg, 'to'=>$to], function($m) use($msg,$to,$subject,$name){
            //dd($to);
            
            //$m->from($to, $name);

            $m->to('orientacion_escolar@secundaria382.com','Secundaria382');
            
        });
        
        
    	
    }

     public function imap(){

      $oClient = Client::account('default');

      $oClient->connect();

      $aFolder = $oClient->getFolders();

      foreach($aFolder as $oFolder){
       
          //Get all Messages of the current Mailbox $oFolder
          /** @var \Webklex\IMAP\Support\MessageCollection $aMessage */
          $aMessage = $oFolder->messages()->all()->get();
          
          /** @var \Webklex\IMAP\Message $oMessage */
         
          foreach($aMessage as $oMessage){

              echo $oMessage->getSubject().'<br />';
             // echo 'Attachments: '.$oMessage->getAttachments()->count().'<br />';
              echo $oMessage->getHTMLBody(true);
          }
          
      }
      
    }

}
