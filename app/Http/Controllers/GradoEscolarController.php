<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\GradoEscolar;

class GradoEscolarController extends Controller
{
    function __construct(){
        $this->middleware('roles');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $gradoescolar = GradoEscolar::all();

        return view('gradoescolar.index',compact('gradoescolar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $llavequesigue = GradoEscolar::all()->last()->id + 1;
        //dd($llavequesigue);
        return view('gradoescolar.create', compact('llavequesigue') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request->all());
        $this->validate($request,[
            'id' => 'required|numeric',
            'gradoEscolar' => 'required|regex:/^[A-Za-z]+ [A-Za-zÑñ\s]+$/'
        ]);
        $create = GradoEscolar::create( $request->all() );

        if($create){
            return redirect()->route('gradoescolar.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('gradoescolar.index')->with('error','A ocurrido un error');
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = GradoEscolar::findOrFail($id);

        return view('gradoescolar.edit',compact('edit'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'id' => 'required|numeric',
            'gradoEscolar' => 'required|regex:/^[A-Za-z]+ [A-Za-zÑñ\s]+$/'
        ]);
        $update = GradoEscolar::findOrFail($id)->update($request->all());

        if($update){
            return redirect()->route('gradoescolar.index')->with('ok','Modificado Exitosamente');
        }
            return redirect()->route('gradoescolar.index')->with('error','A ocurrido un error');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = GradoEscolar::findOrFail($id)->delete();
        
        if($delete){
            return redirect()->route('gradoescolar.index')->with('ok','Eliminado Exitosamente');
        }
            return redirect()->route('gradoescolar.index')->with('error','A ocurrido un error');
    }
}
