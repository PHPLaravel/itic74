<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Alumnos;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;

class FirstController extends Controller
{


    function __construct(){
        $this->middleware('roles');
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$alumnos = Alumnos::where('estatus','=',1)->get();
        $alumnos = Alumnos::with('horarioAlumno')->where('estatus','=',1)->get();
        //dd($alumnos);
        //return view('admin.index',compact('alumnos'));
        //return response()->json($alumnos,201);
        return $alumnos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd( $request->all() );

        $validate = \Validator::make($request->all(), [
            'matricula' => 'required',
        ]);

        if ($validate->fails()) {

            return response()->json('Error al validar',404);

        }
        /*
        $this->validate($request, [

            'matricula' => 'required|numeric|min:9',
            'nombre' => 'required|regex:/^[a-zA-Z]+ [a-zA-Z]+$/',
            'apellidos' => 'required|regex:/^[a-zA-Z]+ [a-zA-Z]+$/',
            'telefono' => 'required|numeric|min:10',
            'homePhone' => 'required|numeric|min:10',
            'email' => 'required|email',
            'curp' => 'required|regex:/^([a-z]{4})([0-9]{6})([a-z]{6})([0-9]{2})$/i',
            'estatus' => 'required'

        ]);
        */

        
        //$ok = Alumnos::create( $request->all() );

        /*
        if($ok){
            return redirect()->route('alumno.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('alumno.index')->with('error','A ocurrido un error');
        */    
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $alumno = Alumnos::findOrFail($id);
        //dd($alumno);
        return view('admin.edit',compact('alumno'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request, [

            'matricula' => 'required|numeric|min:9',
            'nombre' => 'required|regex:/^[a-zA-Z]+ [a-zA-Z]+$/',
            'apellidos' => 'required|regex:/^[a-zA-Z]+ [a-zA-Z]+$/',
            'telefono' => 'required|numeric|min:10',
            'homePhone' => 'required|numeric|min:10',
            'email' => 'required|email',
            'curp' => 'required|regex:/^([a-z]{4})([0-9]{6})([a-z]{6})([0-9]{2})$/i',
            'estatus' => 'required'

        ]);

        
        
        $alumno = Alumnos::findOrFail($id)->update( $request->all() );
        if($alumno){
            return redirect()->route('alumno.index')->with('ok','Modificado Exitosamente');
        }
            return redirect()->route('alumno.index')->with('error','A ocurrido un error');
           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $x = Alumnos::findOrFail($id)->update([
            'estatus' => 0
        ]);

        if($x){
            return redirect()->route('alumno.index')->with('ok','Eliminacion Exitosamente');

        }
            return redirect()->route('alumno.index')->with('error','A ocurrido un error');
    }
}
