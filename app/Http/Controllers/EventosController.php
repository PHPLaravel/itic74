<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Eventos;
use App\Grupo;
use App\Docentes;

class EventosController extends Controller
{
    //

    public function index(){

    	$evento = Eventos::all();

    	return view('eventos.index', compact('evento') );
    }

    public function create(){

    	$docente = Docentes::all();

    	$grupo   = Grupo::all();

    	return view('eventos.create', compact('docente','grupo'));
    }

    public function store(Request $request){

    	$this->validate($request,[
            'eventoEscolar' => 'required',
            'idD'=> 'required',
            'idGrupo'=> 'required',
            'Foto'=>'required',
        ]);

        $url = url('/');
            
        $pathFile = $url."/template/documents/";

        $file = $request->file('Foto');

        $path = public_path('/template/documents/');

        $local = $file->move($path, $file->getClientOriginalName() );

        $eventoSave = new Eventos;
        $eventoSave->eventoEscolar = $request->eventoEscolar;
        $eventoSave->idD = $request->idD;
        $eventoSave->idGrupo = $request->idGrupo;
        $eventoSave->ruta = $pathFile;
        $eventoSave->Foto = $file->getClientOriginalName();

        if( $eventoSave->save() ){
        	return redirect()->route('eventos.index');
        }
        	return redirect()->route('eventos.index');


    }

    public function edit($id){

    		
    	$evento  = Eventos::findOrFail($id);

		$eventos = \DB::select("SELECT
		evento.id,	
		evento.eventoEscolar,
        docente.idD,
		docente.nombre,
        gru.idGrupo,
		gru.nombreG,
		evento.ruta,
		evento.Foto
		FROM eventos AS evento
		INNER JOIN docentes AS docente
		ON docente.idD = evento.idD
		INNER JOIN grupo AS gru
		ON gru.idGrupo = evento.idGrupo
		WHERE evento.id = $id");
		//dd($eventos);
		
    	$docente = Docentes::where('idD','<>',$evento->idD)->get();

        $grupo   = Grupo::where('idGrupo','<>',$evento->idGrupo)->get();

    	return view('eventos.edit', compact('eventos','docente','grupo','row') );
    	

    }

    public function update(Request $request, $id){

    	$eventoselected = Eventos::findOrFail($id);

        $path = public_path('template/documents/');

        
        if( $request->hasFile('Foto') ){

            $file = $request->file('Foto');

            $extension = $file->getClientOriginalExtension();

            $nombreDearchivo = $file->getClientOriginalName();


            if( $extension == "jpeg" || $extension == "jpg" || $extension == "png" ){

                $file->move($path,$nombreDearchivo);

                $eventoselected->update([

                    'eventoEscolar'=> $request->get('eventoEscolar'),

                    'idD'          => $request->get('idD'),

                    'idGrupo'      => $request->get('idGrupo'),

                    'Foto'         => $nombreDearchivo,

                ]);

                return redirect()->route('eventos.index');

            }else{
                echo "El archivo no contiene los parametros indicados";
            }

                

        }else{

            $eventoselected->update( $request->all() );

            return redirect()->route('eventos.index');
        }
        
        //
    }

    public function destroy($id){

        $documento  = Eventos::findOrFail($id);

        $documento->delete();

        return redirect()->route('eventos.index');

    }

    public function download($id){

    	$documento  = Eventos::findOrFail($id);

    	//dd($id);

    	return response()->download(public_path("template/documents/$documento->Foto"));

    }
}
