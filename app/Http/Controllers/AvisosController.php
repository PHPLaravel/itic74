<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Avisos;
use DB;
use App\Docentes;
use App\Grupo;

class AvisosController extends Controller
{
    //
    function __construct(){
        $this->middleware('roles');
    }
    public function index(){
    	/*
    	$avisos = Avisos::select('avisos.id','avisos.avisoEscolar','docentes.nombre','docentes.especialidadOmateria','grupo.nombreG')
    	->leftjoin('docentes','avisos.idD','=','docentes.idD')
    	->leftjoin('grupo','avisos.idGrupo','=','grupo.idGrupo')
    	->where('avisos.avisoEscolar','=','junta de padres')
    	->get();
		*/
		$gralquery = Avisos::select(
			'avisos.id',
			'avisos.avisoEscolar',
			'docentes.nombre',
            'avisos.ruta',
            'avisos.Foto',
			'docentes.especialidadOmateria',
			'grupo.nombreG')

		->leftjoin('docentes','avisos.idD','=','docentes.idD')
		->leftjoin('grupo','avisos.idGrupo','=','grupo.idGrupo')
		->get();
		//dd($gralquery);
    	return view('avisos.index',compact('gralquery'));
    }

    public function create(){
    	$docentes  = DB::table('docentes')->get();
    	$grupos    = DB::table('grupo')->get();
    	//dd($grupos);
    	return view('avisos.create',compact('docentes','grupos'));
    }

    public function store(Request $request){

        if ( $request->hasFile('archivo') ) {

            $url = url('/');
            //$path = public_path('foto/docentes/');
            $pathFile = $url."/aviso/";

            //dd($pathFile);
            
            $file = $request->file('archivo');

            $path = public_path('/aviso/');

            $local = $file->move($path, $file->getClientOriginalName() );

            $avisos = new Avisos;
            $avisos->avisoEscolar = $request->avisoEscolar;
            $avisos->idD = $request->idD;
            $avisos->idGrupo = $request->idGrupo;
            $avisos->ruta = $pathFile;
            $avisos->Foto = $file->getClientOriginalName();
            $avisos->save();

            $ok = $avisos->save();

            if ($ok) {
                return redirect()->route('avisos')->with('ok','Creado Exitosamente');
            }
                return redirect()->route('avisos')->with('error','A ocurrido un error');

        }else{


            $url = url('/');

            $file = 'logo_edomex.png';

            $pathFile = 'http://localhost:8000/template/images/';

            //$pathFile = $url."/aviso/";

            //dd($pathFile);
            
            //$file = $request->file('archivo');

            $path = public_path('/aviso/');

            //$local = $file->move($path,  );

            $avisos = new Avisos;
            $avisos->avisoEscolar = $request->avisoEscolar;
            $avisos->idD = $request->idD;
            $avisos->idGrupo = $request->idGrupo;
            $avisos->ruta = $pathFile;
            $avisos->Foto = $file;
            $avisos->save();

            $ok = $avisos->save();

            if ($ok) {
                return redirect()->route('avisos')->with('ok','Creado Exitosamente');
            }
                return redirect()->route('avisos')->with('error','A ocurrido un error');

        }

        
        /*
    	$ok = Avisos::create( $request->all() );

    	if($ok){
    		return redirect()->route('avisos');
    	}
    	return "Error";
        */
    }

    public function show(Request $request){
        dd($request->all() );
    }

    public function edit($id){
        $edit = Avisos::findOrFail($id);
        $docente = Docentes::where('idD','<>',$edit->docentes->idD)->get();
        $grupo = Grupo::where('idGrupo','<>',$edit->grupo->idGrupo)->get();

        //dd($edit);
        return view('avisos.edit',compact('edit','docente','grupo'));
    }
    public function update(Request $request, $id){
        $update = Avisos::findOrFail($id)->update( $request->all() );

        if ($update) {
            return redirect()->route('avisos');
        }
            return redirect()->route('avisos');
        //dd( $request->all() );
    }
    public function destroy($id){
        //
        $x = Avisos::findOrFail($id)->delete();

        if($x){
            return redirect()->route('avisos');
        }
            return redirect()->route('avisos');
    }

}
