<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Grupo;
use App\Docentes;
use App\Documents;


class DocumentsController extends Controller
{
    //
    public function index(){

    	$documentos = Documents::all();

    	return view('documentos.index', compact('documentos'));
    }

    public function create(){

    	$docente = Docentes::all();

    	$grupo   = Grupo::all();

    	return view('documentos.create', compact('docente','grupo'));

    }

    public function store(Request $request){

    	$url = url('/');
            
        $destiny = $url."/UploadedFiles/";

    	$archivo = $request->file('file');

    	$nombreArchivo = $archivo->getClientOriginalName();

    	$extencion      = $archivo->getClientOriginalExtension();

    	$path = public_path('UploadedFiles');

    	$archivo->move($path,$nombreArchivo);

    	$file = new Documents;
    	$file->idD = $request->idD;
    	$file->idGrupo = $request->idGrupo;
    	$file->path = $destiny;
    	$file->name = $nombreArchivo;
    	$file->extencion = $extencion;
    	$file->save();


    }

    public function edit($id){
    	$archivoSeleccionado = Documents::findOrFail($id);
    	dd($archivoSeleccionado);
    }

    public function download($id){

        $archivoSeleccionado = Documents::find($id);

        $file = $archivoSeleccionado->name;

        $pathTofile = public_path('/UploadedFiles/').$file;

        return response()->download($pathTofile);  
    }
}
