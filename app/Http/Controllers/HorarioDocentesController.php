<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Materias;
use App\HorarioProfesor;
use App\Docentes;
use App\Grupo;

class HorarioDocentesController extends Controller
{
    function __construct(){
        $this->middleware('roles');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $query = HorarioProfesor::select(
            'horario_profesor.idHorario',
            'docentes.idD',
            'docentes.nombre',
            'materia.idMateria',
            'materia.claveDemateria',
            'materia.nombreDelamateria',
            'horario_profesor.dia',
            'horario_profesor.horaInicio',
            'horario_profesor.horaFin',
            'grupo.idGrupo',
            'grupo.nombreG',
            'grupo.turno'
        )
        ->leftjoin('materia','horario_profesor.idMateria','=','materia.idMateria')
        ->leftjoin('docentes','horario_profesor.idD','=','docentes.idD')
        ->leftjoin('grupo','horario_profesor.idGrupo','=','grupo.idGrupo')
        ->get();
        //dd($query);
        return view('horarioDocentes.index',compact('query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $docente = Docentes::all();
        $materia = Materias::all();
        $grupo   = Grupo::all();
        return view('horarioDocentes.create',compact('docente','materia','grupo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'idHorario' => 'required',
            'idD' => 'required',
            'idMateria' => 'required',
            'dia' => 'required|regex:/^[A-Z][a-z]*$/',
            'horaInicio' => 'required|date_format:H:i',
            'horaFin' => 'required|date_format:H:i',
            'idGrupo' => 'required'
        ]);
        
        $store = HorarioProfesor::create($request->all() );

        if($store){
            return redirect()->route('horarioDocentes.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('horarioDocentes.index')->with('error','A ocurrido un error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = HorarioProfesor::findOrFail($id);

        $materia = Materias::where('idMateria','<>',$edit->materias->idMateria)->get();

        $docente = Docentes::where('idD','<>',$edit->docentes->idD)->get();
        $grupo = Grupo::where('idGrupo','<>',$edit->grupo->idGrupo)->get();
        //dd();
        return view('horarioDocentes.edit',compact('edit','materia','docente','grupo') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $this->validate($request,[
            
            'idD' => 'required',
            'idMateria' => 'required',
            'dia' => 'required|regex:/^[A-Z][a-z]*$/',
            'idGrupo' => 'required'
        ]);
        
        $update = HorarioProfesor::findOrFail($id)->update( $request->all() );

        if($update){
            return redirect()->route('horarioDocentes.index')->with('ok','Modificado Exitosamente');
        }
            return redirect()->route('horarioDocentes.index')->with('error','A ocurrido un error');
        
   
        //dd( $request->get('idD') );   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = HorarioProfesor::findOrFail($id)->delete();

        if($delete){
            return redirect()->route('horarioDocentes.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('horarioDocentes.index')->with('error','A ocurrido un error');
    }
}
