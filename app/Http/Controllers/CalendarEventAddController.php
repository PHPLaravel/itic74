<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CalendarAddEvent;

class CalendarEventAddController extends Controller
{
    //
    public function add(Request $request){
    	//dd(  );
    	$event = CalendarAddEvent::create( $request->all() );

    	return redirect()->route('/');
    }

    public function edit(Request $request){
    	$id = $request->get('id');
    	$title = $request->get('title');
    	$color = $request->get('color');

    	$evento = CalendarAddEvent::findOrFail($id)->update([
    		'title' => $title,
    		'color' => $color
    	]);

    	return redirect()->route('/');
    	//dd($evento);
    }

    public function destroy(Request $request){

    	$id = $request->get('id');

    	$x = CalendarAddEvent::findOrFail($id)->delete();
    	
    	if($x){

    		return redirect()->route('/');	
    	}
    	
    }
}
