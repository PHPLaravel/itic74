<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Dates;

use Mail;

class DateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd( $request->all() );
        $this->validate($request,[

            'dia'=>'required',

            'name'=>'required',

            'telefono'=>'required',

            'email'=>'required',

            'asunto'=>'required',

            'roleschool_id'=>'required',


        ]);
        
        $from = $request->email;

        $asunto = $request->asunto;

        Mail::send('emails.dates',[ 'msg'=>$asunto, 'to'=>$from], function($m) use($asunto,$from){
            //dd($to);
            
            //$m->from($to, $name);

            $m->to('jaccdeveloper20@gmail.com','Secundaria382');
            
        });

        
        $store = Dates::create( $request->all() );

        if($store){
            return redirect()->back();
        }
            return redirect()->route('/');
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
