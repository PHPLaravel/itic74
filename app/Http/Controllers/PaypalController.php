<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
use Carbon\Carbon;

use App\ServiciosConcepto;
use App\ServiciosTipo;

class PaypalController extends Controller
{
	private $_api_context;
    public  $nombreAlumno;
    public  $nombre;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
        $this->middleware('roles');
    }
    //
    public function index(){


        // $servTipo = ServiciosTipo::all();


        $servTipo = collect([
            [
                'idt' => '1',
                'tipo' => 'Inscripciones'
            ],
            [
                'idt' => '2',
                'tipo' => 'Tramites y Documentos'
            ],
            [
                'idt' => '3',
                'tipo' => 'Reinscripciones'
            ]
        ]);

    	return view('paypal.prueba')->with(['servTipo' => $servTipo]);
    }

    public function comboPay(Request $request){

        $id = $request->get('id');

        $ID = (int) $id;


        /*
        $consulta = \DB::select("SELECT
        serv.idconce,
        serv.concepto
        FROM serviciosConcepto AS serv
        INNER JOIN serviciosTipo AS tipo
        ON tipo.idt = serv.idt
        WHERE tipo.idt = $ID");
        */


        $consulta = collect([
            [
                'idconce' => 1,
                'concepto' => 'Renovación de credencial',
                'cantidad' => 90
            ],
            [
                'idconce' => 2,
                'concepto' => 'Tramite de credencial',
                'cantidad' => 100

            ],
            [
                'idconce' => 3,
                'concepto' => 'Cambiar datos',
                'cantidad' => 70
            ], 

            

        ]);

        return $consulta;


    }

    public function cantidadP(Request $request){

        $idc = $request->get('idconce');

        $IDC = (int) $idc;

        $consultaP = \DB::select("SELECT
        ser.cantidad
        FROM serviciosConcepto AS ser
        WHERE ser.idconce = $IDC");

        return $consultaP[0]->cantidad;
    }

    public function payWithpaypal(Request $request){
        
        Session::put('nombre',$request->get('nombre'));

        //dd($request->all() );
        /*
        $this->validate($request, [

            'matricula' => 'required|numeric|min:9',

            'rfc' => 'required|regex:/^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?$/i',

            'curp' => 'required|regex:/^([a-z]{4})([0-9]{6})([a-z]{6})([0-9]{2})$/i',

            'ap' => 'required|regex:/^[A-Za-z]+$/',

            'am' => 'required|regex:/^[A-Za-z]+$/',

            'nombre' => 'required|regex:/^[A-Za-z]+ [A-Za-z]+$/',

            'calle' => 'required|regex:/^[a-zA-Z0-9\s]+$/',

            'NE' => 'required|regex:/^[a-zA-Z0-9\s]+$/',

            'NI' => 'required|regex:/^[a-zA-Z0-9\s]+$/',

            'colonia' => 'required|regex:/^[a-zA-Z0-9\s]+$/',

            'localidad' => 'required|regex:/^[a-zA-Z0-9\s]+$/',

            'municipio' => 'required|regex:/^[a-zA-Z0-9\s]+$/',

            'estado' => 'required|regex:/^[a-zA-Z0-9\s]+$/',

            'cp' => 'required|numeric|regex:/^+$/',

        ]);
        

        $services = $request->get('services');

        // total = 0
        foreach ($services as $service){

           $service_array = explode('|', $service);
           $service_id = $service_array[0];
           $service_count = $service_array[1];

           // Ir a tu base y consultar el servicio con el id $service_id
            // service = Service::findOrFail(1);
            // cantidad = service->cantidad;
            // subtotal = cantidad * $service_count
            // total += subtotal
        }


        dd($request->get('services'));
        */
        

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();

        $item_1->setName('Credencial') /** item name **/
            ->setCurrency('MXN')
            ->setQuantity(1)
            ->setPrice($request->get('amount')); /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('MXN')
            ->setTotal($request->get('amount'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('status')) /** Specify return URL **/
            ->setCancelUrl(URL::to('paypal'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {

            $payment->create($this->_api_context);

        } catch (\PayPal\Exception\PPConnectionException $ex) {

            if (\Config::get('app.debug')) {

                \Session::put('error', 'Connection timeout');
                return Redirect::to('/');

            } else {

                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/');

            }

        }

        foreach ($payment->getLinks() as $link) {

            if ($link->getRel() == 'approval_url') {

                $redirect_url = $link->getHref();
                break;

            }

        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());

        if (isset($redirect_url)) {

            /** redirect to paypal **/
            return Redirect::away($redirect_url);

        }

        \Session::put('error', 'Unknown error occurred');
        return Redirect::to('/');
        

    }

    public function getPaymentStatus(){
            /** Get the payment ID before session clear **/
            $payment_id = Session::get('paypal_payment_id');
    /** clear the session payment ID **/
            //Session::forget('paypal_payment_id');
            //dd($payment_id);

            if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
                //\Session::put('error', 'Payment failed');
                return Redirect::route('/');
            }
    
            $payment = Payment::get($payment_id, $this->_api_context);
            $execution = new PaymentExecution();
            $execution->setPayerId(Input::get('PayerID'));
    /**Execute the payment **/
            

            $result = $payment->execute($execution, $this->_api_context);


            if ($result->getState() == 'approved') {
            \Session::put('success', 'Payment success');
                        //return Redirect::route('beki');
                        //dd($result->transactions[0]->item_list->items[0]->name);
                        
                        $claveTransaccion = $result->id;
                        $nombreAlumno     = Session::get('nombre');
                        $correo           = $result->payer->payer_info->email;
                        $total            = $result->transactions[0]->amount->total;
                        $pesos            = $result->transactions[0]->amount->currency;
                        $status           = $result->payer->status;
                        /*
                        $created_at       = $result->create_time; 
                        $updated_at       = $result->update_time;
                        */

                        \DB::table('sales')->insert([

                            'claveTransaccion' => $claveTransaccion,
                            'nombreAlumno'     => $nombreAlumno,
                            'correo'           => $correo,
                            'total'            => $total,
                            'currency'         => $pesos,
                            'status'           => $status,
                            'created_at'                 => Carbon::now()->toDateTimeString(),
                            'updated_at'                 => Carbon::now()->toDateTimeString(),

                        ]);
                        return Redirect::route('beki');
                        
                        //dd($result);
                        //dd($result);
            }
            \Session::put('error', 'Payment failed');
                    return Redirect::route('/');
    }

}
