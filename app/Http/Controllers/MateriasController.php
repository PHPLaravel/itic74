<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Materias;

class MateriasController extends Controller
{
    function __construct(){
        $this->middleware('roles');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $materias = Materias::all();
        //dd($materias);
        return view('materias.index',compact('materias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $llavequesigue = Materias::all()->last()->idMateria + 1;
        return view('materias.create', compact('llavequesigue') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'claveDemateria' => 'required|numeric',
            'nombreDelamateria' => 'required|regex:/^[A-ZÀÂÇÉÈÊËÎÏÔÛÙÜŸÑÆŒa-zàâçéèêëîïôûùüÿñæœ0-9_.,() ]+$/',
            'creditos' => 'required|numeric'
        ]);
        
        $ok = Materias::create( $request->all() );

        if($ok){
            return redirect()->route('materias.index')->with('ok','Creado Exitosamente');
        }
        return redirect()->route('materias.index')->with('error','A ocurrido un error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = Materias::findOrFail($id);

        return view('materias.edit',compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $this->validate($request,[
            'claveDemateria' => 'required|numeric',
            'nombreDelamateria' => 'required|regex:/^[A-ZÀÂÇÉÈÊËÎÏÔÛÙÜŸÑÆŒa-zàâçéèêëîïôûùüÿñæœ0-9_.,() ]+$/',
            'creditos' => 'required|numeric'
        ]);

        
        $update = Materias::findOrFail($id)->update( $request->all() );
        
        if($update){
            return redirect()->route('materias.index')->with('ok','Modificado Exitosamente');
        }
        return redirect()->route('materias.index')->with('error','A ocurrido un error');
        
        //dd($request->all() );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = Materias::findOrFail($id)->delete();

        if($delete){
            return redirect()->route('materias.index')->with('ok','Eliminado Exitosamente');
        }
        return redirect()->route('materias.index')->with('error','A ocurrido un error');
    }
}
