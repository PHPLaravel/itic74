<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Orientadores;

class OrientadoresController extends Controller
{
    function __construct(){
        $this->middleware('roles');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $orientadores = Orientadores::all();
        return view('orientador.index',compact('orientadores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('orientador.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            
            'nombre' => 'required',
            'apellidoPaterno' => 'required',
            'apellidoMat>erno' => 'required',
            'telefono' => 'required',
            'correo' => 'required',
            'sexo' => 'required',
            'calle' => 'required',
            'municipio' => 'required',
            'colon>ia' => 'required',
            'CP' => 'required'

        ]);

        $store = Orientadores::create($request->all() );

        if ($store) {
            return redirect()->route('orientador.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('orientador.index')->with('error','A ocurrido un error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = Orientadores::findOrFail($id);
        return view('orientador.edit',compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd();
        $update = Orientadores::findOrFail($id)->update($request->all());

        if($update){
            return redirect()->route('orientador.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('orientador.index')->with('error','A ocurrido un error');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = Orientadores::findOrFail($id)->delete();

        if($delete){
            return redirect()->route('orientador.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('orientador.index')->with('error','A ocurrido un error');
    }
}
