<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Log;

use App\Alumnos;

use App\ControleModule;
use App\Detalle_Materia_Docentes;
use App\Detalle_Grupos_Calificaciones;

use App\Grupo;
use App\Materias;
use App\Docentes;
use App\Calificaciones;

use Session;

class LogController extends Controller
{
    //

    public function index(){

        //$grupo = ControleModule::with('grupos','alumnos')->where('idGrupo','=',1)->get();
        $x = Grupo::all();

        //dd( $x[0]->alumnos );
        return view('modulo.grupos',compact('x'));

    }


    public function showAlu(Request $request){

        $grupo = $request->get('grupo');

        $alumno= $request->get('alumno');

        //$primerAlu = substr($alumno[0],0,12);
        //$segundoAlu = substr($alumno[0],13,30);
        //$cadena = str_replace(' ', '', $alumno[0]);
        unset($alumno[0]);
        
        //dd( $alumno );

        return view('modulo.cal',compact('grupo','alumno'));

    }

    public function allData(Request $request){
        $grupo = $request->get('grupo');
        $capacidad = $request->get('capacidad');
        //dd($capacidad);
        
        $exce = \DB::select('SELECT al.idA,al.nombre,al.matricula FROM alumnos AS al WHERE idA NOT IN (SELECT idA FROM detalle_alumnos_grupos)');
        
        

        return view('modulo.datas', compact('exce','grupo','capacidad') );
        
        
        
    }

    public function slave(Request $request){

        //dd($request->all() );
        
        
        $bandera = $request->get('data');

        $ca = $request->get('capacidad');

        $capacidad = (int)$ca;

        //dd(gettype($capacidad));
        
        if($request->get('data') == 1){

            $cuantos = \DB::select(
                '   SELECT
                    COUNT(det.idA) as cuenta
                    FROM detalle_alumnos_grupos AS det
                '
            );
            //dd(  );
            
            if($cuantos[0]->cuenta < $capacidad){

                $modulo = new ControleModule;

            
                $modulo->idA = $request->get('idA');
                $modulo->idGrupo = $request->get('idGrupo');
           
                $modulo->save();
            }
            

        }else{

            echo "No se inserto ningun alumno";

        }
        
    
    }
    
    /*  --------- Materias Docentes -------- */

        public function maestrosMaterias(){
            $grade = Materias::all();
            //dd($grade);
            return view('modulo.maestroM.index',compact('grade') );
        }

        public function query(Request $request){

            $idMateria = Materias::where('idMateria','=',$request->get('idMateria'))->get();

            //$especialidad =$idMateria[0]->nombreDelamateria;
                
            $docentes = Docentes::all();

            foreach ($idMateria as $key) {
                foreach ($docentes as $k) {


                    if($key->nombreDelamateria === $k->especialidadOmateria){

                        $resultado = Docentes::where('especialidadOmateria','=',$key->nombreDelamateria)->get();
                        
                       return view('modulo.maestroM.query',compact('resultado','idMateria') );

                    }


                }
            }
            /*
            if($docentes[0]->especialidadOmateria === $especialidad){

                $resultado = Docentes::where('especialidadOmateria','=',$especialidad)->get();

                //return view('modulo.maestroM.query',compact('resultado','idMateria') );

            }
            */
            /*
            $doc = \DB::select('SELECT dos.idD,dos.nombre,dos.especialidadOmateria FROM docentes AS dos WHERE dos.idD NOT IN (SELECT idD FROM detalle_materia_docentes)');
            //dd($doc);
            return view('modulo.maestroM.query',compact('doc','idMateria') );
            */
            
        }

        public function pinaple(Request $request){

            $count = \DB::table("detalle_materia_docentes")->get();

            //dd(  );
            
            if($request->get('data') == 1){

                

                        $detalle_materia_docente = new Detalle_Materia_Docentes;
                
                        $detalle_materia_docente->idMateria = $request->get('idMateria');

                        $detalle_materia_docente->idD = $request->get('idD');

                        $detalle_materia_docente->save();
               


                

            }else{

                echo "No se inserto ningun alumno";

            }
            
            

        }

        public function getPivot(){
            $grade = Materias::with('docentes')->get();

            return view('modulo.maestroM.loop',compact('grade') );
        }
    /*  --------- Materias Docentes -------- */

    /*  --------- Reportes -------- */
        public function GAR(){

            $reportes = Grupo::with('alumnos','calificaciones')->get();
            //dd($reportes);
            return view('modulo.reports.index', compact('reportes') );
        }
        public function show(Request $request){
            $grupo = Grupo::where('idGrupo','=',$request->get('idGrupo') )->get();
            
            //Session::put('grupo', );
            Session::put('grupo',$grupo[0]->nombreG);
               
            $query = \DB::select('SELECT
            al.idA,
            al.nombre,
            al.matricula,
            al.apellidos,
            al.telefono,
            al.homePhone,
            al.email   
            FROM alumnos AS al
            LEFT JOIN detalle_alumnos_grupos AS det
            ON det.idA = al.idA
            WHERE det.idGrupo ='.$request->get('idGrupo').' ' );

            return view('modulo.reports.show', compact('query') );
            
        }
    /*  --------- Reportes -------- */

    /*  --------- Reportes Maestro Materia -------- */
        public function reporteMM(){

            $reportes = Materias::with('docentes')->get();
            //dd($reportes);
            return view('modulo.reports.indexMM', compact('reportes') );
        }
        public function showMM(Request $request){
            /*
            $materia = Materias::where('idMateria','=',$request->get('idMateria') )->get();
            
            //Session::put('grupo', );
            Session::put('materia',$materia[0]->nombreDelamateria);
            */  
            $query = \DB::select('
            SELECT
            dos.idD,
            dos.diaDeingreso,
            dos.numCedula,
            dos.nombre,
            dos.especialidadOmateria,
            dos.direccion,
            dos.telefono,
            dos.email            
            FROM docentes AS dos
            LEFT JOIN detalle_materia_docentes AS det
            ON det.idD = dos.idD
            WHERE det.idMateria ='.$request->get('idMateria').' ' );
            
            //dd($query);
            return view('modulo.reports.showMM', compact('query') );
            
        }
    /*  --------- Reportes Maestro Materia -------- */

    /*  --------- deletes -------- */  
        public function deleteRow(Request $request){
            
            $query = \DB::select('SELECT COUNT(*) as cuantos
            FROM detalle_alumnos_grupos AS det
            WHERE det.idA ='.$request->get('idA').' ' );

            $id = (int)$request->get('idA');

            if($query[0]->cuantos == 1){

                $update = \DB::delete("DELETE FROM detalle_alumnos_grupos WHERE idA = $id");

                

                if($update){

                    $estatus = 1;

                    return $estatus;

                }

            }
        }
        /*  --------- deletes -------- */   


        public function cal(Request $request){
            
            $grupo = Grupo::where('idGrupo','=',$request->get('idGrupo') )->get();

            //dd($grupo[0]->nombreG);
            Session::put('grupo',$grupo[0]->nombreG);

            Session::put('idGrupo',$grupo[0]->idGrupo);
            
            $query = \DB::select('SELECT
            alu.idA,
            alu.matricula,
            alu.nombre,
            alu.apellidos
            FROM alumnos AS alu
            INNER JOIN detalle_alumnos_grupos AS det
            ON det.idA = alu.idA
            WHERE det.idGrupo ='.$request->get('idGrupo').' AND alu.idA NOT IN (SELECT idA FROM calificaciones) ' );

            $materias = \DB::select("SELECT 
            mat.idMateria,
            mat.nombreDelamateria
            FROM materia AS mat");

            $docente  = \DB::select("
            SELECT
            doc.idD,
            doc.nombre
            FROM docentes AS doc
            WHERE doc.idD NOT IN (SELECT idD FROM calificaciones)");

            return view('modulo.calificaciones.calificaciones1',

                compact('query','materias','docente') 

            );
        }

        public function querytodocentes(Request $request){
            $si = $request->get('materia');
            /*
            $docente = \DB::select("SELECT
            doce.nombre
            FROM docentes AS doce
            where doce.especialidadOmateria = $si ");
            */
            $docente = Docentes::where('especialidadOmateria','=',$si)->get();
            if (count($docente) == 1) {

                return $docente;

            }
                return "Error";
            
        }

        public function showA(Request $request){
            //dd($request->all() );
            
            $idA = $request->get('idA');

            $idD = $request->get('idD');

            $idMateria = $request->get('idMateria');

            $dimension = $request->get('dimension');

            $alumno = Alumnos::where('idA','=',$idA)->get();

            //dd($alumno);

            return view(

                'modulo.calificaciones.calificacionAlumno',

                compact('alumno','idA','idD','idMateria','dimension')

            );
            

        }

        public function save(Request $request){

            $idD = $request->get('idD');

            $idMateria = $request->get('idMateria');

            $idA = $request->get('idA');

            $idGrupo = $request->get('idGrupo');

            $cal1 = $request->get('ca1');
            $cal2 = $request->get('ca2');
            $cal3 = $request->get('ca3');
            $cal4 = $request->get('ca4');
            $cal5 = $request->get('ca5');


            $promedioGral = ($cal1 + $cal2 + $cal3 + $cal4 + $cal5) / 5;



                
                $grade = new Calificaciones;

                $grade->idA = $idA;

                $grade->idD = $idD;

                $grade->idMateria = $idMateria;

                $grade->C1 = $cal1;
                $grade->C2 = $cal2;
                $grade->C3 = $cal3;
                $grade->C4 = $cal4;
                $grade->C5 = $cal5;

                $grade->promedioGral = $promedioGral;

                $grade->save();
                

                


               $lastID = \DB::table('calificaciones')
                ->orderBy('idCalificacion', 'desc')
                ->first();

                //dd($lastID->idCalificacion);

                $detalle = new Detalle_Grupos_Calificaciones;
                $detalle->idCalificacion = $lastID->idCalificacion;
                $detalle->idGrupo = $idGrupo;
                
                

                if( $detalle->save() ){
                    return redirect()->route('GAR')->with('ok','El alumno ha sido calificado');
                } 
                    return redirect()->route('GAR')->with('error','Contacte Al Administrador');  
                  


            /*
            $datos = $request->get('data');

            //dd($datos[0]);

            foreach ($datos as $key ) {

                //var_dump($key['idA']);
                
                $grade = new Calificaciones;

                $grade->idA = $key['idA'];

                $grade->idD = $key['idD'];

                $grade->idMateria = $key['idMateria'];

                $grade->C1 = $key['C1'];
                $grade->C2 = $key['C2'];
                $grade->C3 = $key['C3'];
                $grade->C4 = $key['C4'];
                $grade->C5 = $key['C5'];

                $grade->promedioGral = $key['promedioGral'];

                $grade->save();
                
            }
            */

            /*
            foreach($datos as $item){

                $grade = new Calificaciones;

                $grade->idA = $item->idA;

                $grade->idD = $item->idD;

                $grade->idMateria = $item->idMateria;

                $grade->C1 = $item->C1;
                $grade->C2 = $item->C2;
                $grade->C3 = $item->C3;
                $grade->C4 = $item->C4;
                $grade->C5 = $item->C5;

                $grade->promedioGral = $item->promedioGral;

                $grade->save();


               

            }
            */
        }

        public function reporteExcel(){

            /*
            $reporte = \DB::select("SELECT
            gru.nombreG,
            alumno.nombre,
            cali.C1 AS PrimerParcial,
            cali.C2 AS SegundoParcial,
            cali.C3 AS TercerParcial,
            cali.C4 AS CuartoParcial,
            cali.C5 AS QuintoParcial,
            setCalificaciones(alumno.nombre) AS promedioGral
            FROM alumnos AS alumno
            INNER JOIN calificaciones AS cali
            ON cali.idA = alumno.idA
            INNER JOIN detalle_grupos_calificaciones AS detalle
            ON detalle.idCalificacion = cali.idCalificacion
            INNER JOIN grupo AS gru
            ON gru.idGrupo = detalle.idGrupo
            WHERE gru.nombreG = 'Itic-74' ");
            */

            $reportes = Grupo::with('alumnos','calificaciones')->get();
            //dd($reportes);
            //$id = $reportes[0]->idGrupo;
            /*
            $reporte = \DB::select("SELECT
            gru.idGrupo,        
            mat.nombreDelamateria AS materia,
            alumno.nombre,
            calificacion.C1 AS PrimerParcial,
            calificacion.C2 AS SegundoParcial,
            calificacion.C3 AS TercerParcial,
            calificacion.C4 AS CuartoParcial,
            calificacion.C5 AS QuintoParcial,
            setCalificaciones(alumno.nombre) AS promedioGral
            FROM calificaciones AS calificacion

            INNER JOIN alumnos AS alumno
            ON alumno.idA = calificacion.idA

            INNER JOIN materia AS mat
            ON mat.idMateria = calificacion.idMateria

            INNER JOIN detalle_grupos_calificaciones AS detalle
            ON detalle.idCalificacion = calificacion.idCalificacion

            INNER JOIN grupo AS gru
            ON gru.idGrupo = detalle.idGrupo

            INNER JOIN docentes AS docente
            ON docente.idD = calificacion.idD

            WHERE gru.idGrupo = $id ");
            */

            Session::put('grupo',$reportes[0]->nombreG);
            //return view('modulo.reports.index', compact('reportes') );
            //dd($reportes[0]->calificaciones);

            return view('modulo.calificaciones.reportes', compact('reportes') );

        }

}
