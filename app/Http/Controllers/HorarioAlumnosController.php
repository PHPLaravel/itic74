<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Alumnos;
use App\Materias;
use App\HorarioAlumno;

class HorarioAlumnosController extends Controller
{
    function __construct(){
        $this->middleware('roles');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $query = HorarioAlumno::select('horario_alumno.idHorarioA','alumnos.nombre','horario_alumno.dia','materia.claveDemateria','materia.nombreDelamateria','horario_alumno.horaInicio','horario_alumno.horaFin')
        ->leftjoin('alumnos','horario_alumno.idA','=','alumnos.idA')
        ->leftjoin('materia','horario_alumno.idMateria','=','materia.idMateria')
        ->get();

        //dd($query);
        return view('horariosAlumnos.index',compact('query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $alumnos = Alumnos::all();

        $materia = Materias::all();

        return view('horariosAlumnos.create',compact('alumnos','materia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'idA' => 'required',
            'idMateria' => 'required',
            'dia' => 'required|regex:/^[A-Z][a-z]+$/',
            'horaInicio' => 'required|date_format:H:i',
            'horaFin' => 'required|date_format:H:i|after:horaInicio'
        ]);
        $create = HorarioAlumno::create($request->all() );

        if($create){
            return redirect()->route('horarioAlumnos.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('horarioAlumnos.index')->with('error','A ocurrido un error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = HorarioAlumno::findOrFail($id);

        //$alumnos = \DB::table('alumnos')->get();
        //$materia = \DB::table('materia')->get();
        //
        $alumnos = Alumnos::where('idA','<>',$edit->alumnos->idA)->get();
        $materias = Materias::where('idMateria','<>',$edit->materias->idMateria)->get();

        //dd($materias);
        return view('horariosAlumnos.edit',compact('edit','alumnos','materias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd($request->all() );

        $this->validate($request,[
            'idA' => 'required',
            'idMateria' => 'required',
            'dia' => 'regex:/^[A-Z][a-z]*$/|required',
            'horaInicio' => 'required',
            'horaFin' => 'required'
        ]);
        //dd($request->all() );
        
        $update = HorarioAlumno::findOrFail($id)->update($request->all() );
        if($update){
            return redirect()->route('horarioAlumnos.index')->with('ok','Modificado Exitosamente');
        }
            return redirect()->route('horarioAlumnos.index')->with('error','A ocurrido un error');
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = HorarioAlumno::findOrFail($id)->delete();

        if($delete){
            return redirect()->route('horarioAlumnos.index')->with('ok','Eliminado Exitosamente');
        }
            return redirect()->route('horarioAlumnos.index')->with('error','A ocurrido un error');
    }
}
