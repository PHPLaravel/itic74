<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Banners;

class BannersController extends Controller
{
    //
    function __construct(){
        $this->middleware('roles');
    }
    
    public function index(){

        $banners = Banners::all();

    	return view('banner.index', compact('banners'));
    }

    public function create(){
    	return view('banner.create');
    }

    public function store(Request $request){
        /*
         $this->validate($request,[
            'descripcion' => 'required|regex:/^[A-Z]+[a-z]+$/',
            'foto'=> 'required|mimes:jpeg,png',
           
        ]);
        */
        
        $url = url('/');
        //$path = public_path('foto/docentes/');
        $pathFile = $url."/publicidad/";

        //dd($pathFile);
        
        $file = $request->file('archivo');

        $path = public_path('/publicidad/');

        $local = $file->move($path, $file->getClientOriginalName() );

        $banner = new Banners;
        $banner->archivo = $file->getClientOriginalName();
        $banner->ruta = $pathFile;
        $banner->descripcion = $request->descripcion;
        $banner->save();

        $ok = $banner->save();

        if ($ok) {
            return redirect()->route('banner.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('banner.index')->with('error','A ocurrido un error');

    }

    public function edit($id){

        $banner = Banners::findOrFail($id);

        return view('banner.edit', compact('banner'));
    }

    public function update(Request $request, $id){

        /*
         $this->validate($request,[
            'nombre' => 'required|regex:/^[a-zA-Z]+ [a-zA-Z]+ [a-zA-Z]+$/',
            'especialidadOmateria'=> 'required|regex:/^[a-zA-Z]+$/',
            'genero'=> 'required|regex:/^[a-zA-Z]+$/',
            'fechaNacimiento'=> 'required|date:Y-m-d',
            'direccion'=> 'required|regex:/^[a-zA-Z]+ [0-9]+ [a-zA-Z]+ [a-zA-Z]+$/',
            'telefono'=> 'required|numeric|min:10',
            'email'=> 'required|email',
            'numCedula'=> 'required|numeric|min:8',
            'diaDeingreso'=> 'required|date:Y-m-d',
            'foto'=> 'mimes:jpeg,png',
            'nameUser'=> 'required|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'password'=> 'required',
            'estatus'=> 'required|not_in:0',
        ]);
        */
        $url = url('/');
        //$path = public_path('foto/docentes/');
        $pathFile = $url."/publicidad/";

        if($request->hasFile('archivo')){

            $file = $request->file('archivo');

            $avatar = $file->getClientOriginalName();

            $path = public_path('/publicidad/');

            $local = $file->move($path,$avatar);
            //dd($avatar);
            
            $update = Banners::findOrFail($id)->update([
                'archivo'=> $avatar ,
                'ruta'   => $pathFile,
                'descripcion' => $request->get('descripcion'),

            ]);

            if ($update) {
                return redirect()->route('banner.index')->with('ok','modificado Exitosamente');
            }
                return redirect()->route('banner.index')->with('error','A ocurrido un error');
                
        }

        
        $updateS = Banners::findOrFail($id)->update([
            'descripcion' => $request->get('descripcion'),

        ]);

        if ($updateS) {
            return redirect()->route('banner.index')->with('ok','modificado Exitosamente');
        }
            return redirect()->route('banner.index')->with('error','A ocurrido un error');
        

    }

    public function destroy($id){

        $del = Banners::findOrFail($id)->delete();

    }


}
