<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Calificaciones;
use App\Docentes;
use App\Alumnos;
use App\Materias;

class CalificacionesController extends Controller
{
    function __construct(){
        $this->middleware('roles');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $calificacion = Calificaciones::with('docentes','alumnos','materias')->get();
        //dd($calificacion);
        return view('calificaciones.index',compact('calificacion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $docentes = Docentes::all();
        $alumnos  = Alumnos::all();
        $materias = Materias::all();
        return view('calificaciones.create',compact('docentes','alumnos','materias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[

            'idA' => 'required',
            'idD' => 'required',
            'idMateria' => 'required',
            'C1'  => 'required',
            'C2'  => 'required',
            'C3'  => 'required',
            'C4'  => 'required',
            'C5'  => 'required'

        ]);

        $x = Calificaciones::create($request->all() );

        if($x){
            return redirect()->route('calificaciones.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('calificaciones.index')->with('error','A ocurrido un error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = Calificaciones::findOrFail($id);
        $docentes = Docentes::where('idD','<>',$edit->docentes->idD)->get();
        $alumno = Alumnos::where('idA','<>',$edit->alumnos->idA)->get();
        $materia= Materias::where('idMateria','<>',$edit->materias->idMateria)->get();
        //dd($materia);
        return view('calificaciones.edit',compact(
            'edit','docentes','alumno','materia'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            
            'idA' => 'required',
            'idD' => 'required',
            'idMateria' => 'required',
            'C1'  => 'required',
            'C2'  => 'required',
            'C3'  => 'required',
            'C4'  => 'required',
            'C5'  => 'required'

        ]);
        
        $update = Calificaciones::findOrFail($id)->update( $request->all() );

        if($update){
            return redirect()->route('calificaciones.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('calificaciones.index')->with('error','A ocurrido un error');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $destroy = Calificaciones::findOrFail($id)->delete();

        if($destroy){
            return redirect()->route('calificaciones.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('calificaciones.index')->with('error','A ocurrido un error');
    }
}
