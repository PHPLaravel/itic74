<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;

class SalesController extends Controller
{
    //
    public function index(){
    	/*
    	$ventas = \DB::table('sales')->get();
    	//dd($ventas);
    	return view('ventas.index', compact('ventas') );
    	*/
    	$usuario = \Auth::user()->name;

    	$rolUsuario = \Auth::user()->roles_id;

        Session::put('usuario',$usuario);
        Session::put('rol',$rolUsuario);

    	if ($rolUsuario == 3) {
    		//dd("Formulario Para Busqueda");
    		return view('ventas.formularioBusqueda');
    	}else{
    		//dd("Tabla General");
    		$ventas = \DB::table('sales')->get();
    		return view('ventas.index', compact('ventas') );
    	}

    }

    public function buscar(Request $request){
    	//dd($nombre);
    	$nombre = $request->get('nombre');

    	$ventas = \DB::table('sales')->where('sales.nombreAlumno', 'LIKE', '%' . $nombre . '%')->get();

    	return $ventas;
    }
}
