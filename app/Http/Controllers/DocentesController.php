<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Docentes;

class DocentesController extends Controller
{
    function __construct(){
        $this->middleware('roles');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $docentes = Docentes::where('estatus','=',1)->get();
        return view('docente.index',compact('docentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('docente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request->all() );
        
        $this->validate($request,[
            'nombre' => 'required|regex:/^[a-zA-Z]+ [a-zA-Z]+ [a-zA-Z]+$/',
            'especialidadOmateria'=> 'required|regex:/^[a-zA-Z]+$/',
            'genero'=> 'required|in:M,F',
            'fechaNacimiento'=> 'required|date:Y-m-d',
            'direccion'=> 'required|regex:/^[a-zA-Z]+ [0-9]+ [a-zA-Z]+ [a-zA-Z]+$/',
            'telefono'=> 'required|numeric|min:10',
            'email'=> 'required|email',
            'numCedula'=> 'required|numeric|min:8',
            'diaDeingreso'=> 'required|date:Y-m-d',
            'foto'=> 'required|mimes:jpeg,png',
            'nameUser'=> 'required|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'password'=> 'required',
            'estatus'=> 'required',
        ]);
        
        $url = url('/');
        //$path = public_path('foto/docentes/');
        $pathFile = $url."/foto/docentes/";

        //dd($pathFile);
        
        $file = $request->file('foto');

        $path = public_path('/foto/docentes/');

        $local = $file->move($path, $file->getClientOriginalName() );

        $docente = new Docentes;
        $docente->nombre = $request->nombre;
        $docente->especialidadOmateria = $request->especialidadOmateria;
        $docente->genero = $request->genero;
        $docente->fechaNacimiento = $request->fechaNacimiento;
        $docente->direccion = $request->direccion;
        $docente->telefono = $request->telefono;
        $docente->email = $request->email;
        $docente->numCedula = $request->numCedula;
        $docente->diaDeingreso = $request->diaDeingreso;
        $docente->ruta = $pathFile;
        $docente->Foto = $file->getClientOriginalName();
        $docente->nameUser = $request->nameUser;
        $docente->password = $request->password;
        $docente->estatus = $request->estatus;
        $docente->save();

        $ok = $docente->save();

        if ($ok) {
            return redirect()->route('Docentes.index')->with('ok','Creado Exitosamente');
        }
            return redirect()->route('Docentes.index')->with('error','A ocurrido un error');
           
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $url = url('/');

        $do  = Docentes::all()->last()->idD + 1;

        $ruta = $url."/foto/docentes/";

        $rutadir = public_path('foto\\docentes\\');
        
        if(is_dir($rutadir)){

            $gestor = opendir($rutadir);
            echo "<ul>";

                // Recorre todos los elementos del directorio
                while ( ($archivo = readdir($gestor) ) !== false)  {
                        
                    $ruta_completa = $ruta.$archivo;
                    //dd($ruta_completa);
                    if (!file_exists($do)){
                            mkdir($do, 0777, true);
                    }
                    // Se muestran todos los archivos y carpetas excepto "." y ".."
                    /*
                    if ($archivo != "." && $archivo != "..") {
                        // Si es un directorio se recorre recursivamente
                        //dd($);
                        
                        if (is_dir($ruta_completa)) {
                            //echo "<li>" . $archivo . "</li>";
                            //dd($archivo);

                            if (!file_exists($do)){
                                mkdir($do, 0777, true);
                            }

                        } else {
                            //echo "<li>" . $archivo . "</li>";
                            //dd($archivo);
                            if (!file_exists($do)){
                                mkdir($do, 0777, true);
                            }
                        }
                        
                    }
                    */
                    
                }
                
                // Cierra el gestor de directorios
                closedir($gestor);
            echo "</ul>";
        }


        
        //dd($folder);
        //dd();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = Docentes::findOrFail($id);
        
        return view('docente.edit',compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $this->validate($request,[
            'nombre' => 'required|regex:/^[a-zA-Z]+ [a-zA-Z]+ [a-zA-Z]+$/',
            'especialidadOmateria'=> 'required|regex:/^[a-zA-Z]+$/',
            'genero'=> 'required|regex:/^[a-zA-Z]+$/',
            'fechaNacimiento'=> 'required|date:Y-m-d',
            'direccion'=> 'required|regex:/^[a-zA-Z]+ [0-9]+ [a-zA-Z]+ [a-zA-Z]+$/',
            'telefono'=> 'required|numeric|min:10',
            'email'=> 'required|email',
            'numCedula'=> 'required|numeric|min:8',
            'diaDeingreso'=> 'required|date:Y-m-d',
            'foto'=> 'mimes:jpeg,png',
            'nameUser'=> 'required|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'password'=> 'required',
            'estatus'=> 'required|not_in:0',
        ]);
        
        $url = url('/');
        //$path = public_path('foto/docentes/');
        $pathFile = $url."/foto/docentes/";

        if($request->hasFile('foto')){

            $file = $request->file('foto');

            $avatar = $file->getClientOriginalName();

            $path = public_path('foto/docentes/');

            $local = $file->move($path,$avatar);

            $update = Docentes::findOrFail($id)->update([
                'nombre' => $request->get('nombre'),
                'especialidadOmateria'=> $request->get('especialidadOmateria'),
                'genero'=> $request->get('genero'),
                'fechaNacimiento'=> $request->get('fechaNacimiento'),
                'direccion'=> $request->get('direccion'),
                'telefono'=> $request->get('telefono'),
                'email'=> $request->get('email'),
                'numCedula'=> $request->get('numCedula'),
                'diaDeingreso'=> $request->get('diaDeingreso'),
                'Foto'=> $avatar ,
                'nameUser'=> $request->get('nameUser'),
                'password'=> $request->get('password'),
                'estatus'=> $request->get('estatus'),

            ]);

            if ($update) {
                return redirect()->route('Docentes.index')->with('ok','modificado Exitosamente');
            }
                return redirect()->route('Docentes.index')->with('error','A ocurrido un error');
        }

            $update = Docentes::findOrFail($id)->update([
                'nombre' => $request->get('nombre'),
                'especialidadOmateria'=> $request->get('especialidadOmateria'),
                'genero'=> $request->get('genero'),
                'fechaNacimiento'=> $request->get('fechaNacimiento'),
                'direccion'=> $request->get('direccion'),
                'telefono'=> $request->get('telefono'),
                'email'=> $request->get('email'),
                'numCedula'=> $request->get('numCedula'),
                'diaDeingreso'=> $request->get('diaDeingreso'),
                'nameUser'=> $request->get('nameUser'),
                'password'=> $request->get('password'),
                'estatus'=> $request->get('estatus'),

            ]);

            if ($update) {
                return redirect()->route('Docentes.index')->with('ok','modificado Exitosamente');
            }
                return redirect()->route('Docentes.index')->with('error','A ocurrido un error');
        
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = Docentes::findOrFail($id)->update([
            'estatus' => 0
        ]);

        if ($delete) {
            return redirect()->route('Docentes.index')->with('ok','Eliminado Exitosamente');
        }
            return redirect()->route('Docentes.index')->with('error','A ocurrido un error');
    }
}
