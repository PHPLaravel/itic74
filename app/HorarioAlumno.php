<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorarioAlumno extends Model
{
    //
    protected $table = 'horario_alumno';

    protected $primaryKey = 'idHorarioA';

    protected $fillable = [
		'idA',
		'idMateria',
		'dia',
		'horaInicio',
		'horaFin',
    ];

    public function alumnos(){
    	return $this->belongsTo(Alumnos::class ,'idA');
    }

    public function materias(){
        return $this->belongsTo(Materias::class ,'idMateria');
    }

}
